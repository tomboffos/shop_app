import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:shop_app_mobile/core/utils/constants/radiuses.dart';
import 'package:shop_app_mobile/features/category/presentation/widgets/category_item.dart';
import 'package:shop_app_mobile/features/home/domain/models/recommendation/recommendation.dart';
import 'package:shop_app_mobile/features/product/presentation/widgets/product_item.dart';

@RoutePage()
class RecommendationPage extends StatefulWidget {
  final Recommendation recommendation;

  const RecommendationPage({super.key, required this.recommendation});

  @override
  State<RecommendationPage> createState() => _RecommendationPageState();
}

class _RecommendationPageState extends State<RecommendationPage> {
  bool _showProducts = false;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16),
      child: Column(
        children: [
          if (widget.recommendation.products.isNotEmpty &&
              widget.recommendation.categories.isNotEmpty)
            Container(
              decoration: BoxDecoration(
                border: Border.all(
                  color: Theme.of(context).colorScheme.primary,
                ),
                borderRadius: Radiuses.radius2x,
              ),
              child: Row(
                children: [
                  Expanded(
                    child: InkWell(
                      onTap: () {
                        setState(() {
                          _showProducts = false;
                        });
                      },
                      child: Card(
                        color: !_showProducts ? null : Colors.transparent,
                        elevation: 0,
                        child: const Center(
                          child: Text(
                            'Категории',
                            style: TextStyle(
                              color: Colors.black,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    child: InkWell(
                      onTap: () {
                        setState(() {
                          _showProducts = true;
                        });
                      },
                      child: Card(
                        color: _showProducts ? null : Colors.transparent,
                        elevation: 0,
                        child: const Center(
                          child: Text(
                            'Продукты',
                            style: TextStyle(
                              color: Colors.black,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          if (_showProducts || widget.recommendation.categories.isEmpty)
            SizedBox(
              width: MediaQuery.of(context).size.width,
              child: GridView.builder(
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
                padding: const EdgeInsets.all(10),
                itemCount: widget.recommendation.products.length,
                gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 2,
                  crossAxisSpacing: 10,
                  mainAxisSpacing: 10,
                  childAspectRatio: 3 / 4,
                ),
                itemBuilder: (context, index) => ProductItem(
                  product: widget.recommendation.products[index],
                ),
              ),
            ),
          if (!_showProducts || widget.recommendation.products.isEmpty)
            GridView.builder(
              padding: const EdgeInsets.all(10),
              shrinkWrap: true,
              physics: const NeverScrollableScrollPhysics(),
              itemCount: widget.recommendation.categories.length,
              gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2,
                crossAxisSpacing: 10,
                mainAxisSpacing: 10,
                childAspectRatio: 6 / 7,
              ),
              itemBuilder: (context, index) => CategoryItem(
                category: widget.recommendation.categories[index],
              ),
            ),
        ],
      ),
    );
  }
}
