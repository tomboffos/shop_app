import 'package:auto_route/auto_route.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:field_suggestion/field_suggestion.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shop_app_mobile/core/injection/injection.dart';
import 'package:shop_app_mobile/core/routing/app_router.dart';
import 'package:shop_app_mobile/core/utils/constants/insets.dart';
import 'package:shop_app_mobile/features/cart/presentation/cubit/cart_cubit.dart';
import 'package:shop_app_mobile/features/message/presentation/cubit/message_cubit.dart';
import 'package:shop_app_mobile/features/product/domain/dto/product_get_dto.dart';
import 'package:shop_app_mobile/features/product/domain/repository/product_repository.dart';
import 'package:shop_app_mobile/features/profile/presentation/cubit/user_cubit.dart';
import 'package:shop_app_mobile/features/settings/domain/models/setting.dart';
import 'package:shop_app_mobile/features/settings/presentation/cubit/setting_cubit.dart';

@RoutePage()
class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  void initState() {
    getIt.get<CartCubit>().getItems();
    getIt.get<UserCubit>().getUser();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SettingCubit, SettingState>(
      bloc: getIt.get<SettingCubit>(),
      builder: (context, state) {
        return state.maybeWhen(
          orElse: () => const SizedBox.shrink(),
          loaded: (settings) => AutoTabsScaffold(
            routes: [
              const MainRoute(),
              CategoriesRoute(),
              const CartRoute(),
              const FavoritesRoute(),
              const ProfileRoute()
            ],
            drawer: bool.parse(settings.drawer)
                ? Drawer(
                    child: Padding(
                      padding: const EdgeInsets.only(top: 120),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          InkWell(
                            onTap: () =>
                                context.router.push(const ProfileRoute()),
                            child: const ListTile(
                              leading: Icon(Icons.person),
                              title: Text('Профиль'),
                            ),
                          ),
                          InkWell(
                            onTap: () => context.router.push(CategoriesRoute()),
                            child: const ListTile(
                              leading: Icon(Icons.grid_view_rounded),
                              title: Text('Категории'),
                            ),
                          ),
                          InkWell(
                            onTap: () => context.router.push(const CartRoute()),
                            child: const ListTile(
                              leading: Icon(Icons.shopping_bag),
                              title: Text('Корзина'),
                            ),
                          ),
                          InkWell(
                            onTap: () =>
                                context.router.push(const FavoritesRoute()),
                            child: const ListTile(
                              leading: Icon(Icons.favorite),
                              title: Text('Избранное'),
                            ),
                          ),
                          BlocBuilder<UserCubit, UserState>(
                            bloc: getIt.get<UserCubit>(),
                            builder: (context, state) => state.maybeWhen(
                              orElse: () => const SizedBox.shrink(),
                              authorized: (user) => InkWell(
                                onTap: () {
                                  getIt.get<UserCubit>().logout();
                                  Scaffold.of(context).closeDrawer();
                                },
                                child: const ListTile(
                                  leading: Icon(Icons.logout),
                                  title: Text('Выйти'),
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  )
                : null,
            appBarBuilder: (context, tabsRouter) => AppBar(
              leading: _isNeedToDisplay(settings)
                  ? null
                  : InkWell(
                      onTap: () => context.router.pop(),
                      child: IconButton(
                        icon: const Icon(
                          Icons.chevron_left,
                          size: 40,
                        ),
                        padding: const EdgeInsets.all(5),
                        onPressed: () {
                          context.router.back();
                        },
                      ),
                    ),
              title: BlocBuilder<SettingCubit, SettingState>(
                bloc: getIt.get<SettingCubit>(),
                builder: (context, state) {
                  return state.maybeWhen<Widget>(
                    orElse: SizedBox.shrink,
                    loaded: (settings) => Padding(
                      padding: Insets.dimens2x,
                      child: CachedNetworkImage(
                        imageUrl: settings.logo,
                        width: 50,
                        height: 50,
                      ),
                    ),
                  );
                },
              ),
              actions: [],
              bottom: hideScroll()
                  ? PreferredSize(
                      preferredSize:
                          Size(MediaQuery.of(context).size.width, 50),
                      child: Padding(
                        padding: Insets.dimens,
                        child: FieldSuggestion.network(
                          textController: TextEditingController(),
                          future: (input) => getIt
                              .get<ProductRepository>()
                              .getProducts(ProductGetDto(search: input)),
                          builder: (context, value) => ListView.builder(
                            padding: EdgeInsets.zero,
                            itemBuilder: (context, index) => InkWell(
                              onTap: () {
                                context.router
                                    .push(ProductDetailRoute(
                                        product: value.data![index]))
                                    .then((value) {
                                  // searchController = TextEditingController();
                                });
                              },
                              child: ListTile(
                                contentPadding: EdgeInsets.zero,
                                title: Text(
                                  value.data![index].name,
                                  style: Theme.of(context).textTheme.bodyLarge,
                                ),
                              ),
                            ),
                            itemCount: value.data?.length ?? 0,
                          ),
                          inputDecoration: const InputDecoration(
                            hintText: 'Поиск',
                          ),
                        ),
                      ),
                    )
                  : null,
            ),
            bottomNavigationBuilder: (context, child) {
              final tabsRouter = AutoTabsRouter.of(context);

              return bool.parse(settings.drawer)
                  ? const SizedBox.shrink()
                  : BlocListener<MessageCubit, MessageState>(
                      bloc: getIt.get<MessageCubit>(),
                      listener: (context, state) {
                        state.whenOrNull(
                          addedMessage: (message) =>
                              ScaffoldMessenger.of(context).showSnackBar(
                            SnackBar(
                              content: Text(message),
                            ),
                          ),
                        );
                      },
                      child: BottomNavigationBar(
                        onTap: (index) {
                          tabsRouter.setActiveIndex(index);
                        },
                        currentIndex: tabsRouter.activeIndex,
                        items: const [
                          BottomNavigationBarItem(
                            icon: Icon(Icons.home),
                            label: "Главная",
                          ),
                          BottomNavigationBarItem(
                            icon: Icon(Icons.grid_view_rounded),
                            label: "Категории",
                          ),
                          BottomNavigationBarItem(
                            icon: Icon(Icons.shopping_bag),
                            label: "Корзина",
                          ),
                          BottomNavigationBarItem(
                            icon: Icon(Icons.favorite),
                            label: "Избранное",
                          ),
                          BottomNavigationBarItem(
                            icon: Icon(Icons.person),
                            label: "Профиль",
                          )
                        ],
                      ),
                    );
            },
          ),
        );
      },
    );
  }

  bool hideScroll() => ['/main'].contains(context.router.currentPath);

  bool _isNeedToDisplay(Setting setting) {
    var name = context.router.currentPath;

    return (bool.parse(setting.drawer)
            ? ['/main']
            : ['/main', '/categories', '/cart', '/favorites', '/profile'])
        .contains(name);
  }
}
