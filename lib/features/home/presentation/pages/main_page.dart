import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shop_app_mobile/core/injection/injection.dart';
import 'package:shop_app_mobile/core/utils/constants/gaps.dart';
import 'package:shop_app_mobile/features/home/presentation/cubit/recommendation_cubit.dart';
import 'package:shop_app_mobile/features/home/presentation/widgets/recommendation_item.dart';

@RoutePage()
class MainPage extends StatefulWidget {
  const MainPage({super.key});

  @override
  State<MainPage> createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  final _recommendationCubit = getIt.get<RecommendationCubit>();

  @override
  void initState() {
    _recommendationCubit.getRecommendations();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: RefreshIndicator(
        onRefresh: () async {
          _recommendationCubit.getRecommendations();
        },
        child: SingleChildScrollView(
          physics: const AlwaysScrollableScrollPhysics(),
          child: BlocBuilder<RecommendationCubit, RecommendationState>(
            bloc: _recommendationCubit,
            builder: (context, state) {
              return Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  ...state.maybeWhen(
                    orElse: () => [],
                    loaded: (recommendations) => recommendations.map(
                      (e) => RecommendationItem(
                        recommendation: e,
                      ),
                    ),
                  ),
                  Gaps.gap3,
                  // const NewsSlider(),
                ],
              );
            },
          ),
        ),
      ),
    );
  }
}
