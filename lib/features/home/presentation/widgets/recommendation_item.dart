import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:shop_app_mobile/core/routing/app_router.dart';
import 'package:shop_app_mobile/core/utils/constants/gaps.dart';
import 'package:shop_app_mobile/core/utils/constants/insets.dart';
import 'package:shop_app_mobile/core/utils/extensions/theme_extension.dart';
import 'package:shop_app_mobile/features/category/presentation/widgets/category_item.dart';
import 'package:shop_app_mobile/features/home/domain/models/recommendation/recommendation.dart';
import 'package:shop_app_mobile/features/product/presentation/widgets/product_item.dart';

class RecommendationItem extends StatefulWidget {
  final Recommendation recommendation;

  const RecommendationItem({super.key, required this.recommendation});

  @override
  State<RecommendationItem> createState() => _RecommendationItemState();
}

class _RecommendationItemState extends State<RecommendationItem> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: Insets.dimens2x.copyWith(bottom: 0),
          child: InkWell(
            onTap: () => context.router.push(
              RecommendationRoute(
                recommendation: widget.recommendation,
              ),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  widget.recommendation.title,
                  style: context.text.headlineLarge,
                ),
                const Text(
                  'Все',
                  style: TextStyle(
                    decoration: TextDecoration.underline,
                  ),
                )
              ],
            ),
          ),
        ),
        if (widget.recommendation.categories.isNotEmpty) ...[
          Gaps.gap2,
          SizedBox(
            height: MediaQuery.of(context).size.height / 4,
            child: ListView.builder(
              shrinkWrap: true,
              scrollDirection: Axis.horizontal,
              itemCount: widget.recommendation.categories.length,
              itemBuilder: (BuildContext context, int index) => Padding(
                padding: Insets.dimens2x.copyWith(bottom: 0, top: 0, right: 0),
                child: CategoryItem(
                  category: widget.recommendation.categories[index],
                ),
              ),
            ),
          ),
        ],
        if (widget.recommendation.products.isNotEmpty) ...[
          Gaps.gap2,
          SizedBox(
            height: MediaQuery.of(context).size.height / 3.4,
            child: ListView.builder(
              shrinkWrap: true,
              scrollDirection: Axis.horizontal,
              itemCount: widget.recommendation.products.length,
              itemBuilder: (BuildContext context, int index) => Padding(
                padding: Insets.dimens2x.copyWith(bottom: 0, top: 0, right: 0),
                child: ProductItem(
                  product: widget.recommendation.products[index],
                ),
              ),
            ),
          ),
        ]
      ],
    );
  }
}
