import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:shop_app_mobile/features/home/domain/models/recommendation/recommendation.dart';
import 'package:shop_app_mobile/features/home/domain/repository/recommendation_repository.dart';
import 'package:shop_app_mobile/features/recommendation/domain/dto/recommendation_create_update_dto.dart';

part 'recommendation_state.dart';
part 'recommendation_cubit.freezed.dart';

@injectable
class RecommendationCubit extends Cubit<RecommendationState> {
  final RecommendationRepository _repository;

  RecommendationCubit(this._repository)
      : super(const RecommendationState.initial());

  void getRecommendations() async {
    try {
      final recommendations = await _repository.getRecommendations();

      emit(RecommendationState.loaded(recommendations));
    } on Exception {
      emit(const RecommendationState.loaded([]));
    }
  }

  Future<void> createRecommendation({
    required RecommendationCreateUpdateDto dto,
  }) =>
      _repository.createRecommendation(dto: dto);

  Future<void> updateRecommendation({
    required RecommendationCreateUpdateDto dto,
    required int id,
  }) =>
      _repository.updateRecommendation(dto: dto, id: id);

  Future<void> deleteRecommendation({required int id}) =>
      _repository.deleteRecommendation(id: id);
}
