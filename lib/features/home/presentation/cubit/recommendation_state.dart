part of 'recommendation_cubit.dart';

@freezed
class RecommendationState with _$RecommendationState {
  const factory RecommendationState.initial() = _Initial;
  const factory RecommendationState.loaded(
    List<Recommendation> recommendations,
  ) = _Loaded;
}
