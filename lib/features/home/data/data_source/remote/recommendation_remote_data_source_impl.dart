import 'package:dio/dio.dart';
import 'package:dio/src/response.dart';
import 'package:injectable/injectable.dart';
import 'package:shop_app_mobile/features/category/data/data_source/category_remote_data_source.dart';
import 'package:shop_app_mobile/features/home/data/data_source/remote/recommendation_remote_data_source.dart';
import 'package:shop_app_mobile/features/recommendation/domain/dto/recommendation_create_update_dto.dart';

@Injectable(as: RecommendationRemoteDataSource)
class RecommendationRemoteDataSourceImpl
    implements RecommendationRemoteDataSource {
  final Dio _dio;

  RecommendationRemoteDataSourceImpl(this._dio) {
    _dio.options.baseUrl += CategoryRemoteDataSource.shop;
  }

  @override
  Future<Response> getRecommendations() =>
      _dio.get(RecommendationRemoteDataSource.recommendations);

  @override
  Future<void> createRecommendation(
          {required RecommendationCreateUpdateDto dto}) =>
      _dio.post(
        RecommendationRemoteDataSource.recommendationsAdmin,
        data: dto.toJson(),
      );

  @override
  Future<void> deleteRecommendation({required int id}) =>
      _dio.delete('${RecommendationRemoteDataSource.recommendationsAdmin}/$id');

  @override
  Future<void> updateRecommendation(
          {required RecommendationCreateUpdateDto dto, required int id}) =>
      _dio.post(
        '${RecommendationRemoteDataSource.recommendationsAdmin}/$id',
        data: dto.toJson(),
      );
}
