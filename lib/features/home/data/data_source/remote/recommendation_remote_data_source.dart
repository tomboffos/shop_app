import 'package:dio/dio.dart';
import 'package:shop_app_mobile/features/recommendation/domain/dto/recommendation_create_update_dto.dart';

abstract class RecommendationRemoteDataSource {
  static String recommendations = '/recommendations';
  static String recommendationsAdmin = '/recommendations/admin';

  Future<Response> getRecommendations();

  Future<void> createRecommendation({
    required RecommendationCreateUpdateDto dto,
  });

  Future<void> updateRecommendation({
    required RecommendationCreateUpdateDto dto,
    required int id,
  });

  Future<void> deleteRecommendation({required int id});
}
