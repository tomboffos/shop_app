import 'package:injectable/injectable.dart';
import 'package:shop_app_mobile/features/home/data/data_source/remote/recommendation_remote_data_source.dart';
import 'package:shop_app_mobile/features/home/domain/models/recommendation/recommendation.dart';
import 'package:shop_app_mobile/features/home/domain/repository/recommendation_repository.dart';
import 'package:shop_app_mobile/features/recommendation/domain/dto/recommendation_create_update_dto.dart';

@Injectable(as: RecommendationRepository)
class RecommendationRepositoryImpl implements RecommendationRepository {
  final RecommendationRemoteDataSource _dataSource;

  RecommendationRepositoryImpl(this._dataSource);

  @override
  Future<List<Recommendation>> getRecommendations() async {
    final response = await _dataSource.getRecommendations();

    return (response.data['data'] as List)
        .map((e) => Recommendation.fromJson(e))
        .toList();
  }

  @override
  Future<void> createRecommendation(
          {required RecommendationCreateUpdateDto dto}) =>
      _dataSource.createRecommendation(dto: dto);

  @override
  Future<void> deleteRecommendation({required int id}) =>
      _dataSource.deleteRecommendation(id: id);

  @override
  Future<void> updateRecommendation({
    required RecommendationCreateUpdateDto dto,
    required int id,
  }) =>
      _dataSource.updateRecommendation(
        dto: dto,
        id: id,
      );
}
