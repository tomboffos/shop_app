import 'package:shop_app_mobile/features/home/domain/models/recommendation/recommendation.dart';
import 'package:shop_app_mobile/features/recommendation/domain/dto/recommendation_create_update_dto.dart';

abstract class RecommendationRepository {
  Future<List<Recommendation>> getRecommendations();

  Future<void> createRecommendation({
    required RecommendationCreateUpdateDto dto,
  });

  Future<void> updateRecommendation({
    required RecommendationCreateUpdateDto dto,
    required int id,
  });

  Future<void> deleteRecommendation({required int id});
}
