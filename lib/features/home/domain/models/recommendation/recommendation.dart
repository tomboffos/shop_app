import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:shop_app_mobile/features/category/domain/models/category.dart';
import 'package:shop_app_mobile/features/product/domain/models/product.dart';

part 'recommendation.g.dart';
part 'recommendation.freezed.dart';

@freezed
class Recommendation with _$Recommendation {
  factory Recommendation({
    required String title,
    required int id,
    required List<Product> products,
    required List<Category> categories,
  }) = _Recommendation;

  factory Recommendation.fromJson(Map<String, dynamic> json) =>
      _$RecommendationFromJson(json);
}
