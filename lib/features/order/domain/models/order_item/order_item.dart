import 'package:shop_app_mobile/features/product/domain/models/product.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'order_item.g.dart';
part 'order_item.freezed.dart';

@freezed
class OrderItem with _$OrderItem {
  factory OrderItem({
    required double price,
    required int quantity,
    required Product product,
    required int id,
  }) = _OrderItem;

  factory OrderItem.fromJson(Map<String, dynamic> json) =>
      _$OrderItemFromJson(json);
}
