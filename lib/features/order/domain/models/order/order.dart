import 'package:shop_app_mobile/features/order/domain/models/order_item/order_item.dart';
import 'package:shop_app_mobile/features/order/domain/models/order_status/order_status.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'order.g.dart';
part 'order.freezed.dart';

@freezed
class Order with _$Order {
  factory Order({
    required double price,
    String? address,
    String? phone,
    String? name,
    String? comments,
    required OrderStatus status,
    required List<OrderItem> products,
    required int id,
    @JsonKey(name: 'created_at') required DateTime createdAt,
  }) = _Order;

  factory Order.fromJson(Map<String, dynamic> json) => _$OrderFromJson(json);
}
