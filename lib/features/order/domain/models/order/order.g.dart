// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'order.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$OrderImpl _$$OrderImplFromJson(Map<String, dynamic> json) => _$OrderImpl(
      price: (json['price'] as num).toDouble(),
      address: json['address'] as String?,
      phone: json['phone'] as String?,
      name: json['name'] as String?,
      comments: json['comments'] as String?,
      status: OrderStatus.fromJson(json['status'] as Map<String, dynamic>),
      products: (json['products'] as List<dynamic>)
          .map((e) => OrderItem.fromJson(e as Map<String, dynamic>))
          .toList(),
      id: json['id'] as int,
      createdAt: DateTime.parse(json['created_at'] as String),
    );

Map<String, dynamic> _$$OrderImplToJson(_$OrderImpl instance) =>
    <String, dynamic>{
      'price': instance.price,
      'address': instance.address,
      'phone': instance.phone,
      'name': instance.name,
      'comments': instance.comments,
      'status': instance.status,
      'products': instance.products,
      'id': instance.id,
      'created_at': instance.createdAt.toIso8601String(),
    };
