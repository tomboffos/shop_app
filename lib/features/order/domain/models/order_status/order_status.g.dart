// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'order_status.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$OrderStatusImpl _$$OrderStatusImplFromJson(Map<String, dynamic> json) =>
    _$OrderStatusImpl(
      name: json['name'] as String,
    );

Map<String, dynamic> _$$OrderStatusImplToJson(_$OrderStatusImpl instance) =>
    <String, dynamic>{
      'name': instance.name,
    };
