import 'package:freezed_annotation/freezed_annotation.dart';

part 'order_status.g.dart';
part 'order_status.freezed.dart';

@freezed
class OrderStatus with _$OrderStatus {
  factory OrderStatus({
    required String name,
  }) = _OrderStatus;

  factory OrderStatus.fromJson(Map<String, dynamic> json) =>
      _$OrderStatusFromJson(json);
}
