// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'order_create_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$OrderCreateDtoImpl _$$OrderCreateDtoImplFromJson(Map<String, dynamic> json) =>
    _$OrderCreateDtoImpl(
      deviceId: json['device_id'] as String?,
      userId: json['user_id'] as int?,
      name: json['name'] as String?,
      address: json['address'] as String?,
      phone: json['phone'] as String?,
      comments: json['comments'] as String?,
      products: (json['products'] as List<dynamic>)
          .map((e) => ProductOrderDto.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$$OrderCreateDtoImplToJson(
        _$OrderCreateDtoImpl instance) =>
    <String, dynamic>{
      'device_id': instance.deviceId,
      'user_id': instance.userId,
      'name': instance.name,
      'address': instance.address,
      'phone': instance.phone,
      'comments': instance.comments,
      'products': instance.products.map((e) => e.toJson()).toList(),
    };
