import 'package:shop_app_mobile/features/order/domain/dto/product_order_dto/product_order_dto.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'order_create_dto.g.dart';
part 'order_create_dto.freezed.dart';

@freezed
class OrderCreateDto with _$OrderCreateDto {
  @JsonSerializable(explicitToJson: true)
  factory OrderCreateDto({
    @JsonKey(name: 'device_id') String? deviceId,
    @JsonKey(name: 'user_id') int? userId,
    String? name,
    String? address,
    String? phone,
    String? comments,
    required List<ProductOrderDto> products,
  }) = _OrderCreateDto;

  factory OrderCreateDto.fromJson(Map<String, dynamic> json) =>
      _$OrderCreateDtoFromJson(json);
}
