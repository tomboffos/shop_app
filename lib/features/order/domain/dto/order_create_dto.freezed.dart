// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'order_create_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

OrderCreateDto _$OrderCreateDtoFromJson(Map<String, dynamic> json) {
  return _OrderCreateDto.fromJson(json);
}

/// @nodoc
mixin _$OrderCreateDto {
  @JsonKey(name: 'device_id')
  String? get deviceId => throw _privateConstructorUsedError;
  @JsonKey(name: 'user_id')
  int? get userId => throw _privateConstructorUsedError;
  String? get name => throw _privateConstructorUsedError;
  String? get address => throw _privateConstructorUsedError;
  String? get phone => throw _privateConstructorUsedError;
  String? get comments => throw _privateConstructorUsedError;
  List<ProductOrderDto> get products => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $OrderCreateDtoCopyWith<OrderCreateDto> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $OrderCreateDtoCopyWith<$Res> {
  factory $OrderCreateDtoCopyWith(
          OrderCreateDto value, $Res Function(OrderCreateDto) then) =
      _$OrderCreateDtoCopyWithImpl<$Res, OrderCreateDto>;
  @useResult
  $Res call(
      {@JsonKey(name: 'device_id') String? deviceId,
      @JsonKey(name: 'user_id') int? userId,
      String? name,
      String? address,
      String? phone,
      String? comments,
      List<ProductOrderDto> products});
}

/// @nodoc
class _$OrderCreateDtoCopyWithImpl<$Res, $Val extends OrderCreateDto>
    implements $OrderCreateDtoCopyWith<$Res> {
  _$OrderCreateDtoCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? deviceId = freezed,
    Object? userId = freezed,
    Object? name = freezed,
    Object? address = freezed,
    Object? phone = freezed,
    Object? comments = freezed,
    Object? products = null,
  }) {
    return _then(_value.copyWith(
      deviceId: freezed == deviceId
          ? _value.deviceId
          : deviceId // ignore: cast_nullable_to_non_nullable
              as String?,
      userId: freezed == userId
          ? _value.userId
          : userId // ignore: cast_nullable_to_non_nullable
              as int?,
      name: freezed == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String?,
      address: freezed == address
          ? _value.address
          : address // ignore: cast_nullable_to_non_nullable
              as String?,
      phone: freezed == phone
          ? _value.phone
          : phone // ignore: cast_nullable_to_non_nullable
              as String?,
      comments: freezed == comments
          ? _value.comments
          : comments // ignore: cast_nullable_to_non_nullable
              as String?,
      products: null == products
          ? _value.products
          : products // ignore: cast_nullable_to_non_nullable
              as List<ProductOrderDto>,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$OrderCreateDtoImplCopyWith<$Res>
    implements $OrderCreateDtoCopyWith<$Res> {
  factory _$$OrderCreateDtoImplCopyWith(_$OrderCreateDtoImpl value,
          $Res Function(_$OrderCreateDtoImpl) then) =
      __$$OrderCreateDtoImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {@JsonKey(name: 'device_id') String? deviceId,
      @JsonKey(name: 'user_id') int? userId,
      String? name,
      String? address,
      String? phone,
      String? comments,
      List<ProductOrderDto> products});
}

/// @nodoc
class __$$OrderCreateDtoImplCopyWithImpl<$Res>
    extends _$OrderCreateDtoCopyWithImpl<$Res, _$OrderCreateDtoImpl>
    implements _$$OrderCreateDtoImplCopyWith<$Res> {
  __$$OrderCreateDtoImplCopyWithImpl(
      _$OrderCreateDtoImpl _value, $Res Function(_$OrderCreateDtoImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? deviceId = freezed,
    Object? userId = freezed,
    Object? name = freezed,
    Object? address = freezed,
    Object? phone = freezed,
    Object? comments = freezed,
    Object? products = null,
  }) {
    return _then(_$OrderCreateDtoImpl(
      deviceId: freezed == deviceId
          ? _value.deviceId
          : deviceId // ignore: cast_nullable_to_non_nullable
              as String?,
      userId: freezed == userId
          ? _value.userId
          : userId // ignore: cast_nullable_to_non_nullable
              as int?,
      name: freezed == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String?,
      address: freezed == address
          ? _value.address
          : address // ignore: cast_nullable_to_non_nullable
              as String?,
      phone: freezed == phone
          ? _value.phone
          : phone // ignore: cast_nullable_to_non_nullable
              as String?,
      comments: freezed == comments
          ? _value.comments
          : comments // ignore: cast_nullable_to_non_nullable
              as String?,
      products: null == products
          ? _value._products
          : products // ignore: cast_nullable_to_non_nullable
              as List<ProductOrderDto>,
    ));
  }
}

/// @nodoc

@JsonSerializable(explicitToJson: true)
class _$OrderCreateDtoImpl implements _OrderCreateDto {
  _$OrderCreateDtoImpl(
      {@JsonKey(name: 'device_id') this.deviceId,
      @JsonKey(name: 'user_id') this.userId,
      this.name,
      this.address,
      this.phone,
      this.comments,
      required final List<ProductOrderDto> products})
      : _products = products;

  factory _$OrderCreateDtoImpl.fromJson(Map<String, dynamic> json) =>
      _$$OrderCreateDtoImplFromJson(json);

  @override
  @JsonKey(name: 'device_id')
  final String? deviceId;
  @override
  @JsonKey(name: 'user_id')
  final int? userId;
  @override
  final String? name;
  @override
  final String? address;
  @override
  final String? phone;
  @override
  final String? comments;
  final List<ProductOrderDto> _products;
  @override
  List<ProductOrderDto> get products {
    if (_products is EqualUnmodifiableListView) return _products;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_products);
  }

  @override
  String toString() {
    return 'OrderCreateDto(deviceId: $deviceId, userId: $userId, name: $name, address: $address, phone: $phone, comments: $comments, products: $products)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$OrderCreateDtoImpl &&
            (identical(other.deviceId, deviceId) ||
                other.deviceId == deviceId) &&
            (identical(other.userId, userId) || other.userId == userId) &&
            (identical(other.name, name) || other.name == name) &&
            (identical(other.address, address) || other.address == address) &&
            (identical(other.phone, phone) || other.phone == phone) &&
            (identical(other.comments, comments) ||
                other.comments == comments) &&
            const DeepCollectionEquality().equals(other._products, _products));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, deviceId, userId, name, address,
      phone, comments, const DeepCollectionEquality().hash(_products));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$OrderCreateDtoImplCopyWith<_$OrderCreateDtoImpl> get copyWith =>
      __$$OrderCreateDtoImplCopyWithImpl<_$OrderCreateDtoImpl>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$OrderCreateDtoImplToJson(
      this,
    );
  }
}

abstract class _OrderCreateDto implements OrderCreateDto {
  factory _OrderCreateDto(
      {@JsonKey(name: 'device_id') final String? deviceId,
      @JsonKey(name: 'user_id') final int? userId,
      final String? name,
      final String? address,
      final String? phone,
      final String? comments,
      required final List<ProductOrderDto> products}) = _$OrderCreateDtoImpl;

  factory _OrderCreateDto.fromJson(Map<String, dynamic> json) =
      _$OrderCreateDtoImpl.fromJson;

  @override
  @JsonKey(name: 'device_id')
  String? get deviceId;
  @override
  @JsonKey(name: 'user_id')
  int? get userId;
  @override
  String? get name;
  @override
  String? get address;
  @override
  String? get phone;
  @override
  String? get comments;
  @override
  List<ProductOrderDto> get products;
  @override
  @JsonKey(ignore: true)
  _$$OrderCreateDtoImplCopyWith<_$OrderCreateDtoImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
