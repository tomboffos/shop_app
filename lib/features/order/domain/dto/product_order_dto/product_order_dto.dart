import 'package:freezed_annotation/freezed_annotation.dart';

part 'product_order_dto.g.dart';
part 'product_order_dto.freezed.dart';

@freezed
class ProductOrderDto with _$ProductOrderDto {
  factory ProductOrderDto({
    @JsonKey(name: 'product_id') required int productId,
    required int quantity,
  }) = _ProductOrderDto;

  factory ProductOrderDto.fromJson(Map<String, dynamic> json) =>
      _$ProductOrderDtoFromJson(json);
}
