// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'product_order_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$ProductOrderDtoImpl _$$ProductOrderDtoImplFromJson(
        Map<String, dynamic> json) =>
    _$ProductOrderDtoImpl(
      productId: json['product_id'] as int,
      quantity: json['quantity'] as int,
    );

Map<String, dynamic> _$$ProductOrderDtoImplToJson(
        _$ProductOrderDtoImpl instance) =>
    <String, dynamic>{
      'product_id': instance.productId,
      'quantity': instance.quantity,
    };
