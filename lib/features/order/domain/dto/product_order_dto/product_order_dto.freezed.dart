// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'product_order_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

ProductOrderDto _$ProductOrderDtoFromJson(Map<String, dynamic> json) {
  return _ProductOrderDto.fromJson(json);
}

/// @nodoc
mixin _$ProductOrderDto {
  @JsonKey(name: 'product_id')
  int get productId => throw _privateConstructorUsedError;
  int get quantity => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ProductOrderDtoCopyWith<ProductOrderDto> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ProductOrderDtoCopyWith<$Res> {
  factory $ProductOrderDtoCopyWith(
          ProductOrderDto value, $Res Function(ProductOrderDto) then) =
      _$ProductOrderDtoCopyWithImpl<$Res, ProductOrderDto>;
  @useResult
  $Res call({@JsonKey(name: 'product_id') int productId, int quantity});
}

/// @nodoc
class _$ProductOrderDtoCopyWithImpl<$Res, $Val extends ProductOrderDto>
    implements $ProductOrderDtoCopyWith<$Res> {
  _$ProductOrderDtoCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? productId = null,
    Object? quantity = null,
  }) {
    return _then(_value.copyWith(
      productId: null == productId
          ? _value.productId
          : productId // ignore: cast_nullable_to_non_nullable
              as int,
      quantity: null == quantity
          ? _value.quantity
          : quantity // ignore: cast_nullable_to_non_nullable
              as int,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$ProductOrderDtoImplCopyWith<$Res>
    implements $ProductOrderDtoCopyWith<$Res> {
  factory _$$ProductOrderDtoImplCopyWith(_$ProductOrderDtoImpl value,
          $Res Function(_$ProductOrderDtoImpl) then) =
      __$$ProductOrderDtoImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({@JsonKey(name: 'product_id') int productId, int quantity});
}

/// @nodoc
class __$$ProductOrderDtoImplCopyWithImpl<$Res>
    extends _$ProductOrderDtoCopyWithImpl<$Res, _$ProductOrderDtoImpl>
    implements _$$ProductOrderDtoImplCopyWith<$Res> {
  __$$ProductOrderDtoImplCopyWithImpl(
      _$ProductOrderDtoImpl _value, $Res Function(_$ProductOrderDtoImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? productId = null,
    Object? quantity = null,
  }) {
    return _then(_$ProductOrderDtoImpl(
      productId: null == productId
          ? _value.productId
          : productId // ignore: cast_nullable_to_non_nullable
              as int,
      quantity: null == quantity
          ? _value.quantity
          : quantity // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$ProductOrderDtoImpl implements _ProductOrderDto {
  _$ProductOrderDtoImpl(
      {@JsonKey(name: 'product_id') required this.productId,
      required this.quantity});

  factory _$ProductOrderDtoImpl.fromJson(Map<String, dynamic> json) =>
      _$$ProductOrderDtoImplFromJson(json);

  @override
  @JsonKey(name: 'product_id')
  final int productId;
  @override
  final int quantity;

  @override
  String toString() {
    return 'ProductOrderDto(productId: $productId, quantity: $quantity)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ProductOrderDtoImpl &&
            (identical(other.productId, productId) ||
                other.productId == productId) &&
            (identical(other.quantity, quantity) ||
                other.quantity == quantity));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, productId, quantity);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$ProductOrderDtoImplCopyWith<_$ProductOrderDtoImpl> get copyWith =>
      __$$ProductOrderDtoImplCopyWithImpl<_$ProductOrderDtoImpl>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$ProductOrderDtoImplToJson(
      this,
    );
  }
}

abstract class _ProductOrderDto implements ProductOrderDto {
  factory _ProductOrderDto(
      {@JsonKey(name: 'product_id') required final int productId,
      required final int quantity}) = _$ProductOrderDtoImpl;

  factory _ProductOrderDto.fromJson(Map<String, dynamic> json) =
      _$ProductOrderDtoImpl.fromJson;

  @override
  @JsonKey(name: 'product_id')
  int get productId;
  @override
  int get quantity;
  @override
  @JsonKey(ignore: true)
  _$$ProductOrderDtoImplCopyWith<_$ProductOrderDtoImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
