import 'package:injectable/injectable.dart' hide Order;
import 'package:shop_app_mobile/features/order/data/data_source/remote/order_remote_data_source.dart';
import 'package:shop_app_mobile/features/order/domain/dto/order_create_dto.dart';
import 'package:shop_app_mobile/features/order/domain/models/order/order.dart';
import 'package:shop_app_mobile/features/order/domain/repository/order_repository.dart';

@Injectable(as: OrderRepository)
class OrderRepositoryImpl implements OrderRepository {
  final OrderRemoteDataSource _orderRemoteDataSource;

  OrderRepositoryImpl(this._orderRemoteDataSource);

  @override
  Future<Order> createOrder(OrderCreateDto dto) async {
    final response = await _orderRemoteDataSource.createOrder(dto);
    return Order.fromJson(response.data['data']);
  }

  @override
  Future<List<Order>> getOrders({String? userId, String? deviceId}) async {
    final response = await _orderRemoteDataSource.getOrders(
        userId: userId, deviceId: deviceId);

    return (response.data['data'] as List)
        .map((e) => Order.fromJson(e))
        .toList();
  }

  @override
  Future<List<Order>> getAdminOrder() async {
    final response = await _orderRemoteDataSource.getAdminOrder();

    return (response.data['data'] as List)
        .map((e) => Order.fromJson(e))
        .toList();
  }
}
