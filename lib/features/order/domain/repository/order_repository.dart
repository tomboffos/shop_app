import 'package:shop_app_mobile/features/order/domain/dto/order_create_dto.dart';
import 'package:shop_app_mobile/features/order/domain/models/order/order.dart';

abstract class OrderRepository {
  Future<Order> createOrder(OrderCreateDto dto);

  Future<List<Order>> getOrders({String? userId, String? deviceId});

  Future<List<Order>> getAdminOrder();
}
