import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart' hide Order;
import 'package:shop_app_mobile/features/order/domain/models/order/order.dart';
import 'package:shop_app_mobile/features/order/domain/repository/order_repository.dart';

part 'orders_state.dart';
part 'orders_cubit.freezed.dart';

@injectable
class OrdersCubit extends Cubit<OrdersState> {
  final OrderRepository _orderRepository;

  OrdersCubit(this._orderRepository) : super(const OrdersState.initial());

  void getOrders({String? userId, String? deviceId}) async {
    try {
      final orders =
          await _orderRepository.getOrders(userId: userId, deviceId: deviceId);

      emit(OrdersState.loaded(orders));
    } on DioException catch (e) {
      emit(OrdersState.error(e.response?.statusMessage ?? ''));
    }
  }

  void getAdminOrder() async {
    try {
      final orders = await _orderRepository.getAdminOrder();

      emit(OrdersState.loaded(orders));
    } on DioException catch (e) {
      emit(OrdersState.error(e.response?.statusMessage ?? ''));
    }
  }
}
