import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shop_app_mobile/core/injection/injection.dart';
import 'package:shop_app_mobile/features/order/presentation/orders_cubit/orders_cubit.dart';
import 'package:shop_app_mobile/features/order/presentation/widgets/order_item.dart';

@RoutePage()
class OrdersPage extends StatefulWidget {
  final bool admin;
  const OrdersPage({super.key, this.admin = false});

  @override
  State<OrdersPage> createState() => _OrdersPageState();
}

class _OrdersPageState extends State<OrdersPage> {
  final _ordersCubit = getIt.get<OrdersCubit>();

  @override
  void initState() {
    if (widget.admin) {
      _ordersCubit.getAdminOrder();
    } else {
      _ordersCubit.getOrders();
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<OrdersCubit, OrdersState>(
      bloc: _ordersCubit,
      builder: (context, state) {
        return state.when(
          initial: () => const Center(
            child: CircularProgressIndicator(),
          ),
          loaded: (orders) => ListView.builder(
            itemBuilder: (context, index) => Padding(
              padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
              child: OrderItem(
                order: orders[index],
                admin: widget.admin,
              ),
            ),
            itemCount: orders.length,
          ),
          error: (message) => const Center(child: Text('Произошла ошибка')),
        );
      },
    );
  }
}
