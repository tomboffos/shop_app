import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:shop_app_mobile/core/injection/injection.dart';
import 'package:shop_app_mobile/core/utils/constants/gaps.dart';
import 'package:shop_app_mobile/core/utils/constants/insets.dart';
import 'package:shop_app_mobile/core/utils/constants/validator.dart';
import 'package:shop_app_mobile/core/utils/extensions/string_extension.dart';
import 'package:shop_app_mobile/features/cart/presentation/cubit/cart_cubit.dart';
import 'package:shop_app_mobile/features/cart/presentation/widgets/cart_item_widget.dart';
import 'package:shop_app_mobile/features/order/domain/dto/order_create_dto.dart';
import 'package:shop_app_mobile/features/order/domain/dto/product_order_dto/product_order_dto.dart';
import 'package:shop_app_mobile/features/order/presentation/order_cubit/order_create_cubit.dart';
import 'package:shop_app_mobile/features/order/presentation/widgets/order_success_loaded.dart';
import 'package:shop_app_mobile/features/profile/presentation/cubit/user_cubit.dart';
import 'package:shop_app_mobile/features/settings/presentation/cubit/setting_cubit.dart';

@RoutePage()
class OrderCreatePage extends StatefulWidget {
  const OrderCreatePage({super.key});

  @override
  State<OrderCreatePage> createState() => _OrderCreatePageState();
}

class _OrderCreatePageState extends State<OrderCreatePage> {
  final _cartCubit = getIt.get<CartCubit>();

  final _formKey = GlobalKey<FormState>();
  final _orderCubit = getIt.get<OrderCreateCubit>();

  final _nameController = TextEditingController();
  final _phoneController = TextEditingController();
  final _addressController = TextEditingController();
  final _commentController = TextEditingController();

  var maskFormatter = MaskTextInputFormatter(
    mask: '+# (###) ###-##-##',
    filter: {"#": RegExp(r'[0-9]')},
    type: MaskAutoCompletionType.lazy,
  );

  @override
  void initState() {
    getIt.get<UserCubit>().state.whenOrNull(
      authorized: (user) {
        _nameController.text = user.name;
      },
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<UserCubit, UserState>(
      bloc: getIt.get<UserCubit>(),
      builder: (context, userState) {
        return BlocBuilder<CartCubit, CartState>(
          bloc: _cartCubit,
          builder: (context, state) {
            return state.when(
              initial: () => const Center(
                child: CircularProgressIndicator(),
              ),
              loaded: (items) {
                var sum = items.fold<double>(
                    0,
                    (previousValue, element) =>
                        previousValue +
                        (element.product.price * element.count));

                return BlocBuilder<OrderCreateCubit, OrderCreateState>(
                  bloc: _orderCubit,
                  builder: (context, orderState) {
                    return Form(
                        onChanged: () {
                          _formKey.currentState?.validate();
                        },
                        key: _formKey,
                        child: Scaffold(
                          bottomNavigationBar: orderState.maybeWhen(
                            created: (order) => null,
                            orElse: () => FilledButton(
                              onPressed: () async {
                                if (_formKey.currentState?.validate() ??
                                    false) {
                                  final userId = userState.maybeWhen(
                                      orElse: () => null,
                                      authorized: (user) => user.id);

                                  _orderCubit.createOrder(
                                    OrderCreateDto(
                                      products: items
                                          .map(
                                            (e) => ProductOrderDto(
                                                productId: e.product.id,
                                                quantity: e.count),
                                          )
                                          .toList(),
                                      name: _nameController.text,
                                      phone: _phoneController.text,
                                      address: _addressController.text,
                                      comments: _commentController.text,
                                      userId: userId,
                                    ),
                                  );

                                  _cartCubit.removeAll();
                                }
                              },
                              style: const ButtonStyle(
                                shape: MaterialStatePropertyAll(
                                  RoundedRectangleBorder(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(0)),
                                  ),
                                ),
                              ),
                              child: Padding(
                                padding: Insets.dimens2x,
                                child: const Text(
                                  'В корзину',
                                ),
                              ),
                            ),
                          ),
                          body: SingleChildScrollView(
                            child: BlocBuilder<SettingCubit, SettingState>(
                              bloc: getIt.get<SettingCubit>(),
                              builder: (context, settingState) {
                                return orderState.maybeWhen(
                                  loading: () =>
                                      const CircularProgressIndicator(),
                                  created: (order) =>
                                      OrderSuccessLoaded(order: order),
                                  orElse: () => Card(
                                    margin: const EdgeInsets.all(16),
                                    child: Column(
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.symmetric(
                                              vertical: 12, horizontal: 16),
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Text(
                                                'Ваши товары',
                                                style: Theme.of(context)
                                                    .textTheme
                                                    .headlineMedium,
                                              ),
                                              Gaps.gap,
                                              ListView.builder(
                                                padding: EdgeInsets.zero,
                                                itemBuilder: (context, index) =>
                                                    CartItemWidget(
                                                  item: items[index],
                                                ),
                                                itemCount: items.length,
                                                shrinkWrap: true,
                                                physics:
                                                    const NeverScrollableScrollPhysics(),
                                              )
                                            ],
                                          ),
                                        ),
                                        const Divider(
                                          thickness: 3,
                                        ),
                                        Padding(
                                          padding: Insets.dimens2x,
                                          child: ListTile(
                                            title: Text(
                                              'Сумма заказов:',
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .bodyLarge,
                                            ),
                                            trailing: Text(
                                              items
                                                  .fold<double>(
                                                      0,
                                                      (previousValue,
                                                              element) =>
                                                          previousValue +
                                                          (element.product
                                                                  .price *
                                                              element.count))
                                                  .toCurrency(),
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .bodyLarge,
                                            ),
                                          ),
                                        ),
                                        Padding(
                                          padding:
                                              Insets.dimens2x.copyWith(top: 0),
                                          child: settingState.when(
                                            initial: () =>
                                                const SizedBox.shrink(),
                                            loaded: (settings) =>
                                                settings.deliveryPrice != null
                                                    ? ListTile(
                                                        title: Text(
                                                          'Сумма доставки:',
                                                          style:
                                                              Theme.of(context)
                                                                  .textTheme
                                                                  .bodyLarge,
                                                        ),
                                                        trailing: Text(
                                                          double.tryParse(settings
                                                                      .deliveryPrice!)
                                                                  ?.toCurrency() ??
                                                              '',
                                                          style:
                                                              Theme.of(context)
                                                                  .textTheme
                                                                  .bodyLarge,
                                                        ),
                                                      )
                                                    : const SizedBox.shrink(),
                                          ),
                                        ),
                                        Padding(
                                          padding:
                                              Insets.dimens2x.copyWith(top: 0),
                                          child: settingState.when(
                                            initial: () =>
                                                const SizedBox.shrink(),
                                            loaded: (settings) =>
                                                settings.deliveryPrice != null
                                                    ? ListTile(
                                                        title: Text(
                                                          'Итого: ',
                                                          style:
                                                              Theme.of(context)
                                                                  .textTheme
                                                                  .bodyLarge,
                                                        ),
                                                        trailing: Text(
                                                          (sum +
                                                                  (double.tryParse(
                                                                          settings.deliveryPrice ??
                                                                              '0') ??
                                                                      0))
                                                              .toCurrency(),
                                                          style:
                                                              Theme.of(context)
                                                                  .textTheme
                                                                  .bodyLarge,
                                                        ),
                                                      )
                                                    : const SizedBox.shrink(),
                                          ),
                                        ),
                                        const Divider(
                                          thickness: 3,
                                        ),
                                        Padding(
                                          padding: Insets.dimens2x,
                                          child: Column(
                                            children: [
                                              TextFormField(
                                                controller: _nameController,
                                                decoration:
                                                    const InputDecoration(
                                                  hintText: 'Имя',
                                                ),
                                                validator: Validator.required,
                                              ),
                                              Gaps.gap2,
                                              TextFormField(
                                                controller: _addressController,
                                                decoration:
                                                    const InputDecoration(
                                                  hintText: 'Адрес',
                                                ),
                                                validator: Validator.required,
                                              ),
                                              Gaps.gap2,
                                              TextFormField(
                                                controller: _phoneController,
                                                decoration:
                                                    const InputDecoration(
                                                  hintText: 'Телефон',
                                                ),
                                                inputFormatters: [
                                                  maskFormatter,
                                                ],
                                                validator: Validator.required,
                                              ),
                                              Gaps.gap2,
                                              TextFormField(
                                                controller: _commentController,
                                                decoration:
                                                    const InputDecoration(
                                                  hintText: 'Комментарии',
                                                ),
                                                maxLines: 4,
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                );
                              },
                            ),
                          ),
                        ));
                  },
                );
              },
            );
          },
        );
      },
    );
  }
}
