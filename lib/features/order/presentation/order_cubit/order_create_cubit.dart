import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart' hide Order;
import 'package:shop_app_mobile/features/order/domain/dto/order_create_dto.dart';
import 'package:shop_app_mobile/features/order/domain/models/order/order.dart';
import 'package:shop_app_mobile/features/order/domain/repository/order_repository.dart';

part 'order_create_state.dart';
part 'order_create_cubit.freezed.dart';

@injectable
class OrderCreateCubit extends Cubit<OrderCreateState> {
  final OrderRepository _orderRepository;

  OrderCreateCubit(this._orderRepository)
      : super(const OrderCreateState.initial());

  void createOrder(OrderCreateDto dto) async {
    try {
      final order = await _orderRepository.createOrder(dto);

      emit(OrderCreateState.created(order));
    } on DioException catch (e, _) {
      emit(OrderCreateState.error(e.response.toString()));
    } on Exception {
      emit(const OrderCreateState.error('Неизвестная ошибка '));
    }
  }
}
