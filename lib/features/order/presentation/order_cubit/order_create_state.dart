part of 'order_create_cubit.dart';

@freezed
class OrderCreateState with _$OrderCreateState {
  const factory OrderCreateState.initial() = _Initial;
  const factory OrderCreateState.loading() = _Loading;
  const factory OrderCreateState.created(Order order) = _Created;
  const factory OrderCreateState.error(String message) = _Error;
}
