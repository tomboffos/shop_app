import 'package:auto_route/auto_route.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:shop_app_mobile/core/routing/app_router.dart';
import 'package:shop_app_mobile/core/utils/constants/insets.dart';
import 'package:shop_app_mobile/core/utils/extensions/string_extension.dart';
import 'package:shop_app_mobile/features/order/domain/models/order/order.dart';

class OrderItem extends StatelessWidget {
  final Order order;
  final bool admin;

  const OrderItem({
    super.key,
    required this.order,
    this.admin = false,
  });

  @override
  Widget build(BuildContext context) {
    return ExpansionTile(
      title: Text(
        'Заказ номер: ${order.id}',
        style: Theme.of(context).textTheme.bodyLarge,
      ),
      trailing: Text(
        'Сумма заказа ${order.price.toCurrency()}',
        style: Theme.of(context).textTheme.bodyLarge,
      ),
      initiallyExpanded: true,
      expandedCrossAxisAlignment: CrossAxisAlignment.start,
      children: [
        if (admin)
          Padding(
            padding: Insets.dimens.copyWith(
              left: 16,
              right: 16,
              bottom: 0,
              top: 0,
            ),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const Text('Номер телефона'),
                    Text('${order.phone}'),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const Text('Имя'),
                    Text('${order.name}'),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const Text('Дата заказа'),
                    Text(
                        DateFormat('dd.MM.yyyy HH:mm').format(order.createdAt)),
                  ],
                ),
              ],
            ),
          ),
        ...order.products
            .map(
              (e) => InkWell(
                onTap: () => context.router.push(
                  ProductDetailRoute(
                    product: e.product,
                  ),
                ),
                child: ListTile(
                  contentPadding: const EdgeInsets.symmetric(
                    vertical: 20,
                    horizontal: 10,
                  ),
                  title: Text(
                      '${e.product.name} ${e.quantity} ${e.product.perPrice.name} : ${(e.product.price * e.quantity).toCurrency()} '),
                  leading: e.product.medias.isNotEmpty
                      ? CachedNetworkImage(
                          imageUrl: e.product.medias.first.image,
                          width: 50,
                          fit: BoxFit.contain,
                        )
                      : const SizedBox.shrink(),
                  trailing: SizedBox(
                    width: MediaQuery.of(context).size.width * 0.45,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Text(
                          '${e.quantity}',
                          style: Theme.of(context).textTheme.bodyLarge,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            )
            .toList()
      ],
    );
  }
}
