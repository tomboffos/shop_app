import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:shop_app_mobile/core/utils/constants/gaps.dart';
import 'package:shop_app_mobile/core/utils/constants/insets.dart';
import 'package:shop_app_mobile/features/order/domain/models/order/order.dart';

class OrderSuccessLoaded extends StatefulWidget {
  final Order order;

  const OrderSuccessLoaded({super.key, required this.order});

  @override
  State<OrderSuccessLoaded> createState() => _OrderSuccessLoadedState();
}

class _OrderSuccessLoadedState extends State<OrderSuccessLoaded> {
  @override
  Widget build(BuildContext context) {
    return Card(
      margin: Insets.dimens2x,
      child: Container(
        padding: Insets.dimens2x,
        width: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              decoration: BoxDecoration(
                color: Colors.green,
                borderRadius: BorderRadius.circular(100),
              ),
              padding: Insets.dimens2x,
              child: const Icon(
                Icons.check,
                size: 60,
              ),
            ),
            Gaps.gap2,
            Text(
              'Ваш заказ подтвержден',
              style: Theme.of(context).textTheme.headlineMedium,
            ),
            Gaps.gap2,
            FilledButton(
              onPressed: () {
                context.router.popUntilRoot();
              },
              child: Text(
                'Вернуться к покупкам',
                style: Theme.of(context).textTheme.bodyMedium,
              ),
            )
          ],
        ),
      ),
    );
  }
}
