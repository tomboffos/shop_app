import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:shop_app_mobile/features/order/data/data_source/remote/order_remote_data_source.dart';
import 'package:shop_app_mobile/features/order/domain/dto/order_create_dto.dart';

@Injectable(as: OrderRemoteDataSource)
class OrderRemoteDataSourceImpl implements OrderRemoteDataSource {
  final Dio _dio;

  OrderRemoteDataSourceImpl(this._dio) {
    _dio.options.baseUrl += OrderRemoteDataSource.shop;
  }

  @override
  Future<Response> createOrder(OrderCreateDto dto) =>
      _dio.post(OrderRemoteDataSource.order, data: dto.toJson());

  @override
  Future<Response> getOrders({
    String? userId,
    String? deviceId,
  }) =>
      _dio.get(
        OrderRemoteDataSource.order,
        queryParameters: {
          "user_id": userId,
          "device_id": deviceId,
        },
      );

  @override
  Future<Response> getAdminOrder() =>
      _dio.get(OrderRemoteDataSource.orderAdmin);
}
