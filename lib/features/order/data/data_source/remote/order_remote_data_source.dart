import 'package:dio/dio.dart';
import 'package:shop_app_mobile/features/order/domain/dto/order_create_dto.dart';

abstract class OrderRemoteDataSource {
  static String shop = '/shop';
  static String order = '/order';
  static String orderAdmin = '/order/admin';

  Future<Response> createOrder(OrderCreateDto dto);

  Future<Response> getOrders({
    String? userId,
    String? deviceId,
  });

  Future<Response> getAdminOrder();
}
