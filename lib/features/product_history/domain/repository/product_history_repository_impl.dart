import 'package:shop_app_mobile/features/product/domain/models/product.dart';
import 'package:shop_app_mobile/features/product_history/data/data_source/product_history_remote_data_source.dart';
import 'package:shop_app_mobile/features/product_history/domain/dto/product_history_create_dto.dart';
import 'package:shop_app_mobile/features/product_history/domain/repository/product_history_repository.dart';
import 'package:injectable/injectable.dart';

@Injectable(as: ProductHistoryRepository)
class ProductHistoryRepositoryImpl implements ProductHistoryRepository {
  final ProductHistoryRemoteDataSource _productHistoryRemoteDataSource;

  ProductHistoryRepositoryImpl(this._productHistoryRemoteDataSource);

  @override
  Future<void> addHistory(ProductHistoryCreateDto dto) =>
      _productHistoryRemoteDataSource.addHistory(dto);

  @override
  Future<List<Product>> getProducts(int deviceId) async {
    final response = await _productHistoryRemoteDataSource.getHistory(deviceId);

    return (response.data['data'] as List)
        .map((e) => Product.fromJson(e))
        .toList();
  }
}
