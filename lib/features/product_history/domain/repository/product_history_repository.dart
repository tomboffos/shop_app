import 'package:shop_app_mobile/features/product/domain/models/product.dart';
import 'package:shop_app_mobile/features/product_history/domain/dto/product_history_create_dto.dart';

abstract class ProductHistoryRepository {
  Future<List<Product>> getProducts(int deviceId);

  Future<void> addHistory(ProductHistoryCreateDto dto);
}
