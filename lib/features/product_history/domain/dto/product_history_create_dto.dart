import 'package:freezed_annotation/freezed_annotation.dart';

part 'product_history_create_dto.g.dart';
part 'product_history_create_dto.freezed.dart';

@freezed
class ProductHistoryCreateDto with _$ProductHistoryCreateDto {
  factory ProductHistoryCreateDto({
    @JsonKey(name: 'product_id') required int productId,
    @JsonKey(name: 'device_id') required int deviceId,
  }) = _ProductHistoryCreateDto;

  factory ProductHistoryCreateDto.fromJson(Map<String, dynamic> json) =>
      _$ProductHistoryCreateDtoFromJson(json);
}
