// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'product_history_create_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$ProductHistoryCreateDtoImpl _$$ProductHistoryCreateDtoImplFromJson(
        Map<String, dynamic> json) =>
    _$ProductHistoryCreateDtoImpl(
      productId: json['product_id'] as int,
      deviceId: json['device_id'] as int,
    );

Map<String, dynamic> _$$ProductHistoryCreateDtoImplToJson(
        _$ProductHistoryCreateDtoImpl instance) =>
    <String, dynamic>{
      'product_id': instance.productId,
      'device_id': instance.deviceId,
    };
