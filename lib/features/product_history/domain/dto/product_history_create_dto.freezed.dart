// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'product_history_create_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

ProductHistoryCreateDto _$ProductHistoryCreateDtoFromJson(
    Map<String, dynamic> json) {
  return _ProductHistoryCreateDto.fromJson(json);
}

/// @nodoc
mixin _$ProductHistoryCreateDto {
  @JsonKey(name: 'product_id')
  int get productId => throw _privateConstructorUsedError;
  @JsonKey(name: 'device_id')
  int get deviceId => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ProductHistoryCreateDtoCopyWith<ProductHistoryCreateDto> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ProductHistoryCreateDtoCopyWith<$Res> {
  factory $ProductHistoryCreateDtoCopyWith(ProductHistoryCreateDto value,
          $Res Function(ProductHistoryCreateDto) then) =
      _$ProductHistoryCreateDtoCopyWithImpl<$Res, ProductHistoryCreateDto>;
  @useResult
  $Res call(
      {@JsonKey(name: 'product_id') int productId,
      @JsonKey(name: 'device_id') int deviceId});
}

/// @nodoc
class _$ProductHistoryCreateDtoCopyWithImpl<$Res,
        $Val extends ProductHistoryCreateDto>
    implements $ProductHistoryCreateDtoCopyWith<$Res> {
  _$ProductHistoryCreateDtoCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? productId = null,
    Object? deviceId = null,
  }) {
    return _then(_value.copyWith(
      productId: null == productId
          ? _value.productId
          : productId // ignore: cast_nullable_to_non_nullable
              as int,
      deviceId: null == deviceId
          ? _value.deviceId
          : deviceId // ignore: cast_nullable_to_non_nullable
              as int,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$ProductHistoryCreateDtoImplCopyWith<$Res>
    implements $ProductHistoryCreateDtoCopyWith<$Res> {
  factory _$$ProductHistoryCreateDtoImplCopyWith(
          _$ProductHistoryCreateDtoImpl value,
          $Res Function(_$ProductHistoryCreateDtoImpl) then) =
      __$$ProductHistoryCreateDtoImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {@JsonKey(name: 'product_id') int productId,
      @JsonKey(name: 'device_id') int deviceId});
}

/// @nodoc
class __$$ProductHistoryCreateDtoImplCopyWithImpl<$Res>
    extends _$ProductHistoryCreateDtoCopyWithImpl<$Res,
        _$ProductHistoryCreateDtoImpl>
    implements _$$ProductHistoryCreateDtoImplCopyWith<$Res> {
  __$$ProductHistoryCreateDtoImplCopyWithImpl(
      _$ProductHistoryCreateDtoImpl _value,
      $Res Function(_$ProductHistoryCreateDtoImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? productId = null,
    Object? deviceId = null,
  }) {
    return _then(_$ProductHistoryCreateDtoImpl(
      productId: null == productId
          ? _value.productId
          : productId // ignore: cast_nullable_to_non_nullable
              as int,
      deviceId: null == deviceId
          ? _value.deviceId
          : deviceId // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$ProductHistoryCreateDtoImpl implements _ProductHistoryCreateDto {
  _$ProductHistoryCreateDtoImpl(
      {@JsonKey(name: 'product_id') required this.productId,
      @JsonKey(name: 'device_id') required this.deviceId});

  factory _$ProductHistoryCreateDtoImpl.fromJson(Map<String, dynamic> json) =>
      _$$ProductHistoryCreateDtoImplFromJson(json);

  @override
  @JsonKey(name: 'product_id')
  final int productId;
  @override
  @JsonKey(name: 'device_id')
  final int deviceId;

  @override
  String toString() {
    return 'ProductHistoryCreateDto(productId: $productId, deviceId: $deviceId)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ProductHistoryCreateDtoImpl &&
            (identical(other.productId, productId) ||
                other.productId == productId) &&
            (identical(other.deviceId, deviceId) ||
                other.deviceId == deviceId));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, productId, deviceId);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$ProductHistoryCreateDtoImplCopyWith<_$ProductHistoryCreateDtoImpl>
      get copyWith => __$$ProductHistoryCreateDtoImplCopyWithImpl<
          _$ProductHistoryCreateDtoImpl>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$ProductHistoryCreateDtoImplToJson(
      this,
    );
  }
}

abstract class _ProductHistoryCreateDto implements ProductHistoryCreateDto {
  factory _ProductHistoryCreateDto(
          {@JsonKey(name: 'product_id') required final int productId,
          @JsonKey(name: 'device_id') required final int deviceId}) =
      _$ProductHistoryCreateDtoImpl;

  factory _ProductHistoryCreateDto.fromJson(Map<String, dynamic> json) =
      _$ProductHistoryCreateDtoImpl.fromJson;

  @override
  @JsonKey(name: 'product_id')
  int get productId;
  @override
  @JsonKey(name: 'device_id')
  int get deviceId;
  @override
  @JsonKey(ignore: true)
  _$$ProductHistoryCreateDtoImplCopyWith<_$ProductHistoryCreateDtoImpl>
      get copyWith => throw _privateConstructorUsedError;
}
