import 'package:shop_app_mobile/features/product_history/domain/dto/product_history_create_dto.dart';
import 'package:dio/dio.dart';

abstract class ProductHistoryRemoteDataSource {
  static String productHistory = '/product-history';
  static String productHistoryAdd = '/add';

  Future<Response> getHistory(int deviceId);

  Future<Response> addHistory(ProductHistoryCreateDto dto);
}
