import 'package:shop_app_mobile/features/category/data/data_source/category_remote_data_source.dart';
import 'package:shop_app_mobile/features/product_history/data/data_source/product_history_remote_data_source.dart';
import 'package:shop_app_mobile/features/product_history/domain/dto/product_history_create_dto.dart';
import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';

@Injectable(as: ProductHistoryRemoteDataSource)
class ProductHistoryRemoteDataSourceImpl
    implements ProductHistoryRemoteDataSource {
  final Dio _dio;

  ProductHistoryRemoteDataSourceImpl(this._dio) {
    _dio.options.baseUrl += CategoryRemoteDataSource.shop +
        ProductHistoryRemoteDataSource.productHistory;
  }

  @override
  Future<Response> addHistory(ProductHistoryCreateDto dto) => _dio.post(
        ProductHistoryRemoteDataSource.productHistoryAdd,
        data: dto.toJson(),
      );

  @override
  Future<Response> getHistory(int deviceId) =>
      _dio.get(ProductHistoryRemoteDataSource.productHistory + '/$deviceId');
}
