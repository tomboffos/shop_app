part of 'product_history_cubit.dart';

@freezed
class ProductHistoryState with _$ProductHistoryState {
  const factory ProductHistoryState.initial() = _Initial;
  const factory ProductHistoryState.loaded(List<Product> products) = _Loaded;
}
