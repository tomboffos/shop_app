import 'package:shop_app_mobile/features/product/domain/models/product.dart';
import 'package:shop_app_mobile/features/product_history/domain/dto/product_history_create_dto.dart';
import 'package:shop_app_mobile/features/product_history/domain/repository/product_history_repository.dart';
import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';

part 'product_history_state.dart';
part 'product_history_cubit.freezed.dart';

@injectable
class ProductHistoryCubit extends Cubit<ProductHistoryState> {
  final ProductHistoryRepository _productHistoryRepository;

  ProductHistoryCubit(this._productHistoryRepository)
      : super(const ProductHistoryState.initial());

  Future<void> getHistory(int deviceId) async {
    try {
      final history = await _productHistoryRepository.getProducts(deviceId);

      emit(ProductHistoryState.loaded(history));
    } on Exception {}
  }

  Future<void> addHistory(ProductHistoryCreateDto dto) =>
      _productHistoryRepository.addHistory(dto);
}
