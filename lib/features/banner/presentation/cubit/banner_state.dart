part of 'banner_cubit.dart';

@freezed
class BannerState with _$BannerState {
  const factory BannerState.initial() = _Initial;

  const factory BannerState.loaded(List<Banner> banners) = _Banner;
}
