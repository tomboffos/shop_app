import 'package:shop_app_mobile/features/banner/domain/models/banner/banner.dart';
import 'package:shop_app_mobile/features/banner/domain/repository/banner_repository.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter/foundation.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';

part 'banner_state.dart';
part 'banner_cubit.freezed.dart';

@injectable
class BannerCubit extends Cubit<BannerState> {
  final BannerRepository _bannerRepository;

  BannerCubit(this._bannerRepository) : super(BannerState.initial());

  Future<void> getBanners() async {
    final banners = await _bannerRepository.getBanners();

    emit(BannerState.loaded(banners));
  }
}
