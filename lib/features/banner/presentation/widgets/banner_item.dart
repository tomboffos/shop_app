import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart' hide Banner;
import 'package:shop_app_mobile/core/utils/constants/gaps.dart';
import 'package:shop_app_mobile/core/utils/extensions/theme_extension.dart';
import 'package:shop_app_mobile/core/utils/services/launch.dart';
import 'package:shop_app_mobile/features/banner/domain/models/banner/banner.dart';

class BannerItem extends StatelessWidget {
  final Banner banner;

  const BannerItem({super.key, required this.banner});

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(20),
      child: InkWell(
        onTap: () => Launcher.onTapLink(banner.link),
        child: Card(
          child: Column(
            children: [
              if (banner.image != null)
                CachedNetworkImage(
                  width: 145,
                  fit: BoxFit.cover,
                  imageUrl: banner.image!,
                ),
              Gaps.gap2,
              Text(
                banner.name ?? '',
                style: context.text.headlineSmall,
              ),
              Gaps.gap05x,
              FilledButton(
                onPressed: () => Launcher.onTapLink(banner.link),
                child: const Text('Подробнее'),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
