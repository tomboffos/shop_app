import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shop_app_mobile/core/injection/injection.dart';
import 'package:shop_app_mobile/core/utils/constants/gaps.dart';
import 'package:shop_app_mobile/core/utils/constants/insets.dart';
import 'package:shop_app_mobile/core/utils/extensions/theme_extension.dart';
import 'package:shop_app_mobile/features/banner/presentation/cubit/banner_cubit.dart';
import 'package:shop_app_mobile/features/banner/presentation/widgets/banner_item.dart';

class BannerSlider extends StatefulWidget {
  const BannerSlider({super.key});

  @override
  State<BannerSlider> createState() => _BannerSliderState();
}

class _BannerSliderState extends State<BannerSlider> {
  final _bannerCubit = getIt.get<BannerCubit>();

  @override
  void initState() {
    _bannerCubit.getBanners();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<BannerCubit, BannerState>(
      bloc: _bannerCubit,
      builder: (context, state) {
        return state.maybeWhen(
          orElse: () => const SizedBox.shrink(),
          loaded: (banners) => banners.isNotEmpty
              ? Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: Insets.dimens2x.copyWith(bottom: 0),
                      child: Text(
                        'Новые товары',
                        style: context.text.headlineLarge,
                      ),
                    ),
                    Gaps.gap2,
                    SizedBox(
                      height: MediaQuery.of(context).size.height / 3.7,
                      child: ListView.builder(
                        shrinkWrap: true,
                        scrollDirection: Axis.horizontal,
                        itemCount: banners.length,
                        itemBuilder: (BuildContext context, int index) =>
                            Padding(
                          padding: Insets.dimens2x
                              .copyWith(bottom: 0, top: 0, right: 0),
                          child: BannerItem(
                            banner: banners[index],
                          ),
                        ),
                      ),
                    ),
                  ],
                )
              : const SizedBox.shrink(),
        );
      },
    );
  }

  @override
  void dispose() {
    _bannerCubit.close();
    super.dispose();
  }
}
