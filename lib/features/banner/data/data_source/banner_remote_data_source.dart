import 'package:dio/dio.dart';

abstract class BannerRemoteDataSource {
  static String advertisement = '/advertisement';

  static String banners = '/banner';

  Future<Response> getBanners();
}
