import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:shop_app_mobile/features/banner/data/data_source/banner_remote_data_source.dart';

@Injectable(as: BannerRemoteDataSource)
class BannerRemoteDataSourceImpl implements BannerRemoteDataSource {
  final Dio _dio;

  BannerRemoteDataSourceImpl(this._dio) {
    _dio.options.baseUrl += BannerRemoteDataSource.advertisement;
  }

  @override
  Future<Response> getBanners() => _dio.get(BannerRemoteDataSource.banners);
}
