import 'package:shop_app_mobile/features/banner/domain/models/banner/banner.dart';

abstract class BannerRepository {
  Future<List<Banner>> getBanners();
}
