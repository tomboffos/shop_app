import 'package:injectable/injectable.dart';
import 'package:shop_app_mobile/features/banner/data/data_source/banner_remote_data_source.dart';
import 'package:shop_app_mobile/features/banner/domain/models/banner/banner.dart';
import 'package:shop_app_mobile/features/banner/domain/repository/banner_repository.dart';

@Injectable(as: BannerRepository)
class BannerRepositoryImpl implements BannerRepository {
  final BannerRemoteDataSource _bannerRemoteDataSource;

  BannerRepositoryImpl(this._bannerRemoteDataSource);

  @override
  Future<List<Banner>> getBanners() async {
    final response = await _bannerRemoteDataSource.getBanners();

    return (response.data['data'] as List)
        .map((e) => Banner.fromJson(e))
        .toList();
  }
}
