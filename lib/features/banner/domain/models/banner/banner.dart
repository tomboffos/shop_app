import 'package:freezed_annotation/freezed_annotation.dart';

part 'banner.g.dart';
part 'banner.freezed.dart';

@freezed
class Banner with _$Banner {
  factory Banner({
    String? name,
    String? image,
    String? link,
  }) = _Banner;

  factory Banner.fromJson(Map<String, dynamic> json) => _$BannerFromJson(json);
}
