part of 'favorites_cubit.dart';

@freezed
class FavoritesState with _$FavoritesState {
  const factory FavoritesState.loaded({
    @Default([]) List<Product> products,
  }) = _Loaded;
}
