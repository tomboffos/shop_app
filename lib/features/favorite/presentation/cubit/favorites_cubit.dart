import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:shop_app_mobile/features/favorite/domain/repository/favorte_repository.dart';
import 'package:shop_app_mobile/features/product/domain/models/product.dart';

part 'favorites_state.dart';
part 'favorites_cubit.freezed.dart';

@singleton
class FavoritesCubit extends Cubit<FavoritesState> {
  final FavoriteRepository _favoriteRepository;

  FavoritesCubit(this._favoriteRepository)
      : super(const FavoritesState.loaded()) {
    emit(
      FavoritesState.loaded(
        products: _favoriteRepository.getProducts(),
      ),
    );
  }

  void getProducts() {
    emit(
      FavoritesState.loaded(
        products: _favoriteRepository.getProducts(),
      ),
    );
  }

  Future<void> addProduct(Product product) async {
    await _favoriteRepository.addProduct(product);

    getProducts();
  }

  Future<void> removeProduct(Product product) async {
    await _favoriteRepository.removeProduct(product);
    getProducts();
  }
}
