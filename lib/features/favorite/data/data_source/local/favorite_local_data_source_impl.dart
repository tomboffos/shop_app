import 'package:hive/hive.dart';
import 'package:injectable/injectable.dart';
import 'package:shop_app_mobile/features/favorite/data/data_source/local/favorite_local_data_source.dart';
import 'package:shop_app_mobile/features/product/domain/models/product.dart';

@Injectable(as: FavoriteLocalDataSource)
class FavoriteLocalDataSourceImpl implements FavoriteLocalDataSource {
  @override
  Future<void> addProduct(Product product) =>
      Hive.box(FavoriteLocalDataSource.favorite).add(product);

  @override
  List<Product> getProducts() => Hive.box(FavoriteLocalDataSource.favorite)
      .values
      .map((e) => e as Product)
      .toList();

  @override
  Future<void> removeProduct(Product product) =>
      Hive.box(FavoriteLocalDataSource.favorite).deleteAt(
        getProducts().indexOf(product),
      );
}
