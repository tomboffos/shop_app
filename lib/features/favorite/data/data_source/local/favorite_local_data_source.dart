import 'package:shop_app_mobile/features/product/domain/models/product.dart';

abstract class FavoriteLocalDataSource {
  static String favorite = 'favorites';

  Future<void> addProduct(Product product);

  List<Product> getProducts();

  Future<void> removeProduct(Product product);
}
