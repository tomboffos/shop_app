import 'package:injectable/injectable.dart';
import 'package:shop_app_mobile/features/favorite/data/data_source/local/favorite_local_data_source.dart';
import 'package:shop_app_mobile/features/favorite/domain/repository/favorte_repository.dart';
import 'package:shop_app_mobile/features/product/domain/models/product.dart';

@Injectable(as: FavoriteRepository)
class FavoriteRepositoryImpl implements FavoriteRepository {
  final FavoriteLocalDataSource _favoriteLocalDataSource;

  FavoriteRepositoryImpl(this._favoriteLocalDataSource);

  @override
  List<Product> getProducts() => _favoriteLocalDataSource.getProducts();

  @override
  Future<void> addProduct(Product product) =>
      _favoriteLocalDataSource.addProduct(product);

  @override
  Future<void> removeProduct(Product product) =>
      _favoriteLocalDataSource.removeProduct(product);
}
