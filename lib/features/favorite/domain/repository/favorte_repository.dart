import 'package:shop_app_mobile/features/product/domain/models/product.dart';

abstract class FavoriteRepository {
  List<Product> getProducts();

  Future<void> addProduct(Product product);

  Future<void> removeProduct(Product product);
}
