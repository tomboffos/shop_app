import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:image_picker/image_picker.dart';
import 'package:injectable/injectable.dart';
import 'package:shop_app_mobile/features/category/data/data_source/category_remote_data_source.dart';
import 'package:shop_app_mobile/features/category/domain/dto/category_create_update_dto.dart';

@Injectable(as: CategoryRemoteDataSource)
class CategoryRemoteDataSourceImpl implements CategoryRemoteDataSource {
  final Dio _dio;

  CategoryRemoteDataSourceImpl(this._dio) {
    _dio.options.baseUrl += CategoryRemoteDataSource.shop;
  }

  @override
  Future<Response> getCategories({int? categoryId}) => _dio.get(
        CategoryRemoteDataSource.categories +
            (categoryId != null ? '/$categoryId' : ''),
      );

  @override
  Future<Response> getAdmin({required String search}) => _dio.get(
        CategoryRemoteDataSource.categoriesAdmin,
        data: {'search': search},
      );

  @override
  Future<void> createCategory({
    required CategoryCreateUpdateDto dto,
    required XFile image,
  }) async {
    Map<String, dynamic> data = {
      "image": null,
    };
    data.addAll(dto.toJson());
    data.remove('products');

    int index = 0;
    for (var element in dto.fields) {
      data['fields[$index]'] = element;
      index++;
    }

    FormData formData = FormData.fromMap(data);
    String fileName = image.path.split('/').last;

    formData.files.add(
      MapEntry(
        'image',
        kIsWeb
            ? MultipartFile.fromBytes(
                await image.readAsBytes(),
                filename: fileName,
              )
            : await MultipartFile.fromFile(
                image.path,
                filename: fileName,
              ),
      ),
    );

    await _dio.post(
      CategoryRemoteDataSource.categoriesAdmin,
      data: formData,
    );
  }

  @override
  Future<void> updateCategory({
    required CategoryCreateUpdateDto dto,
    XFile? image,
    required int id,
  }) async {
    Map<String, dynamic> data = {
      "image": dto.image,
    };
    data.addAll(dto.toJson());
    data.remove('products');

    int index = 0;
    for (var element in dto.fields) {
      data['fields[$index]'] = element;
      index++;
    }

    FormData formData = FormData.fromMap(data);
    if (image != null) {
      String fileName = image.path.split('/').last;
      formData.files.add(
        MapEntry(
          'image',
          kIsWeb
              ? MultipartFile.fromBytes(
                  await image.readAsBytes(),
                  filename: fileName,
                )
              : await MultipartFile.fromFile(
                  image.path,
                  filename: fileName,
                ),
        ),
      );
    }

    await _dio.post(
      '${CategoryRemoteDataSource.categoriesAdmin}/$id',
      data: formData,
    );
  }

  @override
  Future<void> deleteCategory({required int id}) =>
      _dio.delete('${CategoryRemoteDataSource.categoriesAdmin}/$id');
}
