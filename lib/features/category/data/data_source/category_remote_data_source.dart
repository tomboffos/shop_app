import 'package:dio/dio.dart';
import 'package:image_picker/image_picker.dart';
import 'package:shop_app_mobile/features/category/domain/dto/category_create_update_dto.dart';

abstract class CategoryRemoteDataSource {
  static String shop = '/shop';

  static String categories = '/category';
  static String categoriesAdmin = '/category/admin';

  Future<Response> getCategories({
    int? categoryId,
  });

  Future<Response> getAdmin({required String search});

  Future<void> createCategory({
    required CategoryCreateUpdateDto dto,
    required XFile image,
  });

  Future<void> updateCategory({
    required CategoryCreateUpdateDto dto,
    XFile? image,
    required int id,
  });

  Future<void> deleteCategory({
    required int id,
  });
}
