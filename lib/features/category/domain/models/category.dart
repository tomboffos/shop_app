import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:hive/hive.dart';
import 'package:shop_app_mobile/features/filters/domain/models/custom_field.dart';
import 'package:shop_app_mobile/features/product/domain/models/product.dart';

part 'category.freezed.dart';
part 'category.g.dart';

@freezed
@HiveType(typeId: 2)
class Category with _$Category {
  factory Category({
    @HiveField(0) required int id,
    @HiveField(1) String? name,
    @HiveField(2) String? image,
    @HiveField(3) @Default(0) int children,
    @HiveField(4) @Default([]) List<Product>? products,
    @HiveField(5) @Default([]) List<CustomField>? fields,
  }) = _Category;

  factory Category.fromJson(Map<String, dynamic> json) =>
      _$CategoryFromJson(json);
}
