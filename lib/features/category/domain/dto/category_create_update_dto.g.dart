// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'category_create_update_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$CategoryCreateUpdateDtoImpl _$$CategoryCreateUpdateDtoImplFromJson(
        Map<String, dynamic> json) =>
    _$CategoryCreateUpdateDtoImpl(
      name: json['name'] as String,
      categoryId: json['category_id'] as int?,
      fields:
          (json['fields'] as List<dynamic>?)?.map((e) => e as int).toList() ??
              const [],
      image: json['image'] as String?,
    );

Map<String, dynamic> _$$CategoryCreateUpdateDtoImplToJson(
        _$CategoryCreateUpdateDtoImpl instance) =>
    <String, dynamic>{
      'name': instance.name,
      'category_id': instance.categoryId,
      'fields': instance.fields,
      'image': instance.image,
    };
