import 'package:freezed_annotation/freezed_annotation.dart';

part 'category_create_update_dto.g.dart';
part 'category_create_update_dto.freezed.dart';

@freezed
class CategoryCreateUpdateDto with _$CategoryCreateUpdateDto {
  factory CategoryCreateUpdateDto({
    required String name,
    @JsonKey(name: 'category_id') int? categoryId,
    @Default([]) @JsonKey(name: 'fields') List<int> fields,
    String? image,
  }) = _CategoryCreateUpdateDto;

  factory CategoryCreateUpdateDto.fromJson(Map<String, dynamic> json) =>
      _$CategoryCreateUpdateDtoFromJson(json);
}
