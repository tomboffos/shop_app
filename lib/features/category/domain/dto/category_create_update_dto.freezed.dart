// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'category_create_update_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

CategoryCreateUpdateDto _$CategoryCreateUpdateDtoFromJson(
    Map<String, dynamic> json) {
  return _CategoryCreateUpdateDto.fromJson(json);
}

/// @nodoc
mixin _$CategoryCreateUpdateDto {
  String get name => throw _privateConstructorUsedError;
  @JsonKey(name: 'category_id')
  int? get categoryId => throw _privateConstructorUsedError;
  @JsonKey(name: 'fields')
  List<int> get fields => throw _privateConstructorUsedError;
  String? get image => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $CategoryCreateUpdateDtoCopyWith<CategoryCreateUpdateDto> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CategoryCreateUpdateDtoCopyWith<$Res> {
  factory $CategoryCreateUpdateDtoCopyWith(CategoryCreateUpdateDto value,
          $Res Function(CategoryCreateUpdateDto) then) =
      _$CategoryCreateUpdateDtoCopyWithImpl<$Res, CategoryCreateUpdateDto>;
  @useResult
  $Res call(
      {String name,
      @JsonKey(name: 'category_id') int? categoryId,
      @JsonKey(name: 'fields') List<int> fields,
      String? image});
}

/// @nodoc
class _$CategoryCreateUpdateDtoCopyWithImpl<$Res,
        $Val extends CategoryCreateUpdateDto>
    implements $CategoryCreateUpdateDtoCopyWith<$Res> {
  _$CategoryCreateUpdateDtoCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? name = null,
    Object? categoryId = freezed,
    Object? fields = null,
    Object? image = freezed,
  }) {
    return _then(_value.copyWith(
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      categoryId: freezed == categoryId
          ? _value.categoryId
          : categoryId // ignore: cast_nullable_to_non_nullable
              as int?,
      fields: null == fields
          ? _value.fields
          : fields // ignore: cast_nullable_to_non_nullable
              as List<int>,
      image: freezed == image
          ? _value.image
          : image // ignore: cast_nullable_to_non_nullable
              as String?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$CategoryCreateUpdateDtoImplCopyWith<$Res>
    implements $CategoryCreateUpdateDtoCopyWith<$Res> {
  factory _$$CategoryCreateUpdateDtoImplCopyWith(
          _$CategoryCreateUpdateDtoImpl value,
          $Res Function(_$CategoryCreateUpdateDtoImpl) then) =
      __$$CategoryCreateUpdateDtoImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {String name,
      @JsonKey(name: 'category_id') int? categoryId,
      @JsonKey(name: 'fields') List<int> fields,
      String? image});
}

/// @nodoc
class __$$CategoryCreateUpdateDtoImplCopyWithImpl<$Res>
    extends _$CategoryCreateUpdateDtoCopyWithImpl<$Res,
        _$CategoryCreateUpdateDtoImpl>
    implements _$$CategoryCreateUpdateDtoImplCopyWith<$Res> {
  __$$CategoryCreateUpdateDtoImplCopyWithImpl(
      _$CategoryCreateUpdateDtoImpl _value,
      $Res Function(_$CategoryCreateUpdateDtoImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? name = null,
    Object? categoryId = freezed,
    Object? fields = null,
    Object? image = freezed,
  }) {
    return _then(_$CategoryCreateUpdateDtoImpl(
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      categoryId: freezed == categoryId
          ? _value.categoryId
          : categoryId // ignore: cast_nullable_to_non_nullable
              as int?,
      fields: null == fields
          ? _value._fields
          : fields // ignore: cast_nullable_to_non_nullable
              as List<int>,
      image: freezed == image
          ? _value.image
          : image // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$CategoryCreateUpdateDtoImpl implements _CategoryCreateUpdateDto {
  _$CategoryCreateUpdateDtoImpl(
      {required this.name,
      @JsonKey(name: 'category_id') this.categoryId,
      @JsonKey(name: 'fields') final List<int> fields = const [],
      this.image})
      : _fields = fields;

  factory _$CategoryCreateUpdateDtoImpl.fromJson(Map<String, dynamic> json) =>
      _$$CategoryCreateUpdateDtoImplFromJson(json);

  @override
  final String name;
  @override
  @JsonKey(name: 'category_id')
  final int? categoryId;
  final List<int> _fields;
  @override
  @JsonKey(name: 'fields')
  List<int> get fields {
    if (_fields is EqualUnmodifiableListView) return _fields;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_fields);
  }

  @override
  final String? image;

  @override
  String toString() {
    return 'CategoryCreateUpdateDto(name: $name, categoryId: $categoryId, fields: $fields, image: $image)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$CategoryCreateUpdateDtoImpl &&
            (identical(other.name, name) || other.name == name) &&
            (identical(other.categoryId, categoryId) ||
                other.categoryId == categoryId) &&
            const DeepCollectionEquality().equals(other._fields, _fields) &&
            (identical(other.image, image) || other.image == image));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, name, categoryId,
      const DeepCollectionEquality().hash(_fields), image);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$CategoryCreateUpdateDtoImplCopyWith<_$CategoryCreateUpdateDtoImpl>
      get copyWith => __$$CategoryCreateUpdateDtoImplCopyWithImpl<
          _$CategoryCreateUpdateDtoImpl>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$CategoryCreateUpdateDtoImplToJson(
      this,
    );
  }
}

abstract class _CategoryCreateUpdateDto implements CategoryCreateUpdateDto {
  factory _CategoryCreateUpdateDto(
      {required final String name,
      @JsonKey(name: 'category_id') final int? categoryId,
      @JsonKey(name: 'fields') final List<int> fields,
      final String? image}) = _$CategoryCreateUpdateDtoImpl;

  factory _CategoryCreateUpdateDto.fromJson(Map<String, dynamic> json) =
      _$CategoryCreateUpdateDtoImpl.fromJson;

  @override
  String get name;
  @override
  @JsonKey(name: 'category_id')
  int? get categoryId;
  @override
  @JsonKey(name: 'fields')
  List<int> get fields;
  @override
  String? get image;
  @override
  @JsonKey(ignore: true)
  _$$CategoryCreateUpdateDtoImplCopyWith<_$CategoryCreateUpdateDtoImpl>
      get copyWith => throw _privateConstructorUsedError;
}
