import 'package:image_picker/image_picker.dart';
import 'package:shop_app_mobile/features/category/domain/dto/category_create_update_dto.dart';
import 'package:shop_app_mobile/features/category/domain/models/category.dart';

abstract class CategoryRepository {
  Future<List<Category>> getCategories({int? categoryId});

  Future<List<Category>> getAdmin({required String search});

  Future<void> createCategory({
    required CategoryCreateUpdateDto dto,
    required XFile image,
  });

  Future<void> updateCategory({
    required CategoryCreateUpdateDto dto,
    XFile? image,
    required int id,
  });

  Future<void> deleteCategory({
    required int id,
  });
}
