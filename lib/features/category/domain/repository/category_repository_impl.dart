import 'package:image_picker/image_picker.dart';
import 'package:shop_app_mobile/features/category/data/data_source/category_remote_data_source.dart';
import 'package:shop_app_mobile/features/category/domain/dto/category_create_update_dto.dart';
import 'package:shop_app_mobile/features/category/domain/models/category.dart';
import 'package:shop_app_mobile/features/category/domain/repository/category_repository.dart';
import 'package:injectable/injectable.dart';

@Injectable(as: CategoryRepository)
class CategoryRepositoryImpl implements CategoryRepository {
  final CategoryRemoteDataSource _categoryRemoteDataSource;

  CategoryRepositoryImpl(this._categoryRemoteDataSource);

  @override
  Future<List<Category>> getCategories({int? categoryId}) async {
    final response =
        await _categoryRemoteDataSource.getCategories(categoryId: categoryId);

    return (response.data['data'] as List)
        .map((e) => Category.fromJson(e))
        .toList();
  }

  @override
  Future<List<Category>> getAdmin({required String search}) async {
    final response = await _categoryRemoteDataSource.getAdmin(search: search);

    return (response.data['data'] as List)
        .map((e) => Category.fromJson(e))
        .toList();
  }

  @override
  Future<void> createCategory({
    required CategoryCreateUpdateDto dto,
    required XFile image,
  }) =>
      _categoryRemoteDataSource.createCategory(
        dto: dto,
        image: image,
      );

  @override
  Future<void> deleteCategory({required int id}) =>
      _categoryRemoteDataSource.deleteCategory(
        id: id,
      );

  @override
  Future<void> updateCategory({
    required CategoryCreateUpdateDto dto,
    XFile? image,
    required int id,
  }) =>
      _categoryRemoteDataSource.updateCategory(
        dto: dto,
        id: id,
        image: image,
      );
}
