import 'package:image_picker/image_picker.dart';
import 'package:shop_app_mobile/features/category/domain/dto/category_create_update_dto.dart';
import 'package:shop_app_mobile/features/category/domain/models/category.dart';
import 'package:shop_app_mobile/features/category/domain/repository/category_repository.dart';
import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';

part 'category_state.dart';
part 'category_cubit.freezed.dart';

@injectable
class CategoryCubit extends Cubit<CategoryState> {
  final CategoryRepository _categoryRepository;

  CategoryCubit(this._categoryRepository)
      : super(const CategoryState.initial());

  Future<void> getCategories({int? categoryId}) async {
    try {
      final categories =
          await _categoryRepository.getCategories(categoryId: categoryId);

      emit(CategoryState.loaded(categories));
    } on Exception {}
  }

  Future<void> getAdmin({String? search}) async {
    try {
      final categories =
          await _categoryRepository.getAdmin(search: search ?? '');

      emit(CategoryState.loaded(categories));
    } on Exception {}
  }

  Future<void> createCategory({
    required CategoryCreateUpdateDto dto,
    required XFile image,
  }) =>
      _categoryRepository.createCategory(
        dto: dto,
        image: image,
      );

  Future<void> updateCategory({
    required CategoryCreateUpdateDto dto,
    XFile? image,
    required int id,
  }) =>
      _categoryRepository.updateCategory(
        dto: dto,
        id: id,
      );

  Future<void> deleteCategory({
    required int id,
  }) =>
      _categoryRepository.deleteCategory(
        id: id,
      );
}
