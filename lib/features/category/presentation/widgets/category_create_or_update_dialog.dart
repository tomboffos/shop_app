import 'package:auto_route/auto_route.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:field_suggestion/field_suggestion.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:shop_app_mobile/core/injection/injection.dart';
import 'package:shop_app_mobile/core/utils/constants/gaps.dart';
import 'package:shop_app_mobile/core/utils/constants/insets.dart';
import 'package:shop_app_mobile/features/app/widgets/confirmation_dialog.dart';
import 'package:shop_app_mobile/features/app/widgets/image_picker_dialog.dart';

import 'package:shop_app_mobile/features/category/domain/dto/category_create_update_dto.dart';
import 'package:shop_app_mobile/features/category/domain/models/category.dart';
import 'package:shop_app_mobile/features/category/domain/repository/category_repository.dart';
import 'package:shop_app_mobile/features/category/presentation/cubit/category_cubit.dart';
import 'package:shop_app_mobile/features/filters/domain/models/custom_field.dart';
import 'package:shop_app_mobile/features/filters/domain/repository/custom_field_repository.dart';
import 'package:shop_app_mobile/features/product/domain/models/product.dart';

class CategoryCreateOrUpdateDialog extends StatefulWidget {
  final Category? category;

  static Future<void> show(BuildContext context, {Category? category}) =>
      showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        builder: (context) => CategoryCreateOrUpdateDialog(
          category: category,
        ),
      );

  const CategoryCreateOrUpdateDialog({super.key, this.category});

  @override
  State<CategoryCreateOrUpdateDialog> createState() =>
      _CategoryCreateOrUpdateDialogState();
}

class _CategoryCreateOrUpdateDialogState
    extends State<CategoryCreateOrUpdateDialog> {
  XFile? image;

  final _nameController = TextEditingController();

  List<Product> services = [];
  List<CustomField> fields = [];
  Category? category;
  final TextEditingController _serviceController = TextEditingController();
  final TextEditingController _filtersController = TextEditingController();
  final TextEditingController _parentController = TextEditingController();

  @override
  void initState() {
    if (widget.category != null) {
      _nameController.text = widget.category!.name ?? '';
      fields = [...(widget.category!.fields ?? [])];
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: MediaQuery.of(context).size.height * 0.7,
      child: SingleChildScrollView(
        child: Padding(
          padding: Insets.dimens2x,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Align(
                alignment: Alignment.topRight,
                child: InkWell(
                  onTap: () => context.router.pop(),
                  child: const Icon(
                    Icons.close,
                    color: Colors.black,
                    size: 30,
                  ),
                ),
              ),
              Text(
                'Фото',
                style: Theme.of(context).textTheme.headlineSmall,
              ),
              Gaps.gap,
              if (image != null)
                InkWell(
                  onTap: () async {
                    await _pickImage(context);
                  },
                  child: Image.asset(
                    image!.path,
                    width: 150,
                  ),
                )
              else if (widget.category != null &&
                  widget.category!.image != null)
                InkWell(
                  onTap: () async {
                    await _pickImage(context);
                  },
                  child: CachedNetworkImage(
                    imageUrl: widget.category!.image!,
                    width: 150,
                  ),
                )
              else
                OutlinedButton(
                  onPressed: () async {
                    await _pickImage(context);
                  },
                  child: const Icon(Icons.add),
                ),
              Gaps.gap,
              Text(
                'Название',
                style: Theme.of(context).textTheme.headlineSmall,
              ),
              Gaps.gap,
              TextField(
                controller: _nameController,
                decoration: const InputDecoration(
                  hintText: 'Название',
                ),
                maxLines: 1,
              ),
              Gaps.gap,
              Text(
                'Родительская категория',
                style: Theme.of(context).textTheme.headlineSmall,
              ),
              Gaps.gap,
              FieldSuggestion.network(
                textController: _parentController,
                builder: (context, value) => ListView.builder(
                  padding: EdgeInsets.zero,
                  itemBuilder: (context, index) => InkWell(
                    onTap: () {
                      setState(() {
                        category = value.data?[index];
                      });
                      _parentController.clear();
                    },
                    child: ListTile(
                      contentPadding: EdgeInsets.zero,
                      title: Text(
                        value.data![index].name ?? '',
                        style: Theme.of(context).textTheme.bodyLarge,
                      ),
                    ),
                  ),
                  itemCount: value.data?.length ?? 0,
                ),
                future: (input) =>
                    getIt.get<CategoryRepository>().getAdmin(search: input),
              ),
              Gaps.gap,
              if (category != null)
                ListTile(
                  title: Text(
                    category!.name ?? '',
                  ),
                  trailing: IconButton(
                    onPressed: () {
                      setState(() {
                        category = null;
                      });
                    },
                    icon: const Icon(Icons.close),
                  ),
                ),
              Gaps.gap,
              Text(
                'Фильтры',
                style: Theme.of(context).textTheme.headlineSmall,
              ),
              Gaps.gap,
              FieldSuggestion.network(
                textController: _filtersController,
                builder: (context, value) => ListView.builder(
                  padding: EdgeInsets.zero,
                  itemBuilder: (context, index) => InkWell(
                    onTap: () {
                      setState(() {
                        if (!fields.contains(value.data![index])) {
                          fields.add(value.data![index]);
                        }
                      });
                      _filtersController.clear();
                    },
                    child: ListTile(
                      contentPadding: EdgeInsets.zero,
                      title: Text(
                        value.data![index].name,
                        style: Theme.of(context).textTheme.bodyLarge,
                      ),
                    ),
                  ),
                  itemCount: value.data?.length ?? 0,
                ),
                future: (input) => getIt.get<CustomFieldRepository>().getAdmin(
                      search: input,
                    ),
              ),
              Gaps.gap,
              ListView.builder(
                itemBuilder: (context, index) => ListTile(
                  title: Text(
                    fields[index].name,
                  ),
                  trailing: IconButton(
                    onPressed: () {
                      setState(() {
                        fields.removeAt(index);
                      });
                    },
                    icon: const Icon(Icons.close),
                  ),
                ),
                itemCount: fields.length,
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
              ),
              Gaps.gap,
              OutlinedButton(
                style: const ButtonStyle(
                  minimumSize: MaterialStatePropertyAll(
                    Size(
                      double.infinity,
                      double.minPositive,
                    ),
                  ),
                ),
                onPressed: () async {
                  if (widget.category != null) {
                    await getIt.get<CategoryCubit>().updateCategory(
                          dto: CategoryCreateUpdateDto(
                            name: _nameController.text,
                            categoryId: category?.id,
                            image: widget.category!.image,
                            fields: fields.map((e) => e.id).toList(),
                          ),
                          image: image,
                          id: widget.category!.id,
                        );
                  } else {
                    if (image != null) {
                      await getIt.get<CategoryCubit>().createCategory(
                            dto: CategoryCreateUpdateDto(
                              name: _nameController.text,
                              categoryId: category?.id,
                              fields: fields.map((e) => e.id).toList(),
                            ),
                            image: image!,
                          );
                    }
                  }

                  context.router.pop();
                },
                child: const Padding(
                  padding: EdgeInsets.symmetric(vertical: 10),
                  child: Text('Сохранить'),
                ),
              ),
              Gaps.gap,
              if (widget.category != null)
                FilledButton(
                  style: const ButtonStyle(
                    minimumSize: MaterialStatePropertyAll(
                      Size(
                        double.infinity,
                        double.minPositive,
                      ),
                    ),
                    backgroundColor: MaterialStatePropertyAll(
                      Colors.red,
                    ),
                  ),
                  onPressed: () async {
                    await showDialog(
                      context: context,
                      builder: (context) => ConfirmationDialog(
                        onConfirm: () async {
                          await getIt
                              .get<CategoryCubit>()
                              .deleteCategory(id: widget.category!.id);
                        },
                      ),
                    );

                    context.router.pop();
                  },
                  child: const Padding(
                    padding: EdgeInsets.symmetric(vertical: 10),
                    child: Text('Удалить'),
                  ),
                ),
              const SizedBox(
                height: 200,
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future<void> _pickImage(BuildContext context) async {
    final file = await ImagePickerDialog.showPickerDialog(context);

    setState(() {
      image = file;
    });
  }
}
