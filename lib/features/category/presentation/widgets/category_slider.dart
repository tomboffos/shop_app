import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shop_app_mobile/core/injection/injection.dart';
import 'package:shop_app_mobile/core/utils/constants/gaps.dart';
import 'package:shop_app_mobile/core/utils/constants/insets.dart';
import 'package:shop_app_mobile/core/utils/extensions/theme_extension.dart';
import 'package:shop_app_mobile/features/category/presentation/cubit/category_cubit.dart';
import 'package:shop_app_mobile/features/category/presentation/widgets/category_item.dart';

class CategorySlider extends StatefulWidget {
  const CategorySlider({super.key});

  @override
  State<CategorySlider> createState() => _CategorySliderState();
}

class _CategorySliderState extends State<CategorySlider> {
  final _categoryCubit = getIt.get<CategoryCubit>();

  @override
  void initState() {
    _categoryCubit.getCategories();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CategoryCubit, CategoryState>(
      bloc: _categoryCubit,
      builder: (context, state) {
        return state.maybeWhen(
          orElse: () => const SizedBox.shrink(),
          loaded: (categories) => categories.isNotEmpty
              ? Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding:
                          Insets.dimens2x.copyWith(bottom: 0, top: 0, right: 0),
                      child: Text(
                        'Категории',
                        style: context.text.headlineLarge,
                      ),
                    ),
                    Gaps.gap2,
                    SizedBox(
                      height: MediaQuery.of(context).size.height / 5,
                      child: ListView.builder(
                        shrinkWrap: true,
                        scrollDirection: Axis.horizontal,
                        itemCount: categories.length,
                        itemBuilder: (BuildContext context, int index) =>
                            Padding(
                          padding: Insets.dimens2x
                              .copyWith(bottom: 0, top: 0, right: 0),
                          child: CategoryItem(
                            category: categories[index],
                          ),
                        ),
                      ),
                    ),
                  ],
                )
              : const SizedBox.shrink(),
        );
      },
    );
  }

  @override
  void dispose() {
    _categoryCubit.close();
    super.dispose();
  }
}
