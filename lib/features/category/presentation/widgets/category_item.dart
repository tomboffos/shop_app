import 'package:auto_route/auto_route.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:shop_app_mobile/core/routing/app_router.dart';
import 'package:shop_app_mobile/core/utils/constants/gaps.dart';
import 'package:shop_app_mobile/core/utils/extensions/theme_extension.dart';
import 'package:shop_app_mobile/features/category/domain/models/category.dart';

class CategoryItem extends StatelessWidget {
  final Category category;
  final Function()? onPressed;

  const CategoryItem({
    super.key,
    required this.category,
    this.onPressed,
  });

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPressed ??
          () => category.children > 0
              ? context.router.push(CategoriesDetailRoute(category: category))
              : context.router.push(
                  ProductsRoute(category: category),
                ),
      child: SizedBox(
        width: MediaQuery.of(context).size.width / 2 - 32,
        height: MediaQuery.of(context).size.height / 4.5,
        child: Card(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                CachedNetworkImage(
                  width: 100,
                  height: 100,
                  fit: BoxFit.cover,
                  imageUrl: (category.image?.contains('https') ?? false) ||
                          (category.image?.contains('http') ?? false)
                      ? category.image!
                      : '',
                  errorWidget: (context, string, object) => const Center(
                    child: Icon(
                      Icons.grid_3x3_sharp,
                      size: 40,
                    ),
                  ),
                ),
                Gaps.gap3,
                if (category.name != null)
                  Text(
                    category.name!,
                    style: context.text.titleLarge,
                    textAlign: TextAlign.center,
                  ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
