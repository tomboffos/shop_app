import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shop_app_mobile/core/injection/injection.dart';
import 'package:shop_app_mobile/core/utils/constants/insets.dart';
import 'package:shop_app_mobile/features/category/presentation/cubit/category_cubit.dart';
import 'package:shop_app_mobile/features/category/presentation/widgets/category_create_or_update_dialog.dart';
import 'package:shop_app_mobile/features/category/presentation/widgets/category_item.dart';

@RoutePage()
class CategoriesPage extends StatefulWidget {
  final bool admin;
  const CategoriesPage({
    super.key,
    this.admin = false,
  });

  @override
  State<CategoriesPage> createState() => _CategoriesPageState();
}

class _CategoriesPageState extends State<CategoriesPage> {
  final _categoryCubit = getIt.get<CategoryCubit>();

  @override
  void initState() {
    if (widget.admin) {
      _categoryCubit.getAdmin();
    } else {
      _categoryCubit.getCategories();
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
      onRefresh: () async {},
      child: Padding(
        padding:
            widget.admin ? EdgeInsets.zero : Insets.dimens3x.copyWith(right: 0),
        child: BlocBuilder<CategoryCubit, CategoryState>(
          bloc: _categoryCubit,
          builder: (context, state) {
            return state.maybeWhen(
              orElse: () => const Center(
                child: CircularProgressIndicator(),
              ),
              loaded: (categories) => SingleChildScrollView(
                physics: const AlwaysScrollableScrollPhysics(),
                child: Padding(
                  padding: const EdgeInsets.only(
                    top: 50,
                  ),
                  child: Column(
                    children: [
                      if (widget.admin)
                        Padding(
                          padding: Insets.dimens4x.copyWith(top: 0, bottom: 10),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                'Категории',
                                style:
                                    Theme.of(context).textTheme.headlineMedium,
                              ),
                              FilledButton(
                                onPressed: () async {
                                  await CategoryCreateOrUpdateDialog.show(
                                    context,
                                  );

                                  _categoryCubit.getAdmin();
                                },
                                child: const Icon(
                                  Icons.add,
                                ),
                              )
                            ],
                          ),
                        ),
                      Wrap(
                        alignment: WrapAlignment.spaceEvenly,
                        children: categories
                            .map(
                              (e) => CategoryItem(
                                category: e,
                                onPressed: widget.admin
                                    ? () async {
                                        await CategoryCreateOrUpdateDialog.show(
                                          context,
                                          category: e,
                                        );

                                        _categoryCubit.getAdmin();
                                      }
                                    : null,
                              ),
                            )
                            .toList(),
                      ),
                    ],
                  ),
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
