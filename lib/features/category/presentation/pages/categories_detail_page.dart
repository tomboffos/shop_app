import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shop_app_mobile/core/injection/injection.dart';
import 'package:shop_app_mobile/features/category/domain/models/category.dart';
import 'package:shop_app_mobile/features/category/presentation/cubit/category_cubit.dart';
import 'package:shop_app_mobile/features/category/presentation/widgets/category_item.dart';

@RoutePage()
class CategoriesDetailPage extends StatefulWidget {
  final Category category;

  const CategoriesDetailPage({super.key, required this.category});

  @override
  State<CategoriesDetailPage> createState() => _CategoriesDetailPageState();
}

class _CategoriesDetailPageState extends State<CategoriesDetailPage> {
  final _categoryCubit = getIt.get<CategoryCubit>();

  @override
  void initState() {
    _categoryCubit.getCategories(
      categoryId: widget.category.id,
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(
        horizontal: 20,
      ),
      child: BlocBuilder<CategoryCubit, CategoryState>(
        bloc: _categoryCubit,
        builder: (context, state) {
          return state.maybeWhen(
            orElse: () => const Center(
              child: CircularProgressIndicator(),
            ),
            loaded: (categories) => GridView.builder(
              padding: const EdgeInsets.all(10),
              itemCount: categories.length,
              gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2,
                crossAxisSpacing: 10,
                mainAxisSpacing: 10,
                childAspectRatio: 6 / 7,
              ),
              itemBuilder: (context, index) => CategoryItem(
                category: categories[index],
              ),
            ),
          );
        },
      ),
    );
  }
}
