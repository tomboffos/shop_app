import 'package:injectable/injectable.dart';
import 'package:shop_app_mobile/features/per_price_option/data/data_source/per_price_option_data_source.dart';
import 'package:shop_app_mobile/features/per_price_option/domain/repository/per_price_option_repository.dart';

@Injectable(as: PerPriceOptionRepository)
class PerPriceOptionsRepositoryImpl implements PerPriceOptionRepository {
  final PerPriceOptionDataSource _dataSource;

  PerPriceOptionsRepositoryImpl(this._dataSource);

  @override
  Future<void> createOption({required String name}) =>
      _dataSource.createOption(name: name);

  @override
  Future<void> delete({required int id}) => _dataSource.delete(id: id);

  @override
  Future<void> update({required String name, required int id}) =>
      _dataSource.update(
        name: name,
        id: id,
      );
}
