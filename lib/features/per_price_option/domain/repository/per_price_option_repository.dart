abstract class PerPriceOptionRepository {
  Future<void> createOption({required String name});

  Future<void> update({required String name, required int id});

  Future<void> delete({required int id});
}
