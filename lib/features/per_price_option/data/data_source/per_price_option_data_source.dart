abstract class PerPriceOptionDataSource {
  static String perPriceOptions = '/per-price-options';
  static String perPriceOptionsAdmin = '/per-price-options/admin';

  Future<void> createOption({required String name});

  Future<void> update({required String name, required int id});

  Future<void> delete({required int id});
}
