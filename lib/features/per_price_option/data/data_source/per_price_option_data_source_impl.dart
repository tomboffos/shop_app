import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:shop_app_mobile/features/category/data/data_source/category_remote_data_source.dart';
import 'package:shop_app_mobile/features/per_price_option/data/data_source/per_price_option_data_source.dart';

@Injectable(as: PerPriceOptionDataSource)
class PerPriceOptiondataSourceImpl implements PerPriceOptionDataSource {
  final Dio _dio;

  PerPriceOptiondataSourceImpl(this._dio) {
    _dio.options.baseUrl += CategoryRemoteDataSource.shop;
  }

  @override
  Future<void> createOption({required String name}) =>
      _dio.post(PerPriceOptionDataSource.perPriceOptionsAdmin,
          data: {'name': name});

  @override
  Future<void> delete({required int id}) => _dio.delete(
        '${PerPriceOptionDataSource.perPriceOptionsAdmin}/$id',
      );

  @override
  Future<void> update({required String name, required int id}) => _dio.post(
        '${PerPriceOptionDataSource.perPriceOptionsAdmin}/$id',
        data: {'name': name},
      );
}
