import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:shop_app_mobile/features/per_price_option/domain/repository/per_price_option_repository.dart';
import 'package:shop_app_mobile/features/product/domain/models/per_price/per_price.dart';
import 'package:shop_app_mobile/features/product/domain/repository/product_repository.dart';

part 'per_price_option_state.dart';
part 'per_price_option_cubit.freezed.dart';

@injectable
class PerPriceOptionCubit extends Cubit<PerPriceOptionState> {
  final ProductRepository _productRepository;
  final PerPriceOptionRepository _repository;

  PerPriceOptionCubit(this._productRepository, this._repository)
      : super(const PerPriceOptionState.initial());

  void getOptions() async {
    final options = await _productRepository.getOptions();

    emit(PerPriceOptionState.loaded(options));
  }

  Future<void> create({required String name}) =>
      _repository.createOption(name: name);

  Future<void> update({required String name, required int id}) =>
      _repository.update(name: name, id: id);

  Future<void> delete({required int id}) => _repository.delete(id: id);
}
