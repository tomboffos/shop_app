part of 'per_price_option_cubit.dart';

@freezed
class PerPriceOptionState with _$PerPriceOptionState {
  const factory PerPriceOptionState.initial() = _Initial;

  const factory PerPriceOptionState.loaded(List<PerPrice> options) = _Loaded;
}
