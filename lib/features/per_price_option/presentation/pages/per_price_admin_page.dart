import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shop_app_mobile/core/injection/injection.dart';
import 'package:shop_app_mobile/core/utils/constants/gaps.dart';
import 'package:shop_app_mobile/core/utils/constants/insets.dart';
import 'package:shop_app_mobile/features/per_price_option/presentation/cubit/per_price_option_cubit.dart';
import 'package:shop_app_mobile/features/per_price_option/presentation/widgets/option_create_update_dialog.dart';

@RoutePage()
class PerPriceAdminPage extends StatefulWidget {
  const PerPriceAdminPage({super.key});

  @override
  State<PerPriceAdminPage> createState() => _PerPriceAdminPageState();
}

class _PerPriceAdminPageState extends State<PerPriceAdminPage> {
  final _perPriceCubit = getIt.get<PerPriceOptionCubit>();

  @override
  void initState() {
    _perPriceCubit.getOptions();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: BlocBuilder<PerPriceOptionCubit, PerPriceOptionState>(
          bloc: _perPriceCubit,
          builder: (context, state) {
            return Padding(
              padding: Insets.dimens2x,
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        'Единицы измерения',
                        style: Theme.of(context).textTheme.headlineMedium,
                      ),
                      FilledButton(
                        onPressed: () async {
                          await OptionCreateUpdateDialog.show(context);

                          _perPriceCubit.getOptions();
                        },
                        child: const Icon(
                          Icons.add,
                        ),
                      )
                    ],
                  ),
                  Gaps.gap2,
                  state.maybeWhen(
                    orElse: () => const Center(
                      child: CircularProgressIndicator(),
                    ),
                    loaded: (data) => ListView.builder(
                      itemBuilder: (context, index) => ListTile(
                        title: Text(data[index].name),
                        onTap: () async {
                          await OptionCreateUpdateDialog.show(
                            context,
                            option: data[index],
                          );

                          _perPriceCubit.getOptions();
                        },
                      ),
                      itemCount: data.length,
                      shrinkWrap: true,
                      physics: const NeverScrollableScrollPhysics(),
                    ),
                  )
                ],
              ),
            );
          },
        ),
      ),
    );
  }
}
