import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:shop_app_mobile/core/injection/injection.dart';
import 'package:shop_app_mobile/core/utils/constants/gaps.dart';
import 'package:shop_app_mobile/core/utils/constants/insets.dart';
import 'package:shop_app_mobile/features/app/widgets/confirmation_dialog.dart';
import 'package:shop_app_mobile/features/per_price_option/presentation/cubit/per_price_option_cubit.dart';
import 'package:shop_app_mobile/features/product/domain/models/per_price/per_price.dart';

class OptionCreateUpdateDialog extends StatefulWidget {
  final PerPrice? option;
  const OptionCreateUpdateDialog({super.key, this.option});

  static show(BuildContext context, {PerPrice? option}) => showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        builder: (context) => OptionCreateUpdateDialog(
          option: option,
        ),
      );

  @override
  State<OptionCreateUpdateDialog> createState() =>
      _OptionCreateUpdateDialogState();
}

class _OptionCreateUpdateDialogState extends State<OptionCreateUpdateDialog> {
  final _nameController = TextEditingController();

  @override
  void initState() {
    if (widget.option != null) {
      _nameController.text = widget.option!.name;
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Padding(
        padding: Insets.dimens2x,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Gaps.gap,
            Text(
              'Имя',
              style: Theme.of(context).textTheme.headlineSmall,
            ),
            Gaps.gap,
            TextField(
              controller: _nameController,
            ),
            Gaps.gap,
            OutlinedButton(
              style: const ButtonStyle(
                minimumSize: MaterialStatePropertyAll(
                  Size(
                    double.infinity,
                    double.minPositive,
                  ),
                ),
              ),
              onPressed: () async {
                if (widget.option != null) {
                  await getIt.get<PerPriceOptionCubit>().update(
                        id: widget.option!.id,
                        name: _nameController.text,
                      );
                } else {
                  await getIt.get<PerPriceOptionCubit>().create(
                        name: _nameController.text,
                      );
                }

                context.router.pop();
              },
              child: const Padding(
                padding: EdgeInsets.symmetric(vertical: 10),
                child: Text('Сохранить'),
              ),
            ),
            Gaps.gap,
            if (widget.option != null)
              FilledButton(
                style: const ButtonStyle(
                  minimumSize: MaterialStatePropertyAll(
                    Size(
                      double.infinity,
                      double.minPositive,
                    ),
                  ),
                  backgroundColor: MaterialStatePropertyAll(
                    Colors.red,
                  ),
                ),
                onPressed: () async {
                  await showDialog(
                    context: context,
                    builder: (context) => ConfirmationDialog(
                      onConfirm: () => getIt
                          .get<PerPriceOptionCubit>()
                          .delete(id: widget.option!.id),
                    ),
                  );

                  context.router.pop();
                },
                child: const Padding(
                  padding: EdgeInsets.symmetric(vertical: 10),
                  child: Text('Удалить'),
                ),
              ),
            SizedBox(
              height: 200,
            )
          ],
        ),
      ),
    );
  }
}
