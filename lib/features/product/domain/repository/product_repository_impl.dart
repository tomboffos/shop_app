import 'package:image_picker/image_picker.dart';
import 'package:shop_app_mobile/features/product/data/data_source/product_remote_data_source.dart';
import 'package:shop_app_mobile/features/product/domain/create_update_dto/product_create_update_dto.dart';
import 'package:shop_app_mobile/features/product/domain/dto/product_get_dto.dart';
import 'package:shop_app_mobile/features/product/domain/models/per_price/per_price.dart';
import 'package:shop_app_mobile/features/product/domain/models/product.dart';
import 'package:shop_app_mobile/features/product/domain/repository/product_repository.dart';
import 'package:injectable/injectable.dart';

@Injectable(as: ProductRepository)
class ProductRepositoryImpl implements ProductRepository {
  final ProductRemoteDataSource _productRemoteDataSource;

  ProductRepositoryImpl(this._productRemoteDataSource);

  @override
  Future<List<Product>> getProducts(ProductGetDto dto) async {
    final response = await _productRemoteDataSource.getProducts(dto);

    return (response.data['data'] as List)
        .map((e) => Product.fromJson(e))
        .toList();
  }

  @override
  Future<void> createProduct(
          {required ProductCreateUpdateDto dto, List<XFile>? medias}) =>
      _productRemoteDataSource.createProduct(dto, medias);

  @override
  Future<void> deleteProduct({required int id}) =>
      _productRemoteDataSource.deleteProduct(id);

  @override
  Future<void> updateProduct(ProductCreateUpdateDto dto, int id,
          {List<XFile>? medias, List<String>? originImages}) =>
      _productRemoteDataSource.updateProduct(dto, id);

  @override
  Future<List<PerPrice>> getOptions() async {
    final response = await _productRemoteDataSource.getOptions();

    return (response.data['data'] as List)
        .map((e) => PerPrice.fromJson(e))
        .toList();
  }
}
