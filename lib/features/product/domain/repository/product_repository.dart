import 'package:image_picker/image_picker.dart';
import 'package:shop_app_mobile/features/product/domain/create_update_dto/product_create_update_dto.dart';
import 'package:shop_app_mobile/features/product/domain/dto/product_get_dto.dart';
import 'package:shop_app_mobile/features/product/domain/models/per_price/per_price.dart';
import 'package:shop_app_mobile/features/product/domain/models/product.dart';

abstract class ProductRepository {
  Future<List<Product>> getProducts(ProductGetDto dto);

  Future<void> createProduct({
    required ProductCreateUpdateDto dto,
    List<XFile>? medias,
  });

  Future<void> updateProduct(
    ProductCreateUpdateDto dto,
    int id, {
    List<XFile>? medias,
    List<String>? originImages,
  });

  Future<void> deleteProduct({required int id});

  Future<List<PerPrice>> getOptions();
}
