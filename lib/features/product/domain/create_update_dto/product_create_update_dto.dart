import 'package:freezed_annotation/freezed_annotation.dart';

part 'product_create_update_dto.g.dart';
part 'product_create_update_dto.freezed.dart';

@freezed
class ProductCreateUpdateDto with _$ProductCreateUpdateDto {
  factory ProductCreateUpdateDto({
    required String name,
    required String description,
    required String price,
    @JsonKey(name: 'per_price_option_id') required int perPriceOptionId,
    @Default(true) bool active,
    @Default({})
    @JsonKey(name: 'custom_fields')
    Map<String, dynamic> customFields,
    @Default([]) List<int> categories,
  }) = _ProductCreateUpdateDto;

  factory ProductCreateUpdateDto.fromJson(Map<String, dynamic> json) =>
      _$ProductCreateUpdateDtoFromJson(json);
}
