// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'product_create_update_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

ProductCreateUpdateDto _$ProductCreateUpdateDtoFromJson(
    Map<String, dynamic> json) {
  return _ProductCreateUpdateDto.fromJson(json);
}

/// @nodoc
mixin _$ProductCreateUpdateDto {
  String get name => throw _privateConstructorUsedError;
  String get description => throw _privateConstructorUsedError;
  String get price => throw _privateConstructorUsedError;
  @JsonKey(name: 'per_price_option_id')
  int get perPriceOptionId => throw _privateConstructorUsedError;
  bool get active => throw _privateConstructorUsedError;
  @JsonKey(name: 'custom_fields')
  Map<String, dynamic> get customFields => throw _privateConstructorUsedError;
  List<int> get categories => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ProductCreateUpdateDtoCopyWith<ProductCreateUpdateDto> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ProductCreateUpdateDtoCopyWith<$Res> {
  factory $ProductCreateUpdateDtoCopyWith(ProductCreateUpdateDto value,
          $Res Function(ProductCreateUpdateDto) then) =
      _$ProductCreateUpdateDtoCopyWithImpl<$Res, ProductCreateUpdateDto>;
  @useResult
  $Res call(
      {String name,
      String description,
      String price,
      @JsonKey(name: 'per_price_option_id') int perPriceOptionId,
      bool active,
      @JsonKey(name: 'custom_fields') Map<String, dynamic> customFields,
      List<int> categories});
}

/// @nodoc
class _$ProductCreateUpdateDtoCopyWithImpl<$Res,
        $Val extends ProductCreateUpdateDto>
    implements $ProductCreateUpdateDtoCopyWith<$Res> {
  _$ProductCreateUpdateDtoCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? name = null,
    Object? description = null,
    Object? price = null,
    Object? perPriceOptionId = null,
    Object? active = null,
    Object? customFields = null,
    Object? categories = null,
  }) {
    return _then(_value.copyWith(
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      description: null == description
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String,
      price: null == price
          ? _value.price
          : price // ignore: cast_nullable_to_non_nullable
              as String,
      perPriceOptionId: null == perPriceOptionId
          ? _value.perPriceOptionId
          : perPriceOptionId // ignore: cast_nullable_to_non_nullable
              as int,
      active: null == active
          ? _value.active
          : active // ignore: cast_nullable_to_non_nullable
              as bool,
      customFields: null == customFields
          ? _value.customFields
          : customFields // ignore: cast_nullable_to_non_nullable
              as Map<String, dynamic>,
      categories: null == categories
          ? _value.categories
          : categories // ignore: cast_nullable_to_non_nullable
              as List<int>,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$ProductCreateUpdateDtoImplCopyWith<$Res>
    implements $ProductCreateUpdateDtoCopyWith<$Res> {
  factory _$$ProductCreateUpdateDtoImplCopyWith(
          _$ProductCreateUpdateDtoImpl value,
          $Res Function(_$ProductCreateUpdateDtoImpl) then) =
      __$$ProductCreateUpdateDtoImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {String name,
      String description,
      String price,
      @JsonKey(name: 'per_price_option_id') int perPriceOptionId,
      bool active,
      @JsonKey(name: 'custom_fields') Map<String, dynamic> customFields,
      List<int> categories});
}

/// @nodoc
class __$$ProductCreateUpdateDtoImplCopyWithImpl<$Res>
    extends _$ProductCreateUpdateDtoCopyWithImpl<$Res,
        _$ProductCreateUpdateDtoImpl>
    implements _$$ProductCreateUpdateDtoImplCopyWith<$Res> {
  __$$ProductCreateUpdateDtoImplCopyWithImpl(
      _$ProductCreateUpdateDtoImpl _value,
      $Res Function(_$ProductCreateUpdateDtoImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? name = null,
    Object? description = null,
    Object? price = null,
    Object? perPriceOptionId = null,
    Object? active = null,
    Object? customFields = null,
    Object? categories = null,
  }) {
    return _then(_$ProductCreateUpdateDtoImpl(
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      description: null == description
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String,
      price: null == price
          ? _value.price
          : price // ignore: cast_nullable_to_non_nullable
              as String,
      perPriceOptionId: null == perPriceOptionId
          ? _value.perPriceOptionId
          : perPriceOptionId // ignore: cast_nullable_to_non_nullable
              as int,
      active: null == active
          ? _value.active
          : active // ignore: cast_nullable_to_non_nullable
              as bool,
      customFields: null == customFields
          ? _value._customFields
          : customFields // ignore: cast_nullable_to_non_nullable
              as Map<String, dynamic>,
      categories: null == categories
          ? _value._categories
          : categories // ignore: cast_nullable_to_non_nullable
              as List<int>,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$ProductCreateUpdateDtoImpl implements _ProductCreateUpdateDto {
  _$ProductCreateUpdateDtoImpl(
      {required this.name,
      required this.description,
      required this.price,
      @JsonKey(name: 'per_price_option_id') required this.perPriceOptionId,
      this.active = true,
      @JsonKey(name: 'custom_fields')
      final Map<String, dynamic> customFields = const {},
      final List<int> categories = const []})
      : _customFields = customFields,
        _categories = categories;

  factory _$ProductCreateUpdateDtoImpl.fromJson(Map<String, dynamic> json) =>
      _$$ProductCreateUpdateDtoImplFromJson(json);

  @override
  final String name;
  @override
  final String description;
  @override
  final String price;
  @override
  @JsonKey(name: 'per_price_option_id')
  final int perPriceOptionId;
  @override
  @JsonKey()
  final bool active;
  final Map<String, dynamic> _customFields;
  @override
  @JsonKey(name: 'custom_fields')
  Map<String, dynamic> get customFields {
    if (_customFields is EqualUnmodifiableMapView) return _customFields;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableMapView(_customFields);
  }

  final List<int> _categories;
  @override
  @JsonKey()
  List<int> get categories {
    if (_categories is EqualUnmodifiableListView) return _categories;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_categories);
  }

  @override
  String toString() {
    return 'ProductCreateUpdateDto(name: $name, description: $description, price: $price, perPriceOptionId: $perPriceOptionId, active: $active, customFields: $customFields, categories: $categories)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ProductCreateUpdateDtoImpl &&
            (identical(other.name, name) || other.name == name) &&
            (identical(other.description, description) ||
                other.description == description) &&
            (identical(other.price, price) || other.price == price) &&
            (identical(other.perPriceOptionId, perPriceOptionId) ||
                other.perPriceOptionId == perPriceOptionId) &&
            (identical(other.active, active) || other.active == active) &&
            const DeepCollectionEquality()
                .equals(other._customFields, _customFields) &&
            const DeepCollectionEquality()
                .equals(other._categories, _categories));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      name,
      description,
      price,
      perPriceOptionId,
      active,
      const DeepCollectionEquality().hash(_customFields),
      const DeepCollectionEquality().hash(_categories));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$ProductCreateUpdateDtoImplCopyWith<_$ProductCreateUpdateDtoImpl>
      get copyWith => __$$ProductCreateUpdateDtoImplCopyWithImpl<
          _$ProductCreateUpdateDtoImpl>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$ProductCreateUpdateDtoImplToJson(
      this,
    );
  }
}

abstract class _ProductCreateUpdateDto implements ProductCreateUpdateDto {
  factory _ProductCreateUpdateDto(
      {required final String name,
      required final String description,
      required final String price,
      @JsonKey(name: 'per_price_option_id') required final int perPriceOptionId,
      final bool active,
      @JsonKey(name: 'custom_fields') final Map<String, dynamic> customFields,
      final List<int> categories}) = _$ProductCreateUpdateDtoImpl;

  factory _ProductCreateUpdateDto.fromJson(Map<String, dynamic> json) =
      _$ProductCreateUpdateDtoImpl.fromJson;

  @override
  String get name;
  @override
  String get description;
  @override
  String get price;
  @override
  @JsonKey(name: 'per_price_option_id')
  int get perPriceOptionId;
  @override
  bool get active;
  @override
  @JsonKey(name: 'custom_fields')
  Map<String, dynamic> get customFields;
  @override
  List<int> get categories;
  @override
  @JsonKey(ignore: true)
  _$$ProductCreateUpdateDtoImplCopyWith<_$ProductCreateUpdateDtoImpl>
      get copyWith => throw _privateConstructorUsedError;
}
