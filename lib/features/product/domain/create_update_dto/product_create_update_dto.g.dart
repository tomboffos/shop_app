// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'product_create_update_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$ProductCreateUpdateDtoImpl _$$ProductCreateUpdateDtoImplFromJson(
        Map<String, dynamic> json) =>
    _$ProductCreateUpdateDtoImpl(
      name: json['name'] as String,
      description: json['description'] as String,
      price: json['price'] as String,
      perPriceOptionId: json['per_price_option_id'] as int,
      active: json['active'] as bool? ?? true,
      customFields: json['custom_fields'] as Map<String, dynamic>? ?? const {},
      categories: (json['categories'] as List<dynamic>?)
              ?.map((e) => e as int)
              .toList() ??
          const [],
    );

Map<String, dynamic> _$$ProductCreateUpdateDtoImplToJson(
        _$ProductCreateUpdateDtoImpl instance) =>
    <String, dynamic>{
      'name': instance.name,
      'description': instance.description,
      'price': instance.price,
      'per_price_option_id': instance.perPriceOptionId,
      'active': instance.active,
      'custom_fields': instance.customFields,
      'categories': instance.categories,
    };
