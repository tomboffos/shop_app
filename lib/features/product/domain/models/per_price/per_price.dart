import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:hive/hive.dart';

part 'per_price.g.dart';
part 'per_price.freezed.dart';

@freezed
@HiveType(typeId: 4)
class PerPrice with _$PerPrice {
  factory PerPrice({
    @HiveField(1) required String name,
    @HiveField(2) required int id,
  }) = _PerPrice;

  factory PerPrice.fromJson(Map<String, dynamic> json) =>
      _$PerPriceFromJson(json);
}
