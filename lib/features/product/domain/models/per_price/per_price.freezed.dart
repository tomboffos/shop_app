// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'per_price.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

PerPrice _$PerPriceFromJson(Map<String, dynamic> json) {
  return _PerPrice.fromJson(json);
}

/// @nodoc
mixin _$PerPrice {
  @HiveField(1)
  String get name => throw _privateConstructorUsedError;
  @HiveField(2)
  int get id => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $PerPriceCopyWith<PerPrice> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $PerPriceCopyWith<$Res> {
  factory $PerPriceCopyWith(PerPrice value, $Res Function(PerPrice) then) =
      _$PerPriceCopyWithImpl<$Res, PerPrice>;
  @useResult
  $Res call({@HiveField(1) String name, @HiveField(2) int id});
}

/// @nodoc
class _$PerPriceCopyWithImpl<$Res, $Val extends PerPrice>
    implements $PerPriceCopyWith<$Res> {
  _$PerPriceCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? name = null,
    Object? id = null,
  }) {
    return _then(_value.copyWith(
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$PerPriceImplCopyWith<$Res>
    implements $PerPriceCopyWith<$Res> {
  factory _$$PerPriceImplCopyWith(
          _$PerPriceImpl value, $Res Function(_$PerPriceImpl) then) =
      __$$PerPriceImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({@HiveField(1) String name, @HiveField(2) int id});
}

/// @nodoc
class __$$PerPriceImplCopyWithImpl<$Res>
    extends _$PerPriceCopyWithImpl<$Res, _$PerPriceImpl>
    implements _$$PerPriceImplCopyWith<$Res> {
  __$$PerPriceImplCopyWithImpl(
      _$PerPriceImpl _value, $Res Function(_$PerPriceImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? name = null,
    Object? id = null,
  }) {
    return _then(_$PerPriceImpl(
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$PerPriceImpl implements _PerPrice {
  _$PerPriceImpl(
      {@HiveField(1) required this.name, @HiveField(2) required this.id});

  factory _$PerPriceImpl.fromJson(Map<String, dynamic> json) =>
      _$$PerPriceImplFromJson(json);

  @override
  @HiveField(1)
  final String name;
  @override
  @HiveField(2)
  final int id;

  @override
  String toString() {
    return 'PerPrice(name: $name, id: $id)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$PerPriceImpl &&
            (identical(other.name, name) || other.name == name) &&
            (identical(other.id, id) || other.id == id));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, name, id);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$PerPriceImplCopyWith<_$PerPriceImpl> get copyWith =>
      __$$PerPriceImplCopyWithImpl<_$PerPriceImpl>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$PerPriceImplToJson(
      this,
    );
  }
}

abstract class _PerPrice implements PerPrice {
  factory _PerPrice(
      {@HiveField(1) required final String name,
      @HiveField(2) required final int id}) = _$PerPriceImpl;

  factory _PerPrice.fromJson(Map<String, dynamic> json) =
      _$PerPriceImpl.fromJson;

  @override
  @HiveField(1)
  String get name;
  @override
  @HiveField(2)
  int get id;
  @override
  @JsonKey(ignore: true)
  _$$PerPriceImplCopyWith<_$PerPriceImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
