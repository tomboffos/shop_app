// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'per_price.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class PerPriceAdapter extends TypeAdapter<PerPrice> {
  @override
  final int typeId = 4;

  @override
  PerPrice read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return PerPrice(
      name: fields[1] as String,
      id: fields[2] as int,
    );
  }

  @override
  void write(BinaryWriter writer, PerPrice obj) {
    writer
      ..writeByte(2)
      ..writeByte(1)
      ..write(obj.name)
      ..writeByte(2)
      ..write(obj.id);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is PerPriceAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$PerPriceImpl _$$PerPriceImplFromJson(Map<String, dynamic> json) =>
    _$PerPriceImpl(
      name: json['name'] as String,
      id: json['id'] as int,
    );

Map<String, dynamic> _$$PerPriceImplToJson(_$PerPriceImpl instance) =>
    <String, dynamic>{
      'name': instance.name,
      'id': instance.id,
    };
