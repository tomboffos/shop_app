import 'package:hive/hive.dart';
import 'package:shop_app_mobile/core/utils/converters/double_converter.dart';
import 'package:shop_app_mobile/features/category/domain/models/category.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:shop_app_mobile/features/product/domain/models/per_price/per_price.dart';
import 'package:shop_app_mobile/features/product/domain/models/product_media/product_media.dart';

part 'product.freezed.dart';
part 'product.g.dart';

@freezed
@HiveType(typeId: 1)
class Product with _$Product {
  factory Product({
    @HiveField(0) @Default([]) List<Category> categories,
    @HiveField(1) required String name,
    @HiveField(2) String? description,
    @HiveField(3) @DoubleConverter() required double price,
    @HiveField(4)
    @JsonKey(
      name: 'custom_fields',
      defaultValue: {},
    )
    @HiveField(5)
    Map<String, dynamic>? customFields,
    @HiveField(6) @Default([]) List<ProductMedia> medias,
    @HiveField(7)
    @JsonKey(
      name: 'per_price',
    )
    required PerPrice perPrice,
    @HiveField(8) required int id,
  }) = _Product;

  factory Product.fromJson(Map<String, dynamic> json) =>
      _$ProductFromJson(json);
}
