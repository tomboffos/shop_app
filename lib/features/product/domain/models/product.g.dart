// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'product.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class ProductAdapter extends TypeAdapter<Product> {
  @override
  final int typeId = 1;

  @override
  Product read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return Product(
      categories: (fields[0] as List).cast<Category>(),
      name: fields[1] as String,
      description: fields[2] as String?,
      price: fields[3] as double,
      customFields: (fields[4] as Map?)?.cast<String, dynamic>(),
      medias: (fields[6] as List).cast<ProductMedia>(),
      perPrice: fields[7] as PerPrice,
      id: fields[8] as int,
    );
  }

  @override
  void write(BinaryWriter writer, Product obj) {
    writer
      ..writeByte(8)
      ..writeByte(0)
      ..write(obj.categories)
      ..writeByte(1)
      ..write(obj.name)
      ..writeByte(2)
      ..write(obj.description)
      ..writeByte(3)
      ..write(obj.price)
      ..writeByte(4)
      ..write(obj.customFields)
      ..writeByte(6)
      ..write(obj.medias)
      ..writeByte(7)
      ..write(obj.perPrice)
      ..writeByte(8)
      ..write(obj.id);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is ProductAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$ProductImpl _$$ProductImplFromJson(Map<String, dynamic> json) =>
    _$ProductImpl(
      categories: (json['categories'] as List<dynamic>?)
              ?.map((e) => Category.fromJson(e as Map<String, dynamic>))
              .toList() ??
          const [],
      name: json['name'] as String,
      description: json['description'] as String?,
      price: const DoubleConverter().fromJson(json['price'] as String),
      customFields: json['custom_fields'] as Map<String, dynamic>? ?? {},
      medias: (json['medias'] as List<dynamic>?)
              ?.map((e) => ProductMedia.fromJson(e as Map<String, dynamic>))
              .toList() ??
          const [],
      perPrice: PerPrice.fromJson(json['per_price'] as Map<String, dynamic>),
      id: json['id'] as int,
    );

Map<String, dynamic> _$$ProductImplToJson(_$ProductImpl instance) =>
    <String, dynamic>{
      'categories': instance.categories,
      'name': instance.name,
      'description': instance.description,
      'price': const DoubleConverter().toJson(instance.price),
      'custom_fields': instance.customFields,
      'medias': instance.medias,
      'per_price': instance.perPrice,
      'id': instance.id,
    };
