import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:hive/hive.dart';

part 'product_media.g.dart';
part 'product_media.freezed.dart';

@freezed
@HiveType(typeId: 3)
class ProductMedia with _$ProductMedia {
  factory ProductMedia({
    @HiveField(0) required String image,
    @HiveField(1) required String type,
  }) = _ProductMedia;

  factory ProductMedia.fromJson(Map<String, dynamic> json) =>
      _$ProductMediaFromJson(json);
}
