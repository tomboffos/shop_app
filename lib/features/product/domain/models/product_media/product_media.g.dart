// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'product_media.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class ProductMediaAdapter extends TypeAdapter<ProductMedia> {
  @override
  final int typeId = 3;

  @override
  ProductMedia read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return ProductMedia(
      image: fields[0] as String,
      type: fields[1] as String,
    );
  }

  @override
  void write(BinaryWriter writer, ProductMedia obj) {
    writer
      ..writeByte(2)
      ..writeByte(0)
      ..write(obj.image)
      ..writeByte(1)
      ..write(obj.type);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is ProductMediaAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$ProductMediaImpl _$$ProductMediaImplFromJson(Map<String, dynamic> json) =>
    _$ProductMediaImpl(
      image: json['image'] as String,
      type: json['type'] as String,
    );

Map<String, dynamic> _$$ProductMediaImplToJson(_$ProductMediaImpl instance) =>
    <String, dynamic>{
      'image': instance.image,
      'type': instance.type,
    };
