// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'product_media.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

ProductMedia _$ProductMediaFromJson(Map<String, dynamic> json) {
  return _ProductMedia.fromJson(json);
}

/// @nodoc
mixin _$ProductMedia {
  @HiveField(0)
  String get image => throw _privateConstructorUsedError;
  @HiveField(1)
  String get type => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ProductMediaCopyWith<ProductMedia> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ProductMediaCopyWith<$Res> {
  factory $ProductMediaCopyWith(
          ProductMedia value, $Res Function(ProductMedia) then) =
      _$ProductMediaCopyWithImpl<$Res, ProductMedia>;
  @useResult
  $Res call({@HiveField(0) String image, @HiveField(1) String type});
}

/// @nodoc
class _$ProductMediaCopyWithImpl<$Res, $Val extends ProductMedia>
    implements $ProductMediaCopyWith<$Res> {
  _$ProductMediaCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? image = null,
    Object? type = null,
  }) {
    return _then(_value.copyWith(
      image: null == image
          ? _value.image
          : image // ignore: cast_nullable_to_non_nullable
              as String,
      type: null == type
          ? _value.type
          : type // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$ProductMediaImplCopyWith<$Res>
    implements $ProductMediaCopyWith<$Res> {
  factory _$$ProductMediaImplCopyWith(
          _$ProductMediaImpl value, $Res Function(_$ProductMediaImpl) then) =
      __$$ProductMediaImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({@HiveField(0) String image, @HiveField(1) String type});
}

/// @nodoc
class __$$ProductMediaImplCopyWithImpl<$Res>
    extends _$ProductMediaCopyWithImpl<$Res, _$ProductMediaImpl>
    implements _$$ProductMediaImplCopyWith<$Res> {
  __$$ProductMediaImplCopyWithImpl(
      _$ProductMediaImpl _value, $Res Function(_$ProductMediaImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? image = null,
    Object? type = null,
  }) {
    return _then(_$ProductMediaImpl(
      image: null == image
          ? _value.image
          : image // ignore: cast_nullable_to_non_nullable
              as String,
      type: null == type
          ? _value.type
          : type // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$ProductMediaImpl implements _ProductMedia {
  _$ProductMediaImpl(
      {@HiveField(0) required this.image, @HiveField(1) required this.type});

  factory _$ProductMediaImpl.fromJson(Map<String, dynamic> json) =>
      _$$ProductMediaImplFromJson(json);

  @override
  @HiveField(0)
  final String image;
  @override
  @HiveField(1)
  final String type;

  @override
  String toString() {
    return 'ProductMedia(image: $image, type: $type)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ProductMediaImpl &&
            (identical(other.image, image) || other.image == image) &&
            (identical(other.type, type) || other.type == type));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, image, type);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$ProductMediaImplCopyWith<_$ProductMediaImpl> get copyWith =>
      __$$ProductMediaImplCopyWithImpl<_$ProductMediaImpl>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$ProductMediaImplToJson(
      this,
    );
  }
}

abstract class _ProductMedia implements ProductMedia {
  factory _ProductMedia(
      {@HiveField(0) required final String image,
      @HiveField(1) required final String type}) = _$ProductMediaImpl;

  factory _ProductMedia.fromJson(Map<String, dynamic> json) =
      _$ProductMediaImpl.fromJson;

  @override
  @HiveField(0)
  String get image;
  @override
  @HiveField(1)
  String get type;
  @override
  @JsonKey(ignore: true)
  _$$ProductMediaImplCopyWith<_$ProductMediaImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
