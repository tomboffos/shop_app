import 'package:freezed_annotation/freezed_annotation.dart';

part 'product_get_dto.g.dart';
part 'product_get_dto.freezed.dart';

@freezed
class ProductGetDto with _$ProductGetDto {
  factory ProductGetDto({
    @JsonKey(name: 'price_from') String? priceFrom,
    @JsonKey(name: 'price_to') String? priceTo,
    @JsonKey(name: 'category_id') int? categoryId,
    String? search,
    Map<String, dynamic>? filters,
  }) = _ProductGetDto;

  factory ProductGetDto.fromJson(Map<String, dynamic> json) =>
      _$ProductGetDtoFromJson(json);
}
