// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'product_get_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$ProductGetDtoImpl _$$ProductGetDtoImplFromJson(Map<String, dynamic> json) =>
    _$ProductGetDtoImpl(
      priceFrom: json['price_from'] as String?,
      priceTo: json['price_to'] as String?,
      categoryId: json['category_id'] as int?,
      search: json['search'] as String?,
      filters: json['filters'] as Map<String, dynamic>?,
    );

Map<String, dynamic> _$$ProductGetDtoImplToJson(_$ProductGetDtoImpl instance) =>
    <String, dynamic>{
      'price_from': instance.priceFrom,
      'price_to': instance.priceTo,
      'category_id': instance.categoryId,
      'search': instance.search,
      'filters': instance.filters,
    };
