// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'product_get_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

ProductGetDto _$ProductGetDtoFromJson(Map<String, dynamic> json) {
  return _ProductGetDto.fromJson(json);
}

/// @nodoc
mixin _$ProductGetDto {
  @JsonKey(name: 'price_from')
  String? get priceFrom => throw _privateConstructorUsedError;
  @JsonKey(name: 'price_to')
  String? get priceTo => throw _privateConstructorUsedError;
  @JsonKey(name: 'category_id')
  int? get categoryId => throw _privateConstructorUsedError;
  String? get search => throw _privateConstructorUsedError;
  Map<String, dynamic>? get filters => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ProductGetDtoCopyWith<ProductGetDto> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ProductGetDtoCopyWith<$Res> {
  factory $ProductGetDtoCopyWith(
          ProductGetDto value, $Res Function(ProductGetDto) then) =
      _$ProductGetDtoCopyWithImpl<$Res, ProductGetDto>;
  @useResult
  $Res call(
      {@JsonKey(name: 'price_from') String? priceFrom,
      @JsonKey(name: 'price_to') String? priceTo,
      @JsonKey(name: 'category_id') int? categoryId,
      String? search,
      Map<String, dynamic>? filters});
}

/// @nodoc
class _$ProductGetDtoCopyWithImpl<$Res, $Val extends ProductGetDto>
    implements $ProductGetDtoCopyWith<$Res> {
  _$ProductGetDtoCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? priceFrom = freezed,
    Object? priceTo = freezed,
    Object? categoryId = freezed,
    Object? search = freezed,
    Object? filters = freezed,
  }) {
    return _then(_value.copyWith(
      priceFrom: freezed == priceFrom
          ? _value.priceFrom
          : priceFrom // ignore: cast_nullable_to_non_nullable
              as String?,
      priceTo: freezed == priceTo
          ? _value.priceTo
          : priceTo // ignore: cast_nullable_to_non_nullable
              as String?,
      categoryId: freezed == categoryId
          ? _value.categoryId
          : categoryId // ignore: cast_nullable_to_non_nullable
              as int?,
      search: freezed == search
          ? _value.search
          : search // ignore: cast_nullable_to_non_nullable
              as String?,
      filters: freezed == filters
          ? _value.filters
          : filters // ignore: cast_nullable_to_non_nullable
              as Map<String, dynamic>?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$ProductGetDtoImplCopyWith<$Res>
    implements $ProductGetDtoCopyWith<$Res> {
  factory _$$ProductGetDtoImplCopyWith(
          _$ProductGetDtoImpl value, $Res Function(_$ProductGetDtoImpl) then) =
      __$$ProductGetDtoImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {@JsonKey(name: 'price_from') String? priceFrom,
      @JsonKey(name: 'price_to') String? priceTo,
      @JsonKey(name: 'category_id') int? categoryId,
      String? search,
      Map<String, dynamic>? filters});
}

/// @nodoc
class __$$ProductGetDtoImplCopyWithImpl<$Res>
    extends _$ProductGetDtoCopyWithImpl<$Res, _$ProductGetDtoImpl>
    implements _$$ProductGetDtoImplCopyWith<$Res> {
  __$$ProductGetDtoImplCopyWithImpl(
      _$ProductGetDtoImpl _value, $Res Function(_$ProductGetDtoImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? priceFrom = freezed,
    Object? priceTo = freezed,
    Object? categoryId = freezed,
    Object? search = freezed,
    Object? filters = freezed,
  }) {
    return _then(_$ProductGetDtoImpl(
      priceFrom: freezed == priceFrom
          ? _value.priceFrom
          : priceFrom // ignore: cast_nullable_to_non_nullable
              as String?,
      priceTo: freezed == priceTo
          ? _value.priceTo
          : priceTo // ignore: cast_nullable_to_non_nullable
              as String?,
      categoryId: freezed == categoryId
          ? _value.categoryId
          : categoryId // ignore: cast_nullable_to_non_nullable
              as int?,
      search: freezed == search
          ? _value.search
          : search // ignore: cast_nullable_to_non_nullable
              as String?,
      filters: freezed == filters
          ? _value._filters
          : filters // ignore: cast_nullable_to_non_nullable
              as Map<String, dynamic>?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$ProductGetDtoImpl implements _ProductGetDto {
  _$ProductGetDtoImpl(
      {@JsonKey(name: 'price_from') this.priceFrom,
      @JsonKey(name: 'price_to') this.priceTo,
      @JsonKey(name: 'category_id') this.categoryId,
      this.search,
      final Map<String, dynamic>? filters})
      : _filters = filters;

  factory _$ProductGetDtoImpl.fromJson(Map<String, dynamic> json) =>
      _$$ProductGetDtoImplFromJson(json);

  @override
  @JsonKey(name: 'price_from')
  final String? priceFrom;
  @override
  @JsonKey(name: 'price_to')
  final String? priceTo;
  @override
  @JsonKey(name: 'category_id')
  final int? categoryId;
  @override
  final String? search;
  final Map<String, dynamic>? _filters;
  @override
  Map<String, dynamic>? get filters {
    final value = _filters;
    if (value == null) return null;
    if (_filters is EqualUnmodifiableMapView) return _filters;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableMapView(value);
  }

  @override
  String toString() {
    return 'ProductGetDto(priceFrom: $priceFrom, priceTo: $priceTo, categoryId: $categoryId, search: $search, filters: $filters)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ProductGetDtoImpl &&
            (identical(other.priceFrom, priceFrom) ||
                other.priceFrom == priceFrom) &&
            (identical(other.priceTo, priceTo) || other.priceTo == priceTo) &&
            (identical(other.categoryId, categoryId) ||
                other.categoryId == categoryId) &&
            (identical(other.search, search) || other.search == search) &&
            const DeepCollectionEquality().equals(other._filters, _filters));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, priceFrom, priceTo, categoryId,
      search, const DeepCollectionEquality().hash(_filters));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$ProductGetDtoImplCopyWith<_$ProductGetDtoImpl> get copyWith =>
      __$$ProductGetDtoImplCopyWithImpl<_$ProductGetDtoImpl>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$ProductGetDtoImplToJson(
      this,
    );
  }
}

abstract class _ProductGetDto implements ProductGetDto {
  factory _ProductGetDto(
      {@JsonKey(name: 'price_from') final String? priceFrom,
      @JsonKey(name: 'price_to') final String? priceTo,
      @JsonKey(name: 'category_id') final int? categoryId,
      final String? search,
      final Map<String, dynamic>? filters}) = _$ProductGetDtoImpl;

  factory _ProductGetDto.fromJson(Map<String, dynamic> json) =
      _$ProductGetDtoImpl.fromJson;

  @override
  @JsonKey(name: 'price_from')
  String? get priceFrom;
  @override
  @JsonKey(name: 'price_to')
  String? get priceTo;
  @override
  @JsonKey(name: 'category_id')
  int? get categoryId;
  @override
  String? get search;
  @override
  Map<String, dynamic>? get filters;
  @override
  @JsonKey(ignore: true)
  _$$ProductGetDtoImplCopyWith<_$ProductGetDtoImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
