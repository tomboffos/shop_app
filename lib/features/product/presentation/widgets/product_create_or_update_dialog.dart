import 'package:auto_route/auto_route.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:field_suggestion/field_suggestion.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_colorpicker/flutter_colorpicker.dart';
import 'package:image_picker/image_picker.dart';
import 'package:shop_app_mobile/core/injection/injection.dart';
import 'package:shop_app_mobile/core/utils/constants/gaps.dart';
import 'package:shop_app_mobile/core/utils/constants/insets.dart';
import 'package:shop_app_mobile/features/app/widgets/confirmation_dialog.dart';
import 'package:shop_app_mobile/features/category/domain/models/category.dart';
import 'package:shop_app_mobile/features/category/domain/repository/category_repository.dart';
import 'package:shop_app_mobile/features/per_price_option/presentation/cubit/per_price_option_cubit.dart';
import 'package:shop_app_mobile/features/product/domain/create_update_dto/product_create_update_dto.dart';
import 'package:shop_app_mobile/features/product/domain/models/per_price/per_price.dart';

import 'package:shop_app_mobile/features/product/domain/models/product.dart';
import 'package:shop_app_mobile/features/product/presentation/cubit/product_cubit.dart';

class ProductsCreateOrUpdateDialog extends StatefulWidget {
  final Product? product;
  const ProductsCreateOrUpdateDialog({super.key, this.product});

  static show(BuildContext context, {Product? product}) => showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      builder: (context) => ProductsCreateOrUpdateDialog(
            product: product,
          ));

  @override
  State<ProductsCreateOrUpdateDialog> createState() =>
      _ServicesCreateOrUpdateDialogState();
}

class _ServicesCreateOrUpdateDialogState
    extends State<ProductsCreateOrUpdateDialog> {
  final _nameController = TextEditingController();
  final _descriptionController = TextEditingController();
  final _categoriesController = TextEditingController();
  final _priceController = TextEditingController();
  final _perPriceOptionCubit = getIt.get<PerPriceOptionCubit>();
  List<Category> categories = [];
  List<XFile>? images;
  Map<String, dynamic> values = {};
  PerPrice? option;
  @override
  void initState() {
    _perPriceOptionCubit.getOptions();
    if (widget.product != null) {
      _nameController.text = widget.product!.name;
      _descriptionController.text = widget.product!.description ?? '';

      // _durationController.text = widget.product!..toString();
      _priceController.text = (widget.product!.price).toInt().toString();
      option = widget.product!.perPrice;
      categories = [...widget.product!.categories];
      widget.product?.customFields?.keys.forEach((element) {
        values[element] = widget.product?.customFields?[element];
      });
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: MediaQuery.of(context).size.height,
      child: SingleChildScrollView(
        child: Padding(
          padding: Insets.dimens2x,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Align(
                alignment: Alignment.topRight,
                child: InkWell(
                  onTap: () => context.router.pop(),
                  child: const Icon(
                    Icons.close,
                    color: Colors.black,
                    size: 30,
                  ),
                ),
              ),
              Text(
                'Фото',
                style: Theme.of(context).textTheme.headlineSmall,
              ),
              Gaps.gap,
              if (images != null)
                InkWell(
                    onTap: () async {
                      await _pickImage(context);
                    },
                    child: SizedBox(
                      height: 150,
                      child: ListView.builder(
                        itemBuilder: (context, item) => Container(
                          margin: const EdgeInsets.only(right: 10),
                          child: Image.asset(
                            images![item].path,
                            width: 150,
                            height: 150,
                            fit: BoxFit.cover,
                          ),
                        ),
                        itemCount: images!.length,
                        scrollDirection: Axis.horizontal,
                      ),
                    ))
              else if (widget.product != null)
                InkWell(
                  onTap: () async {
                    await _pickImage(context);
                  },
                  child: SizedBox(
                    height: 150,
                    child: ListView.builder(
                        scrollDirection: Axis.horizontal,
                        itemCount: widget.product!.medias.length,
                        itemBuilder: (context, index) {
                          return Container(
                            margin: const EdgeInsets.only(right: 10),
                            child: CachedNetworkImage(
                              imageUrl: widget.product!.medias[index].image,
                              width: 150,
                              height: 150,
                              fit: BoxFit.cover,
                            ),
                          );
                        }),
                  ),
                )
              else
                OutlinedButton(
                  onPressed: () async {
                    await _pickImage(context);
                  },
                  child: const Icon(Icons.add),
                ),
              Gaps.gap,
              Text(
                'Имя',
                style: Theme.of(context).textTheme.headlineSmall,
              ),
              Gaps.gap,
              TextField(
                controller: _nameController,
                decoration: const InputDecoration(
                  hintText: 'Имя',
                ),
              ),
              Gaps.gap,
              Text(
                'Описание',
                style: Theme.of(context).textTheme.headlineSmall,
              ),
              Gaps.gap,
              TextField(
                controller: _descriptionController,
                maxLines: 3,
                decoration: const InputDecoration(
                  hintText: 'Описание',
                ),
              ),
              Gaps.gap,
              Text(
                'Цена',
                style: Theme.of(context).textTheme.headlineSmall,
              ),
              Gaps.gap,
              TextField(
                keyboardType: TextInputType.number,
                controller: _priceController,
                decoration: const InputDecoration(
                  suffix: Text('Т'),
                  hintText: '',
                ),
              ),
              Gaps.gap,
              Text(
                'Единица измерения в цене',
                style: Theme.of(context).textTheme.headlineSmall,
              ),
              Gaps.gap,
              BlocBuilder<PerPriceOptionCubit, PerPriceOptionState>(
                bloc: _perPriceOptionCubit,
                builder: (context, state) {
                  return state.when(
                    initial: () => SizedBox.shrink(),
                    loaded: (data) => DropdownButtonFormField(
                      decoration: const InputDecoration(),
                      value: option,
                      items: data
                          .map(
                            (e) => DropdownMenuItem(
                              value: e,
                              child: Text(e.name),
                            ),
                          )
                          .toList(),
                      onChanged: (value) {
                        setState(() {
                          option = value;
                        });
                      },
                    ),
                  );
                },
              ),
              Gaps.gap,
              Text(
                'Категории',
                style: Theme.of(context).textTheme.headlineSmall,
              ),
              Gaps.gap,
              FieldSuggestion.network(
                textController: _categoriesController,
                builder: (context, value) => ListView.builder(
                  padding: EdgeInsets.zero,
                  itemBuilder: (context, index) => InkWell(
                    onTap: () {
                      setState(() {
                        if (!categories.contains(value.data![index])) {
                          categories.add(value.data![index]);
                        }
                      });
                      _categoriesController.clear();
                    },
                    child: ListTile(
                      contentPadding: EdgeInsets.zero,
                      title: Text(
                        value.data![index].name ?? '',
                        style: Theme.of(context).textTheme.bodyLarge,
                      ),
                      trailing: Text(
                        'Количество субкатегории : ${value.data![index].children}',
                        style: Theme.of(context).textTheme.bodyLarge,
                      ),
                    ),
                  ),
                  itemCount: value.data?.length ?? 0,
                ),
                future: (input) =>
                    getIt.get<CategoryRepository>().getAdmin(search: input),
              ),
              Gaps.gap,
              ListView.builder(
                itemBuilder: (context, index) => ListTile(
                  title: Text(
                    categories[index].name ?? '',
                  ),
                  trailing: IconButton(
                    onPressed: () {
                      setState(() {
                        categories.removeAt(index);
                      });
                    },
                    icon: const Icon(Icons.close),
                  ),
                ),
                itemCount: categories.length,
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
              ),
              Gaps.gap,
              Builder(builder: (context) {
                final fields =
                    categories.expand((e) => e.fields ?? []).toList();
                return ListView.builder(
                  itemCount: fields.length,
                  itemBuilder: (context, index) => Builder(builder: (context) {
                    return Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          fields[index].name,
                          style: Theme.of(context).textTheme.headlineSmall,
                        ),
                        Gaps.gap,
                        SizedBox(
                          child: DropdownButtonFormField(
                            isExpanded: true,
                            items: fields[index]
                                    .values
                                    ?.map<DropdownMenuItem<String>>(
                                      (e) => DropdownMenuItem<String>(
                                        value: e.toString(),
                                        child: fields[index].type == 'color'
                                            ? Container(
                                                width: 50,
                                                height: 50,
                                                margin: const EdgeInsets.only(
                                                    bottom: 10),
                                                color: e.toString().toColor(),
                                              )
                                            : Text(e),
                                      ),
                                    )
                                    .toList() ??
                                [],
                            value: values[fields[index].name],
                            onChanged: (val) {
                              values[fields[index].name] = val;
                            },
                          ),
                        )
                      ],
                    );
                  }),
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                );
              }),
              Gaps.gap,
              OutlinedButton(
                style: const ButtonStyle(
                  minimumSize: MaterialStatePropertyAll(
                    Size(
                      double.infinity,
                      double.minPositive,
                    ),
                  ),
                ),
                onPressed: () async {
                  if (widget.product != null) {
                    if (option != null) {
                      await getIt.get<ProductCubit>().updateProduct(
                            dto: ProductCreateUpdateDto(
                              name: _nameController.text,
                              price: _priceController.text,
                              description: _descriptionController.text,
                              categories: categories.map((e) => e.id).toList(),
                              perPriceOptionId: option!.id,
                              customFields: values,
                            ),
                            medias: images ?? [],
                            id: widget.product!.id,
                            originImages: widget.product!.medias
                                .map((e) => e.image)
                                .toList(),
                          );
                    }
                  } else {
                    if (option != null) {
                      await getIt.get<ProductCubit>().createProduct(
                            dto: ProductCreateUpdateDto(
                              name: _nameController.text,
                              price: _priceController.text,
                              description: _descriptionController.text,
                              categories: categories.map((e) => e.id).toList(),
                              perPriceOptionId: option!.id,
                              customFields: values,
                            ),
                            medias: images ?? [],
                          );
                    }
                  }

                  context.router.pop();
                },
                child: const Padding(
                  padding: EdgeInsets.symmetric(vertical: 10),
                  child: Text('Сохранить'),
                ),
              ),
              Gaps.gap,
              if (widget.product != null)
                FilledButton(
                  style: const ButtonStyle(
                    minimumSize: MaterialStatePropertyAll(
                      Size(
                        double.infinity,
                        double.minPositive,
                      ),
                    ),
                    backgroundColor: MaterialStatePropertyAll(
                      Colors.red,
                    ),
                  ),
                  onPressed: () async {
                    await showDialog(
                      context: context,
                      builder: (context) => ConfirmationDialog(
                        onConfirm: () => getIt
                            .get<ProductCubit>()
                            .deleteProduct(id: widget.product!.id),
                      ),
                    );

                    context.router.pop();
                  },
                  child: const Padding(
                    padding: EdgeInsets.symmetric(vertical: 10),
                    child: Text('Удалить'),
                  ),
                ),
              SizedBox(
                height: 200,
              )
            ],
          ),
        ),
      ),
    );
  }

  Future<void> _pickImage(BuildContext context) async {
    final file = await ImagePicker().pickMultiImage();

    setState(() {
      images = file;
    });
  }
}
