import 'package:auto_route/auto_route.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shop_app_mobile/core/injection/injection.dart';
import 'package:shop_app_mobile/core/routing/app_router.dart';
import 'package:shop_app_mobile/core/utils/constants/gaps.dart';
import 'package:shop_app_mobile/core/utils/constants/radiuses.dart';
import 'package:shop_app_mobile/core/utils/extensions/theme_extension.dart';
import 'package:shop_app_mobile/core/utils/extensions/string_extension.dart';
import 'package:shop_app_mobile/features/cart/presentation/cubit/cart_cubit.dart';
import 'package:shop_app_mobile/features/favorite/presentation/cubit/favorites_cubit.dart';
import 'package:shop_app_mobile/features/product/domain/models/product.dart';

class ProductItem extends StatelessWidget {
  final Product product;
  final Function()? onPressed;

  const ProductItem({super.key, required this.product, this.onPressed});

  @override
  Widget build(BuildContext context) {
    return Card(
      child: InkWell(
        onTap: onPressed ??
            () => context.router.push(
                  ProductDetailRoute(product: product),
                ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            if (product.medias.isNotEmpty)
              SizedBox(
                height: 100,
                width: 200,
                child: Stack(
                  children: [
                    PageView.builder(
                      itemCount: product.medias.length,
                      itemBuilder: (context, index) => ClipRRect(
                        borderRadius: Radiuses.radius,
                        child: CachedNetworkImage(
                          imageUrl: product.medias[index].image,
                          width: MediaQuery.of(context).size.width,
                          fit: BoxFit.contain,
                        ),
                      ),
                    ),
                    BlocBuilder<FavoritesCubit, FavoritesState>(
                      bloc: getIt.get<FavoritesCubit>(),
                      builder: (context, state) {
                        return Align(
                          alignment: Alignment.topRight,
                          child: Transform.translate(
                            offset: const Offset(0, -20),
                            child: IconButton(
                              icon: Icon(
                                state.products.contains(product)
                                    ? Icons.favorite_rounded
                                    : Icons.favorite_outline,
                                color: Colors.red,
                                size: 30,
                              ),
                              onPressed: () {
                                if (!state.products.contains(product)) {
                                  getIt
                                      .get<FavoritesCubit>()
                                      .addProduct(product);
                                } else {
                                  getIt
                                      .get<FavoritesCubit>()
                                      .removeProduct(product);
                                }
                              },
                            ),
                          ),
                        );
                      },
                    ),
                  ],
                ),
              ),
            Gaps.gap2,
            Text(
              product.name,
              style: context.text.titleLarge,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10),
              child: FilledButton(
                onPressed: () {
                  getIt.get<CartCubit>().addItemToCart(product);
                },
                child: Text(
                    '${product.price.toCurrency()} / ${product.perPrice.name}'),
              ),
            )
          ],
        ),
      ),
    );
  }
}
