import 'package:image_picker/image_picker.dart';
import 'package:shop_app_mobile/features/product/domain/create_update_dto/product_create_update_dto.dart';
import 'package:shop_app_mobile/features/product/domain/dto/product_get_dto.dart';
import 'package:shop_app_mobile/features/product/domain/models/product.dart';
import 'package:shop_app_mobile/features/product/domain/repository/product_repository.dart';
import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';

part 'product_state.dart';
part 'product_cubit.freezed.dart';

@injectable
class ProductCubit extends Cubit<ProductState> {
  final ProductRepository _productRepository;

  ProductCubit(this._productRepository) : super(const ProductState.initial());

  Future<void> getProducts(ProductGetDto dto) async {
    try {
      final products = await _productRepository.getProducts(dto);

      emit(ProductState.loaded(products));
    } on Exception {}
  }

  Future<void> createProduct({
    required ProductCreateUpdateDto dto,
    required List<XFile> medias,
  }) =>
      _productRepository.createProduct(
        dto: dto,
        medias: medias,
      );

  Future<void> updateProduct({
    required ProductCreateUpdateDto dto,
    required int id,
    List<XFile>? medias,
    List<String>? originImages,
  }) =>
      _productRepository.updateProduct(
        dto,
        id,
        medias: medias,
        originImages: originImages,
      );

  Future<void> deleteProduct({required int id}) =>
      _productRepository.deleteProduct(id: id);
}
