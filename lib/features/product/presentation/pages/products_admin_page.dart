import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shop_app_mobile/core/injection/injection.dart';
import 'package:shop_app_mobile/core/utils/constants/gaps.dart';
import 'package:shop_app_mobile/core/utils/constants/insets.dart';
import 'package:shop_app_mobile/features/product/domain/dto/product_get_dto.dart';
import 'package:shop_app_mobile/features/product/presentation/cubit/product_cubit.dart';
import 'package:shop_app_mobile/features/product/presentation/widgets/product_item.dart';
import 'package:shop_app_mobile/features/product/presentation/widgets/product_create_or_update_dialog.dart';

@RoutePage()
class ProductsAdminPage extends StatefulWidget {
  const ProductsAdminPage({super.key});

  @override
  State<ProductsAdminPage> createState() => _ServicesAdminPageState();
}

class _ServicesAdminPageState extends State<ProductsAdminPage> {
  final _productsCubit = getIt.get<ProductCubit>();

  @override
  void initState() {
    _productsCubit.getProducts(ProductGetDto());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ProductCubit, ProductState>(
      bloc: _productsCubit,
      builder: (context, state) {
        return state.maybeWhen(
          orElse: () => const Center(
            child: CircularProgressIndicator(),
          ),
          loaded: (products) => Scaffold(
            appBar: AppBar(
              toolbarHeight: 20,
              bottom: PreferredSize(
                preferredSize: Size(MediaQuery.of(context).size.width, 50),
                child: Padding(
                  padding: Insets.dimens,
                  child: TextField(
                    // controller: _searchController,
                    decoration: const InputDecoration(
                      hintText: 'Поиск',
                    ),
                    onChanged: (value) {
                      _productsCubit.getProducts(
                          ProductGetDto(search: value.isEmpty ? null : value));
                    },
                  ),
                ),
              ),
            ),
            body: SingleChildScrollView(
              child: Padding(
                padding: Insets.dimens2x,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Gaps.gap,
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          'Товары',
                          style: Theme.of(context).textTheme.headlineLarge,
                        ),
                        FilledButton(
                          onPressed: () {
                            ProductsCreateOrUpdateDialog.show(context).then(
                              (value) =>
                                  _productsCubit.getProducts(ProductGetDto()),
                            );
                          },
                          child: const Icon(
                            Icons.add,
                          ),
                        )
                      ],
                    ),
                    Gaps.gap,
                    GridView.builder(
                      shrinkWrap: true,
                      physics: const NeverScrollableScrollPhysics(),
                      gridDelegate:
                          const SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 2,
                        childAspectRatio: 3 / 4,
                      ),
                      itemBuilder: (context, index) => ProductItem(
                        product: products[index],
                        onPressed: () => ProductsCreateOrUpdateDialog.show(
                          context,
                          product: products[index],
                        ).then(
                          (value) =>
                              _productsCubit.getProducts(ProductGetDto()),
                        ),
                      ),
                      itemCount: products.length,
                    )
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
