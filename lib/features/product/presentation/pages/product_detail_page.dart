import 'package:auto_route/auto_route.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_colorpicker/flutter_colorpicker.dart';
import 'package:shop_app_mobile/core/injection/injection.dart';
import 'package:shop_app_mobile/core/utils/constants/gaps.dart';
import 'package:shop_app_mobile/core/utils/constants/insets.dart';
import 'package:shop_app_mobile/features/cart/presentation/cubit/cart_cubit.dart';
import 'package:shop_app_mobile/features/product/domain/models/product.dart';
import 'package:shop_app_mobile/features/review/presentation/cubit/review_cubit.dart';
import 'package:shop_app_mobile/features/review/presentation/widgets/review_item.dart';

@RoutePage()
class ProductDetailPage extends StatefulWidget {
  final Product product;

  const ProductDetailPage({super.key, required this.product});

  @override
  State<ProductDetailPage> createState() => _ProductDetailPageState();
}

class _ProductDetailPageState extends State<ProductDetailPage> {
  final _reviewCubit = getIt.get<ReviewCubit>();

  @override
  void initState() {
    _reviewCubit.getReviews(widget.product.id);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: FilledButton(
        onPressed: () {
          getIt.get<CartCubit>().addItemToCart(widget.product);
        },
        style: const ButtonStyle(
          shape: MaterialStatePropertyAll(
            RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(0)),
            ),
          ),
        ),
        child: Padding(
          padding: Insets.dimens2x,
          child: const Text(
            'В корзину',
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: Insets.dimens2x.copyWith(top: 0, bottom: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                width: double.infinity,
                height: 400,
                child: PageView.builder(
                  itemCount: widget.product.medias.length,
                  itemBuilder: (context, index) => CachedNetworkImage(
                    fit: BoxFit.fitWidth,
                    imageUrl:
                        widget.product.medias[index].image.contains('https')
                            ? widget.product.medias[index].image
                            : '',
                  ),
                ),
              ),
              Gaps.gap2,
              Card(
                child: SizedBox(
                  width: double.infinity,
                  child: Padding(
                    padding: Insets.dimens2x,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          widget.product.name,
                          style: Theme.of(context).textTheme.headlineLarge,
                        ),
                        Gaps.gap2,
                        if (widget.product.description != null)
                          Text(
                            widget.product.description ?? '',
                            style: Theme.of(context).textTheme.bodyLarge,
                          ),
                      ],
                    ),
                  ),
                ),
              ),
              Gaps.gap2,
              if (widget.product.customFields != null)
                ExpansionTile(
                  initiallyExpanded: true,
                  title: const Text(
                    'Дополнительные характеристики',
                  ),
                  children: [
                    ListView.builder(
                      itemBuilder: (context, index) =>
                          Builder(builder: (context) {
                        final key =
                            widget.product.customFields!.keys.toList()[index];

                        return ListTile(
                          title: Text(
                            key,
                            style: Theme.of(context).textTheme.bodyLarge,
                          ),
                          trailing: widget.product.customFields![key]
                                  .toString()
                                  .contains('FF')
                              ? Container(
                                  width: 40,
                                  height: 40,
                                  color: widget.product.customFields![key]
                                      .toString()
                                      .toColor(),
                                )
                              : Text(
                                  widget.product.customFields![key].toString(),
                                  style: Theme.of(context).textTheme.bodyLarge,
                                ),
                        );
                      }),
                      shrinkWrap: true,
                      physics: const NeverScrollableScrollPhysics(),
                      itemCount: widget.product.customFields!.keys.length,
                    )
                  ],
                ),
              Gaps.gap2,
              BlocBuilder<ReviewCubit, ReviewState>(
                  bloc: _reviewCubit,
                  builder: (context, state) {
                    return state.maybeWhen(
                        orElse: () => Container(),
                        loaded: (reviews) {
                          final reviewsWithText = reviews
                              .where((element) => element.text != null)
                              .toList();

                          return reviews.isNotEmpty
                              ? ExpansionTile(
                                  title: const Text(
                                    'Отзывы',
                                  ),
                                  children: [
                                    ListView.builder(
                                      itemCount: reviewsWithText.length,
                                      physics:
                                          const NeverScrollableScrollPhysics(),
                                      shrinkWrap: true,
                                      itemBuilder: (context, index) =>
                                          ReviewItem(
                                        review: reviewsWithText[index],
                                      ),
                                    )
                                  ],
                                )
                              : const SizedBox.shrink();
                        });
                  }),
            ],
          ),
        ),
      ),
    );
  }
}
