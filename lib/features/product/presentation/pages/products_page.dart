import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shop_app_mobile/core/injection/injection.dart';
import 'package:shop_app_mobile/core/routing/app_router.dart';
import 'package:shop_app_mobile/core/utils/constants/insets.dart';
import 'package:shop_app_mobile/features/category/domain/models/category.dart';
import 'package:shop_app_mobile/features/product/domain/dto/product_get_dto.dart';
import 'package:shop_app_mobile/features/product/presentation/cubit/product_cubit.dart';
import 'package:shop_app_mobile/features/product/presentation/widgets/product_item.dart';

@RoutePage()
class ProductsPage extends StatefulWidget {
  final Category? category;
  final String? search;

  const ProductsPage({
    super.key,
    this.category,
    this.search,
  });

  @override
  State<ProductsPage> createState() => _ProductsPageState();
}

class _ProductsPageState extends State<ProductsPage> {
  final _productCubit = getIt.get<ProductCubit>();

  final _searchController = TextEditingController();

  ProductGetDto getDto = ProductGetDto();

  @override
  void initState() {
    _productCubit.getProducts(getDto.copyWith(
      categoryId: widget.category?.id,
      search: widget.search,
    ));
    super.initState();
  }

  @override
  void didChangeDependencies() {
    if (widget.search != null) {
      _searchController.text = widget.search!;
    }
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: const SizedBox.shrink(),
        title: Text(widget.category?.name ?? ''),
        centerTitle: true,
        actions: [
          IconButton(
            onPressed: () {
              setState(() {
                getDto = getDto.copyWith(
                  search: null,
                  priceFrom: null,
                  priceTo: null,
                  filters: null,
                );
              });

              _searchController.clear();

              _productCubit.getProducts(getDto);
            },
            icon: const Icon(Icons.close),
          ),
          if (widget.category != null)
            IconButton(
              onPressed: () async {
                final ProductGetDto? filter = await context.router.push(
                  FilterRoute(category: widget.category!),
                );

                if (filter != null) {
                  setState(() {
                    getDto = getDto.copyWith(
                      priceFrom: filter.priceFrom,
                      priceTo: filter.priceTo,
                      filters: filter.filters,
                    );
                  });

                  _productCubit.getProducts(getDto);
                }
              },
              icon: const Icon(Icons.settings),
            ),
        ],
        bottom: PreferredSize(
          preferredSize: Size(MediaQuery.of(context).size.width, 50),
          child: Padding(
            padding: Insets.dimens,
            child: TextField(
              controller: _searchController,
              decoration: const InputDecoration(
                hintText: 'Поиск',
              ),
              onChanged: (value) {
                setState(() {
                  getDto =
                      getDto.copyWith(search: value.isEmpty ? null : value);
                });

                _productCubit.getProducts(getDto);
              },
            ),
          ),
        ),
      ),
      body: BlocBuilder<ProductCubit, ProductState>(
        bloc: _productCubit,
        builder: (context, state) {
          return state.maybeWhen(
            orElse: () => const Padding(
              padding: EdgeInsets.only(top: 100),
              child: Center(
                child: CircularProgressIndicator(),
              ),
            ),
            loaded: (products) => SingleChildScrollView(
              child: Column(
                children: [
                  SizedBox(
                    width: MediaQuery.of(context).size.width,
                    child: GridView.builder(
                      shrinkWrap: true,
                      physics: const NeverScrollableScrollPhysics(),
                      padding: const EdgeInsets.all(10),
                      itemCount: products.length,
                      gridDelegate:
                          const SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 2,
                        crossAxisSpacing: 10,
                        mainAxisSpacing: 10,
                        childAspectRatio: 3 / 4,
                      ),
                      itemBuilder: (context, index) => ProductItem(
                        product: products[index],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
