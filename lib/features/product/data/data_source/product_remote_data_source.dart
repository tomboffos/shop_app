import 'package:image_picker/image_picker.dart';
import 'package:shop_app_mobile/features/product/domain/create_update_dto/product_create_update_dto.dart';
import 'package:shop_app_mobile/features/product/domain/dto/product_get_dto.dart';
import 'package:dio/dio.dart';

abstract class ProductRemoteDataSource {
  static String products = '/products';

  static String productsAdmin = '/products/admin';

  Future<Response> getProducts(
    ProductGetDto dto,
  );

  Future<void> createProduct(
    ProductCreateUpdateDto dto,
    List<XFile>? medias,
  );

  Future<void> updateProduct(
    ProductCreateUpdateDto dto,
    int id, {
    List<XFile>? medias,
    List<String>? originImages,
  });

  Future<void> deleteProduct(
    int id,
  );

  Future<Response> getOptions();
}
