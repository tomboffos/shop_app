import 'package:flutter/foundation.dart';
import 'package:image_picker/image_picker.dart';
import 'package:shop_app_mobile/features/category/data/data_source/category_remote_data_source.dart';
import 'package:shop_app_mobile/features/per_price_option/data/data_source/per_price_option_data_source.dart';
import 'package:shop_app_mobile/features/product/data/data_source/product_remote_data_source.dart';
import 'package:shop_app_mobile/features/product/domain/create_update_dto/product_create_update_dto.dart';
import 'package:shop_app_mobile/features/product/domain/dto/product_get_dto.dart';
import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';

@Injectable(as: ProductRemoteDataSource)
class ProductRemoteDataSourceImpl implements ProductRemoteDataSource {
  final Dio _dio;

  ProductRemoteDataSourceImpl(this._dio) {
    _dio.options.baseUrl += CategoryRemoteDataSource.shop;
  }

  @override
  Future<Response> getProducts(
    ProductGetDto dto,
  ) =>
      _dio.post(
        ProductRemoteDataSource.products,
        data: dto.toJson(),
      );

  @override
  Future<void> createProduct(
    ProductCreateUpdateDto dto,
    List<XFile>? medias,
  ) async {
    Map<String, dynamic> data = {};
    data.addAll(dto.toJson());

    data.remove('categories');
    int index = 0;
    for (var element in dto.categories) {
      data['categories[$index]'] = element;
      index++;
    }

    FormData formData = FormData.fromMap(data);

    if (medias != null) {
      index = 0;
      await Future.forEach(medias, (e) async {
        String fileName = e.path.split('/').last;
        formData.files.add(
          MapEntry(
            'medias[$index]',
            kIsWeb
                ? MultipartFile.fromBytes(
                    await e.readAsBytes(),
                    filename: e.name,
                  )
                : await MultipartFile.fromFile(
                    e.path,
                    filename: fileName,
                  ),
          ),
        );
        index++;
      });
    }

    await _dio.post(ProductRemoteDataSource.productsAdmin, data: formData);
  }

  @override
  Future<void> updateProduct(
    ProductCreateUpdateDto dto,
    int id, {
    List<XFile>? medias,
    List<String>? originImages,
  }) async {
    Map<String, dynamic> data = {};
    data.addAll(dto.toJson());

    data.remove('categories_id');
    data.remove('masters_id');
    int index = 0;
    for (var element in dto.categories) {
      data['categories[$index]'] = element;
      index++;
    }

    if ((medias?.isEmpty ?? true) && originImages != null) {
      index = 0;
      for (var image in originImages) {
        data['medias[$index]'] = image;
      }
    }

    FormData formData = FormData.fromMap(data);

    if (medias != null) {
      int j = 0;
      await Future.forEach(medias, (e) async {
        String fileName = e.path.split('/').last;
        formData.files.add(
          MapEntry(
            'medias[$j]',
            kIsWeb
                ? MultipartFile.fromBytes(
                    await e.readAsBytes(),
                    filename: e.name,
                  )
                : await MultipartFile.fromFile(
                    e.path,
                    filename: fileName,
                  ),
          ),
        );
        j++;
      });
    }

    await _dio.post(
      '${ProductRemoteDataSource.productsAdmin}/$id',
      data: formData,
    );
  }

  @override
  Future<void> deleteProduct(int id) =>
      _dio.delete('${ProductRemoteDataSource.productsAdmin}/$id');

  @override
  Future<Response> getOptions() =>
      _dio.get(PerPriceOptionDataSource.perPriceOptions);
}
