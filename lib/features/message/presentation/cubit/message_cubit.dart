import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';

part 'message_state.dart';
part 'message_cubit.freezed.dart';

@singleton
class MessageCubit extends Cubit<MessageState> {
  MessageCubit() : super(MessageState.initial());

  void addMessage({required String message}) {
    emit(MessageState.addedMessage(message: message));
  }
}
