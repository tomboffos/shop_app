part of 'message_cubit.dart';

@freezed
class MessageState with _$MessageState {
  const factory MessageState.initial() = _Initial;

  const factory MessageState.addedMessage({
    required String message,
  }) = _AddedMessage;
}
