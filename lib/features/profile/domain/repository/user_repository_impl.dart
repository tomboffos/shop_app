import 'package:injectable/injectable.dart';
import 'package:shop_app_mobile/core/services/notification_service.dart';
import 'package:shop_app_mobile/features/profile/data/data_source/local/user_local_data_source.dart';
import 'package:shop_app_mobile/features/profile/data/data_source/remote/user_remote_data_source.dart';
import 'package:shop_app_mobile/features/profile/domain/dto/login_request/login_request_dto.dart';
import 'package:shop_app_mobile/features/profile/domain/dto/register_request/register_request_dto.dart';
import 'package:shop_app_mobile/features/profile/domain/models/user/user.dart';
import 'package:shop_app_mobile/features/profile/domain/repository/user_repository.dart';

@Injectable(as: UserRepository)
class UserRepositoryImpl implements UserRepository {
  final UserRemoteDataSource _dataSource;
  final UserLocalDataSource _localDataSource;
  final NotificationService _service;

  UserRepositoryImpl(
    this._dataSource,
    this._localDataSource,
    this._service,
  );

  @override
  Future<User> authorize(LoginRequestDto dto) async {
    final notificationToken = await _service.getToken();

    var readyDto = dto.copyWith(
      token: notificationToken ?? '',
    );

    final response = await _dataSource.authorize(readyDto);

    final user = User.fromJson(response.data['data']);

    await _localDataSource.saveToken(user.token);

    return user;
  }

  @override
  Future<User> register(RegisterRequestDto dto) async {
    final notificationToken = await _service.getToken();

    var readyDto = dto.copyWith(
      token: notificationToken ?? '',
    );

    final response = await _dataSource.register(readyDto);

    final user = User.fromJson(response.data['data']);

    await _localDataSource.saveToken(user.token);

    return user;
  }

  @override
  Future<User> getUser() async {
    final response = await _dataSource.getUser();

    return User.fromJson(response.data['data']);
  }

  @override
  Future<void> logout() => _localDataSource.logout();

  @override
  Future<void> send({required String title}) => _dataSource.send(title: title);
}
