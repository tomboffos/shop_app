import 'package:shop_app_mobile/features/profile/domain/dto/login_request/login_request_dto.dart';
import 'package:shop_app_mobile/features/profile/domain/dto/register_request/register_request_dto.dart';
import 'package:shop_app_mobile/features/profile/domain/models/user/user.dart';

abstract class UserRepository {
  Future<User> authorize(LoginRequestDto dto);
  Future<User> register(RegisterRequestDto dto);
  Future<User> getUser();
  Future<void> logout();

  Future<void> send({required String title});
}
