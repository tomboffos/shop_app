import 'package:freezed_annotation/freezed_annotation.dart';

part 'login_request_dto.g.dart';
part 'login_request_dto.freezed.dart';

@freezed
class LoginRequestDto with _$LoginRequestDto {
  factory LoginRequestDto({
    required String email,
    required String password,
    required String token,
  }) = _LoginRequestDto;

  factory LoginRequestDto.fromJson(Map<String, dynamic> json) =>
      _$LoginRequestDtoFromJson(json);
}
