import 'package:freezed_annotation/freezed_annotation.dart';

part 'register_request_dto.g.dart';
part 'register_request_dto.freezed.dart';

@freezed
class RegisterRequestDto with _$RegisterRequestDto {
  factory RegisterRequestDto({
    required String email,
    required String password,
    required String token,
    required String name,
  }) = _RegisterRequestDto;

  factory RegisterRequestDto.fromJson(Map<String, dynamic> json) =>
      _$RegisterRequestDtoFromJson(json);
}
