import 'package:dio/dio.dart';
import 'package:shop_app_mobile/features/profile/domain/dto/login_request/login_request_dto.dart';
import 'package:shop_app_mobile/features/profile/domain/dto/register_request/register_request_dto.dart';

abstract class UserRemoteDataSource {
  static String auth = '/auth';
  static String loginApi = '/login';
  static String registerApi = '/register';
  static String userApi = '/user';
  static String notificationApi = '/admin/notification';

  Future<Response> authorize(LoginRequestDto dto);
  Future<Response> register(RegisterRequestDto dto);
  Future<Response> getUser();

  Future<void> send({required String title});
}
