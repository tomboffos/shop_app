import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:shop_app_mobile/features/profile/data/data_source/remote/user_remote_data_source.dart';
import 'package:shop_app_mobile/features/profile/domain/dto/login_request/login_request_dto.dart';
import 'package:shop_app_mobile/features/profile/domain/dto/register_request/register_request_dto.dart';

@Injectable(as: UserRemoteDataSource)
class UserRemoteDataSourceImpl implements UserRemoteDataSource {
  final Dio _dio;

  UserRemoteDataSourceImpl(this._dio) {
    _dio.options.baseUrl += UserRemoteDataSource.auth;
  }

  @override
  Future<Response> authorize(LoginRequestDto dto) => _dio.post(
        UserRemoteDataSource.loginApi,
        data: dto.toJson(),
      );
  @override
  Future<Response> register(RegisterRequestDto dto) => _dio.post(
        UserRemoteDataSource.registerApi,
        data: dto.toJson(),
      );

  @override
  Future<Response> getUser() => _dio.get(UserRemoteDataSource.userApi);

  @override
  Future<void> send({required String title}) =>
      _dio.post(UserRemoteDataSource.notificationApi, data: {
        'title': title,
      });
}
