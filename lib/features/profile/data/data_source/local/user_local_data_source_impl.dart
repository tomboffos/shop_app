import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:injectable/injectable.dart';
import 'package:shop_app_mobile/features/profile/data/data_source/local/user_local_data_source.dart';

@Injectable(as: UserLocalDataSource)
class UserLocalDataSourceImpl implements UserLocalDataSource {
  final FlutterSecureStorage _secureStorage;

  UserLocalDataSourceImpl(this._secureStorage);

  @override
  Future<String?> getToken() =>
      _secureStorage.read(key: UserLocalDataSource.token);

  @override
  Future<void> saveToken(String token) => _secureStorage.write(
        key: UserLocalDataSource.token,
        value: token,
      );

  @override
  Future<void> logout() =>
      _secureStorage.delete(key: UserLocalDataSource.token);
}
