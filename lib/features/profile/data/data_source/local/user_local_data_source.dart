abstract class UserLocalDataSource {
  static String token = 'token';

  Future<void> saveToken(String token);

  Future<String?> getToken();

  Future<void> logout();
}
