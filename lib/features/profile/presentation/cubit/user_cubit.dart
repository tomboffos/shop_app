import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:shop_app_mobile/features/profile/domain/dto/login_request/login_request_dto.dart';
import 'package:shop_app_mobile/features/profile/domain/dto/register_request/register_request_dto.dart';
import 'package:shop_app_mobile/features/profile/domain/models/user/user.dart';
import 'package:shop_app_mobile/features/profile/domain/repository/user_repository.dart';

part 'user_state.dart';
part 'user_cubit.freezed.dart';

@singleton
class UserCubit extends Cubit<UserState> {
  final UserRepository _userRepository;

  UserCubit(this._userRepository) : super(const UserState.unauthorized());

  void login(LoginRequestDto dto) async {
    try {
      final user = await _userRepository.authorize(dto);
      emit(UserState.authorized(user));
    } on DioException catch (e) {
      final prevState = state;

      emit(UserState.error(e.response?.data['message'] ?? ''));

      emit(prevState);
    }
  }

  void register(RegisterRequestDto dto) async {
    try {
      final user = await _userRepository.register(dto);
      emit(UserState.authorized(user));
    } on DioException catch (e) {
      final prevState = state;

      emit(UserState.error(e.response?.statusMessage ?? ''));

      emit(prevState);
    }
  }

  void logout() async {
    await _userRepository.logout();

    emit(const UserState.unauthorized());
  }

  void getUser() async {
    try {
      final user = await _userRepository.getUser();

      emit(UserState.authorized(user));
    } on DioException {
      await _userRepository.logout();

      emit(const UserState.unauthorized());
    }
  }

  Future<void> send({required String title}) async {
    await _userRepository.send(title: title);
  }
}
