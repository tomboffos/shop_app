part of 'user_cubit.dart';

@freezed
class UserState with _$UserState {
  const factory UserState.unauthorized() = _Unauthorized;
  const factory UserState.loading() = _Loading;
  const factory UserState.error(String message) = _Error;
  const factory UserState.authorized(User user) = _Authorized;
}
