import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:shop_app_mobile/core/injection/injection.dart';
import 'package:shop_app_mobile/core/utils/constants/gaps.dart';
import 'package:shop_app_mobile/core/utils/constants/insets.dart';
import 'package:shop_app_mobile/features/profile/presentation/cubit/user_cubit.dart';

class NotificationCreateDialog extends StatefulWidget {
  const NotificationCreateDialog({super.key});

  static show(
    BuildContext context,
  ) =>
      showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        builder: (context) => const NotificationCreateDialog(),
      );

  @override
  State<NotificationCreateDialog> createState() =>
      _NotificationCreateDialogState();
}

class _NotificationCreateDialogState extends State<NotificationCreateDialog> {
  final _nameController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Padding(
        padding: Insets.dimens2x,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Align(
              alignment: Alignment.topRight,
              child: InkWell(
                onTap: () => context.router.pop(),
                child: const Icon(
                  Icons.close,
                  color: Colors.black,
                  size: 30,
                ),
              ),
            ),
            Gaps.gap,
            Gaps.gap,
            Text(
              'Контент',
              style: Theme.of(context).textTheme.headlineSmall,
            ),
            Gaps.gap,
            TextField(
              controller: _nameController,
              decoration: InputDecoration(
                hintText: 'Контент',
              ),
            ),
            Gaps.gap,
            OutlinedButton(
              style: const ButtonStyle(
                minimumSize: MaterialStatePropertyAll(
                  Size(
                    double.infinity,
                    double.minPositive,
                  ),
                ),
              ),
              onPressed: () async {
                await getIt.get<UserCubit>().send(title: _nameController.text);

                context.router.pop();
              },
              child: const Padding(
                padding: EdgeInsets.symmetric(vertical: 10),
                child: Text('Сохранить'),
              ),
            ),
            Gaps.gap2,
          ],
        ),
      ),
    );
  }
}
