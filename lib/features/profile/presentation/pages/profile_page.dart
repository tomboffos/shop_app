import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shop_app_mobile/core/injection/injection.dart';
import 'package:shop_app_mobile/core/routing/app_router.dart';
import 'package:shop_app_mobile/core/utils/constants/gaps.dart';
import 'package:shop_app_mobile/core/utils/constants/insets.dart';
import 'package:shop_app_mobile/core/utils/constants/validator.dart';
import 'package:shop_app_mobile/features/profile/domain/dto/login_request/login_request_dto.dart';
import 'package:shop_app_mobile/features/profile/presentation/cubit/user_cubit.dart';
import 'package:shop_app_mobile/features/profile/presentation/widgets/notification_create_dialog.dart';

@RoutePage()
class ProfilePage extends StatefulWidget {
  const ProfilePage({super.key});

  @override
  State<ProfilePage> createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  final _userCubit = getIt.get<UserCubit>();

  final _passwordController = TextEditingController();
  final _emailController = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<UserCubit, UserState>(
      bloc: _userCubit,
      builder: (context, state) {
        return state.maybeWhen(
          orElse: () => Form(
            key: _formKey,
            onChanged: () => _formKey.currentState?.validate(),
            child: Center(
              child: Card(
                margin: Insets.dimens3x,
                child: Padding(
                  padding: Insets.dimens2x,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Text(
                        'Войдите чтобы посмотреть свой профиль',
                        style: Theme.of(context).textTheme.headlineSmall,
                        textAlign: TextAlign.center,
                      ),
                      Gaps.gap2,
                      TextFormField(
                        controller: _emailController,
                        validator: Validator.email,
                        decoration: InputDecoration(
                          hintText: 'E-mail',
                        ),
                      ),
                      Gaps.gap2,
                      TextFormField(
                        validator: Validator.required,
                        obscureText: true,
                        controller: _passwordController,
                        decoration: InputDecoration(
                          hintText: 'Пароль',
                        ),
                      ),
                      Gaps.gap2,
                      FilledButton(
                        onPressed: () {
                          if (_formKey.currentState?.validate() ?? false) {
                            getIt.get<UserCubit>().login(
                                  LoginRequestDto(
                                      email: _emailController.text,
                                      password: _passwordController.text,
                                      token: 'token'),
                                );
                            _emailController.clear();
                            _passwordController.clear();
                          }
                        },
                        child: const Text('Войти'),
                      ),
                      Gaps.gap,
                      InkWell(
                        onTap: () => context.router.push(const RegisterRoute()),
                        child: const Text(
                          'Зарегистрироваться',
                          style: TextStyle(
                            decoration: TextDecoration.underline,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ),
          authorized: (user) => SingleChildScrollView(
            child: Padding(
              padding: Insets.dimens2x,
              child: Card(
                child: Padding(
                  padding: Insets.dimens2x,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Здравствуйте ${user.name}',
                        style: Theme.of(context).textTheme.headlineMedium,
                      ),
                      Gaps.gap2,
                      InkWell(
                        onTap: () => context.router
                            .push(OrdersRoute(admin: user.roleId == 2)),
                        child: const ListTile(
                          leading: Icon(Icons.shopping_bag),
                          title: Text('Заказы'),
                        ),
                      ),
                      Gaps.gap2,
                      if (user.roleId == 2) ...[
                        // InkWell(
                        //   onTap: () =>
                        //       context.router.push(const NewsAdminRoute()),
                        //   child: const ListTile(
                        //     leading: Icon(Icons.newspaper),
                        //     title: Text('Новости'),
                        //   ),
                        // ),
                        // Gaps.gap2,
                        InkWell(
                          onTap: () =>
                              context.router.push(const SettingUpdateRoute()),
                          child: const ListTile(
                            leading: Icon(Icons.settings),
                            title: Text('Настройки'),
                          ),
                        ),
                        // Gaps.gap2,
                        // InkWell(
                        //   onTap: () => NotificationCreateDialog.show(context),
                        //   child: const ListTile(
                        //     leading: Icon(Icons.notification_add),
                        //     title: Text('Уведомления'),
                        //   ),
                        // ),
                        Gaps.gap2,
                        InkWell(
                          onTap: () => context.router
                              .push(const RecommendationAdminRoute()),
                          child: const ListTile(
                            leading: Icon(Icons.ads_click),
                            title: Text('Рекомендации'),
                          ),
                        ),
                        Gaps.gap2,
                        InkWell(
                          onTap: () =>
                              context.router.push(const ProductsAdminRoute()),
                          child: const ListTile(
                            leading: Icon(Icons.shopping_bag),
                            title: Text('Ваши товары'),
                          ),
                        ),
                        Gaps.gap2,
                        InkWell(
                          onTap: () =>
                              context.router.push(CategoriesRoute(admin: true)),
                          child: const ListTile(
                            leading: Icon(Icons.grid_3x3),
                            title: Text('Категории'),
                          ),
                        ),
                        Gaps.gap2,
                        InkWell(
                          onTap: () =>
                              context.router.push(const FiltersAdminRoute()),
                          child: const ListTile(
                            leading: Icon(Icons.filter),
                            title: Text('Фильтры'),
                          ),
                        ),
                        Gaps.gap2,
                        InkWell(
                          onTap: () =>
                              context.router.push(const PerPriceAdminRoute()),
                          child: const ListTile(
                            leading: Icon(Icons.filter),
                            title: Text('Единицы измерения за продукт'),
                          ),
                        ),
                        Gaps.gap2,
                      ],
                      InkWell(
                        onTap: () => _userCubit.logout(),
                        child: const ListTile(
                          leading: Icon(Icons.logout),
                          title: Text('Выйти'),
                        ),
                      ),
                      Gaps.gap2,
                    ],
                  ),
                ),
              ),
            ),
          ),
        );
      },
      listener: (BuildContext context, UserState state) {
        state.whenOrNull(
          error: (err) => ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(
              content: Text(err),
            ),
          ),
        );
      },
    );
  }
}

@RoutePage()
class ProfileWrapperPage extends StatelessWidget {
  const ProfileWrapperPage({super.key});

  @override
  Widget build(BuildContext context) {
    return const EmptyRouterPage();
  }
}
