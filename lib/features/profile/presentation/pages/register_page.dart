import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shop_app_mobile/core/injection/injection.dart';
import 'package:shop_app_mobile/core/routing/app_router.dart';
import 'package:shop_app_mobile/core/utils/constants/gaps.dart';
import 'package:shop_app_mobile/core/utils/constants/insets.dart';
import 'package:shop_app_mobile/core/utils/constants/validator.dart';
import 'package:shop_app_mobile/features/profile/domain/dto/register_request/register_request_dto.dart';
import 'package:shop_app_mobile/features/profile/presentation/cubit/user_cubit.dart';

@RoutePage()
class RegisterPage extends StatefulWidget {
  const RegisterPage({super.key});

  @override
  State<RegisterPage> createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  final _userCubit = getIt.get<UserCubit>();

  final _nameController = TextEditingController();
  final _passwordController = TextEditingController();
  final _emailController = TextEditingController();

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<UserCubit, UserState>(
      bloc: _userCubit,
      builder: (context, state) {
        return state.maybeWhen(
          loading: () => const Center(
            child: CircularProgressIndicator(),
          ),
          orElse: () => Center(
            child: Form(
              key: _formKey,
              onChanged: () => _formKey.currentState?.validate(),
              child: Card(
                margin: Insets.dimens3x,
                child: Padding(
                  padding: Insets.dimens2x,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Text(
                        'Войдите чтобы посмотреть свой профиль',
                        style: Theme.of(context).textTheme.headlineSmall,
                        textAlign: TextAlign.center,
                      ),
                      Gaps.gap2,
                      TextFormField(
                        validator: Validator.email,
                        controller: _emailController,
                        decoration: const InputDecoration(hintText: 'Email'),
                      ),
                      Gaps.gap2,
                      TextFormField(
                        validator: Validator.required,
                        controller: _nameController,
                        decoration: const InputDecoration(hintText: 'Имя'),
                      ),
                      Gaps.gap2,
                      TextFormField(
                        validator: Validator.required,
                        obscureText: true,
                        controller: _passwordController,
                        decoration: const InputDecoration(
                          hintText: 'Пароль',
                        ),
                      ),
                      Gaps.gap2,
                      FilledButton(
                        onPressed: () {
                          if (_formKey.currentState?.validate() ?? false) {
                            _userCubit.register(
                              RegisterRequestDto(
                                email: _emailController.text,
                                password: _passwordController.text,
                                token: 'token',
                                name: _nameController.text,
                              ),
                            );
                            _emailController.clear();
                            _passwordController.clear();
                            _nameController.clear();
                          }
                        },
                        child: const Text('Зарегистрироваться'),
                      ),
                      Gaps.gap,
                      InkWell(
                        onTap: () =>
                            context.router.replace(const ProfileRoute()),
                        child: const Text(
                          'Логин',
                          style: TextStyle(
                            decoration: TextDecoration.underline,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ),
        );
      },
      listener: (BuildContext context, UserState state) {
        state.whenOrNull(
          error: (err) => ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(
              content: Text(err),
            ),
          ),
          authorized: (user) => context.router.back(),
        );
      },
    );
  }
}
