import 'package:shop_app_mobile/features/news/domain/models/news.dart';
import 'package:shop_app_mobile/features/news/domain/repository/news_repository.dart';
import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';

part 'news_state.dart';
part 'news_cubit.freezed.dart';

@injectable
class NewsCubit extends Cubit<NewsState> {
  final NewsRepository _newsRepository;
  NewsCubit(this._newsRepository) : super(NewsState.initial());

  Future<void> getNews() async {
    try {
      final news = await _newsRepository.getNews();

      emit(
        NewsState.loaded(news),
      );
    } on Exception {}
  }
}
