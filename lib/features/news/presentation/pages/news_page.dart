import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:shop_app_mobile/core/utils/constants/insets.dart';
import 'package:shop_app_mobile/features/news/domain/models/news.dart';
import 'package:shop_app_mobile/features/news/presentation/widgets/news_item.dart';

@RoutePage()
class NewsPage extends StatefulWidget {
  final List<News> news;
  const NewsPage({super.key, required this.news});

  @override
  State<NewsPage> createState() => _NewsPageState();
}

class _NewsPageState extends State<NewsPage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView.builder(
        physics: const NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        itemBuilder: (context, index) => Padding(
          padding: Insets.dimens.copyWith(left: 16, right: 16, bottom: 10),
          child: NewsItem(
            news: widget.news[index],
          ),
        ),
        itemCount: widget.news.length,
      ),
    );
  }
}
