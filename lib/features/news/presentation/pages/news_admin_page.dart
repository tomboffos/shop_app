import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shop_app_mobile/core/injection/injection.dart';
import 'package:shop_app_mobile/core/utils/constants/gaps.dart';
import 'package:shop_app_mobile/core/utils/constants/insets.dart';
import 'package:shop_app_mobile/features/news/presentation/cubit/news_cubit.dart';
import 'package:shop_app_mobile/features/news/presentation/widgets/news_create_update_dialog.dart';
import 'package:shop_app_mobile/features/news/presentation/widgets/news_item.dart';

@RoutePage()
class NewsAdminPage extends StatefulWidget {
  const NewsAdminPage({super.key});

  @override
  State<NewsAdminPage> createState() => _NewsAdminPageState();
}

class _NewsAdminPageState extends State<NewsAdminPage> {
  final _newsCubit = getIt.get<NewsCubit>();
  @override
  void initState() {
    _newsCubit.getNews();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Padding(
        padding: Insets.dimens2x,
        child: BlocBuilder<NewsCubit, NewsState>(
          bloc: _newsCubit,
          builder: (context, state) {
            return Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'Новости',
                      style: Theme.of(context).textTheme.headlineMedium,
                    ),
                    FilledButton(
                      onPressed: () async {
                        await NewsCreateUpdateDialog.show(context);
                        _newsCubit.getNews();
                      },
                      child: const Icon(
                        Icons.add,
                      ),
                    ),
                  ],
                ),
                state.when(
                  initial: () => const Center(
                    child: CircularProgressIndicator(),
                  ),
                  loaded: (data) => ListView.builder(
                    physics: const NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    itemBuilder: (context, index) => Padding(
                      padding: Insets.dimens
                          .copyWith(left: 16, right: 16, bottom: 10),
                      child: NewsItem(
                        news: data[index],
                      ),
                    ),
                    itemCount: data.length,
                  ),
                ),
              ],
            );
          },
        ),
      ),
    );
  }
}
