import 'package:auto_route/auto_route.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_widget_from_html/flutter_widget_from_html.dart';
import 'package:shop_app_mobile/core/utils/constants/gaps.dart';
import 'package:shop_app_mobile/core/utils/constants/insets.dart';
import 'package:shop_app_mobile/features/news/domain/models/news.dart';

@RoutePage()
class NewsDetailPage extends StatelessWidget {
  final News news;

  const NewsDetailPage({super.key, required this.news});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: MediaQuery.of(context).size.height / 3.5,
              child: PageView.builder(
                itemBuilder: (context, index) => CachedNetworkImage(
                  imageUrl: news.images[index],
                  fit: BoxFit.cover,
                  width: MediaQuery.of(context).size.width,
                ),
                itemCount: news.images.length,
              ),
            ),
            Gaps.gap2,
            if (news.body != null)
              Padding(
                padding: Insets.dimens2x,
                child: HtmlWidget(news.body!),
              )
          ],
        ),
      ),
    );
  }
}
