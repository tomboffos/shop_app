import 'package:auto_route/auto_route.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:shop_app_mobile/core/routing/app_router.dart';
import 'package:shop_app_mobile/core/utils/constants/radiuses.dart';
import 'package:shop_app_mobile/core/utils/extensions/theme_extension.dart';
import 'package:shop_app_mobile/features/news/domain/models/news.dart';

class NewsItem extends StatelessWidget {
  final News news;

  const NewsItem({super.key, required this.news});

  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: () => context.router.push(
        NewsDetailRoute(
          news: news,
        ),
      ),
      minVerticalPadding: 30,
      leading: ClipRRect(
        borderRadius: Radiuses.radius,
        child: CachedNetworkImage(
          imageUrl: news.images.firstOrNull ?? '',
          width: 100,
          fit: BoxFit.fitWidth,
          errorWidget: (context, link, obj) => const Icon(
            Icons.newspaper,
            size: 30,
          ),
        ),
      ),
      title: Text(
        news.title,
        style: context.text.bodyLarge,
      ),
      trailing: IconButton(
        onPressed: () {},
        icon: const Icon(
          Icons.arrow_forward,
          size: 20,
        ),
      ),
    );
  }
}
