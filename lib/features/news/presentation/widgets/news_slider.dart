import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shop_app_mobile/core/injection/injection.dart';
import 'package:shop_app_mobile/core/routing/app_router.dart';
import 'package:shop_app_mobile/core/utils/constants/insets.dart';
import 'package:shop_app_mobile/core/utils/extensions/theme_extension.dart';
import 'package:shop_app_mobile/features/news/presentation/cubit/news_cubit.dart';
import 'package:shop_app_mobile/features/news/presentation/widgets/news_item.dart';

class NewsSlider extends StatefulWidget {
  const NewsSlider({super.key});

  @override
  State<NewsSlider> createState() => _NewsSliderState();
}

class _NewsSliderState extends State<NewsSlider> {
  final _newsCubit = getIt.get<NewsCubit>();

  @override
  void initState() {
    _newsCubit.getNews();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<NewsCubit, NewsState>(
      bloc: _newsCubit,
      builder: (BuildContext context, NewsState state) {
        return state.maybeWhen(
          orElse: () => const SizedBox.shrink(),
          loaded: (news) => news.isNotEmpty
              ? Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    InkWell(
                      onTap: () => context.router.push(NewsRoute(
                        news: news,
                      )),
                      child: Padding(
                        padding: Insets.dimens2x.copyWith(
                          bottom: 0,
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              'Последние новости',
                              style: context.text.headlineLarge,
                            ),
                            const Text(
                              'Все',
                              style: TextStyle(
                                decoration: TextDecoration.underline,
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                    SizedBox(
                      height: MediaQuery.of(context).size.height / 6,
                      child: PageView.builder(
                        itemCount: news.length,
                        itemBuilder: (context, index) => Padding(
                          padding: Insets.dimens3x.copyWith(
                            left: 16,
                            right: 16,
                          ),
                          child: NewsItem(news: news[index]),
                        ),
                      ),
                    ),
                  ],
                )
              : const SizedBox.shrink(),
        );
      },
    );
  }

  @override
  void dispose() {
    _newsCubit.close();
    super.dispose();
  }
}
