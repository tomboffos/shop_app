import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:shop_app_mobile/core/utils/constants/gaps.dart';
import 'package:shop_app_mobile/core/utils/constants/insets.dart';
import 'package:shop_app_mobile/features/news/domain/models/news.dart';

class NewsCreateUpdateDialog extends StatefulWidget {
  final News? news;
  const NewsCreateUpdateDialog({super.key, this.news});

  static show(BuildContext context, {News? news}) => showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        builder: (context) => NewsCreateUpdateDialog(
          news: news,
        ),
      );

  @override
  State<NewsCreateUpdateDialog> createState() => _NewsCreateUpdateDialogState();
}

class _NewsCreateUpdateDialogState extends State<NewsCreateUpdateDialog> {
  final _nameController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Padding(
        padding: Insets.dimens2x,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Align(
              alignment: Alignment.topRight,
              child: InkWell(
                onTap: () => context.router.pop(),
                child: const Icon(
                  Icons.close,
                  color: Colors.black,
                  size: 30,
                ),
              ),
            ),
            Gaps.gap,
            Text(
              'Имя',
              style: Theme.of(context).textTheme.headlineSmall,
            ),
            Gaps.gap,
            TextField(
              controller: _nameController,
              decoration: InputDecoration(
                hintText: 'Название',
              ),
            ),
            Gaps.gap,
            Text(
              'Ссылка',
              style: Theme.of(context).textTheme.headlineSmall,
            ),
            Gaps.gap,
            TextField(
              controller: _nameController,
              decoration: InputDecoration(
                hintText: 'Ссылка',
              ),
            ),
            Gaps.gap,
            Text(
              'Ссылка',
              style: Theme.of(context).textTheme.headlineSmall,
            ),
            Gaps.gap,
          ],
        ),
      ),
    );
  }
}
