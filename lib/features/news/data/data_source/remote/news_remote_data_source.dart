import 'package:dio/dio.dart';

abstract class NewsRemoteDataSource {
  static String news = '/news';

  Future<Response> getNews();
}
