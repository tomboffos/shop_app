import 'package:shop_app_mobile/features/banner/data/data_source/banner_remote_data_source.dart';
import 'package:shop_app_mobile/features/news/data/data_source/remote/news_remote_data_source.dart';
import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';

@Injectable(as: NewsRemoteDataSource)
class NewsRemoteDataSourceImpl implements NewsRemoteDataSource {
  final Dio _dio;

  NewsRemoteDataSourceImpl(this._dio) {
    _dio.options.baseUrl += BannerRemoteDataSource.advertisement;
  }

  @override
  Future<Response> getNews() => _dio.get(NewsRemoteDataSource.news);
}
