import 'package:shop_app_mobile/features/news/domain/models/news.dart';

abstract class NewsRepository {
  Future<List<News>> getNews();
}
