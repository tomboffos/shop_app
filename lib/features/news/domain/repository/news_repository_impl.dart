import 'package:shop_app_mobile/features/news/data/data_source/remote/news_remote_data_source.dart';
import 'package:shop_app_mobile/features/news/domain/models/news.dart';
import 'package:shop_app_mobile/features/news/domain/repository/news_repository.dart';
import 'package:injectable/injectable.dart';

@Injectable(as: NewsRepository)
class NewsRepositoryImpl implements NewsRepository {
  final NewsRemoteDataSource _newsRemoteDataSource;

  NewsRepositoryImpl(this._newsRemoteDataSource);

  @override
  Future<List<News>> getNews() async {
    final response = await _newsRemoteDataSource.getNews();

    return (response.data['data'] as List)
        .map((e) => News.fromJson(e))
        .toList();
  }
}
