import 'package:injectable/injectable.dart';
import 'package:shop_app_mobile/features/filters/data/data_source/custom_field_remote_data_source.dart';
import 'package:shop_app_mobile/features/filters/domain/dto/filter_create_update_dto.dart';
import 'package:shop_app_mobile/features/filters/domain/models/custom_field.dart';
import 'package:shop_app_mobile/features/filters/domain/repository/custom_field_repository.dart';

@Injectable(as: CustomFieldRepository)
class CustomFieldRepositoryImpl implements CustomFieldRepository {
  final CustomFieldRemoteDataSource _customFieldRemoteDataSource;

  CustomFieldRepositoryImpl(this._customFieldRemoteDataSource);

  @override
  Future<List<CustomField>> getCustomFields(int categoryId) async {
    final response =
        await _customFieldRemoteDataSource.getCustomFields(categoryId);

    return (response.data['data'] as List)
        .map((e) => CustomField.fromJson(e))
        .toList();
  }

  @override
  Future<List<CustomField>> getAdmin({required String search}) async {
    final response = await _customFieldRemoteDataSource.getAdmin(
      search: search,
    );

    return (response.data['data'] as List)
        .map((e) => CustomField.fromJson(e))
        .toList();
  }

  @override
  Future<void> create({required FilterCreateUpdateDto dto}) =>
      _customFieldRemoteDataSource.createField(dto: dto);

  @override
  Future<void> delete({required int id}) =>
      _customFieldRemoteDataSource.delete(id: id);

  @override
  Future<void> update({required FilterCreateUpdateDto dto, required int id}) =>
      _customFieldRemoteDataSource.update(
        id: id,
        dto: dto,
      );
}
