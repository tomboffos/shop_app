import 'package:shop_app_mobile/features/filters/domain/dto/filter_create_update_dto.dart';
import 'package:shop_app_mobile/features/filters/domain/models/custom_field.dart';

abstract class CustomFieldRepository {
  Future<List<CustomField>> getCustomFields(
    int categoryId,
  );

  Future<List<CustomField>> getAdmin({required String search});

  Future<void> create({required FilterCreateUpdateDto dto});

  Future<void> update({required FilterCreateUpdateDto dto, required int id});

  Future<void> delete({required int id});
}
