import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:hive/hive.dart';
import 'package:shop_app_mobile/features/category/domain/models/category.dart';

part 'custom_field.g.dart';
part 'custom_field.freezed.dart';

@freezed
@HiveType(typeId: 7)
class CustomField with _$CustomField {
  factory CustomField({
    @HiveField(1) required String name,
    @HiveField(2) required String type,
    @HiveField(3) List<dynamic>? values,
    @HiveField(4) required int id,
    @HiveField(5) @Default([]) List<Category> categories,
  }) = _CustomField;

  factory CustomField.fromJson(Map<String, dynamic> json) =>
      _$CustomFieldFromJson(json);
}
