import 'package:freezed_annotation/freezed_annotation.dart';

part 'filter_create_update_dto.g.dart';
part 'filter_create_update_dto.freezed.dart';

@freezed
class FilterCreateUpdateDto with _$FilterCreateUpdateDto {
  factory FilterCreateUpdateDto({
    required String name,
    required String type,
    required List<String> values,
    @Default([]) List<int> categories,
  }) = _FilterCreateUpdateDto;

  factory FilterCreateUpdateDto.fromJson(Map<String, dynamic> json) =>
      _$FilterCreateUpdateDtoFromJson(json);
}
