// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'filter_create_update_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$FilterCreateUpdateDtoImpl _$$FilterCreateUpdateDtoImplFromJson(
        Map<String, dynamic> json) =>
    _$FilterCreateUpdateDtoImpl(
      name: json['name'] as String,
      type: json['type'] as String,
      values:
          (json['values'] as List<dynamic>).map((e) => e as String).toList(),
      categories: (json['categories'] as List<dynamic>?)
              ?.map((e) => e as int)
              .toList() ??
          const [],
    );

Map<String, dynamic> _$$FilterCreateUpdateDtoImplToJson(
        _$FilterCreateUpdateDtoImpl instance) =>
    <String, dynamic>{
      'name': instance.name,
      'type': instance.type,
      'values': instance.values,
      'categories': instance.categories,
    };
