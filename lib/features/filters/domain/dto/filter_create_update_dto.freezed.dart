// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'filter_create_update_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

FilterCreateUpdateDto _$FilterCreateUpdateDtoFromJson(
    Map<String, dynamic> json) {
  return _FilterCreateUpdateDto.fromJson(json);
}

/// @nodoc
mixin _$FilterCreateUpdateDto {
  String get name => throw _privateConstructorUsedError;
  String get type => throw _privateConstructorUsedError;
  List<String> get values => throw _privateConstructorUsedError;
  List<int> get categories => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $FilterCreateUpdateDtoCopyWith<FilterCreateUpdateDto> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $FilterCreateUpdateDtoCopyWith<$Res> {
  factory $FilterCreateUpdateDtoCopyWith(FilterCreateUpdateDto value,
          $Res Function(FilterCreateUpdateDto) then) =
      _$FilterCreateUpdateDtoCopyWithImpl<$Res, FilterCreateUpdateDto>;
  @useResult
  $Res call(
      {String name, String type, List<String> values, List<int> categories});
}

/// @nodoc
class _$FilterCreateUpdateDtoCopyWithImpl<$Res,
        $Val extends FilterCreateUpdateDto>
    implements $FilterCreateUpdateDtoCopyWith<$Res> {
  _$FilterCreateUpdateDtoCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? name = null,
    Object? type = null,
    Object? values = null,
    Object? categories = null,
  }) {
    return _then(_value.copyWith(
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      type: null == type
          ? _value.type
          : type // ignore: cast_nullable_to_non_nullable
              as String,
      values: null == values
          ? _value.values
          : values // ignore: cast_nullable_to_non_nullable
              as List<String>,
      categories: null == categories
          ? _value.categories
          : categories // ignore: cast_nullable_to_non_nullable
              as List<int>,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$FilterCreateUpdateDtoImplCopyWith<$Res>
    implements $FilterCreateUpdateDtoCopyWith<$Res> {
  factory _$$FilterCreateUpdateDtoImplCopyWith(
          _$FilterCreateUpdateDtoImpl value,
          $Res Function(_$FilterCreateUpdateDtoImpl) then) =
      __$$FilterCreateUpdateDtoImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {String name, String type, List<String> values, List<int> categories});
}

/// @nodoc
class __$$FilterCreateUpdateDtoImplCopyWithImpl<$Res>
    extends _$FilterCreateUpdateDtoCopyWithImpl<$Res,
        _$FilterCreateUpdateDtoImpl>
    implements _$$FilterCreateUpdateDtoImplCopyWith<$Res> {
  __$$FilterCreateUpdateDtoImplCopyWithImpl(_$FilterCreateUpdateDtoImpl _value,
      $Res Function(_$FilterCreateUpdateDtoImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? name = null,
    Object? type = null,
    Object? values = null,
    Object? categories = null,
  }) {
    return _then(_$FilterCreateUpdateDtoImpl(
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      type: null == type
          ? _value.type
          : type // ignore: cast_nullable_to_non_nullable
              as String,
      values: null == values
          ? _value._values
          : values // ignore: cast_nullable_to_non_nullable
              as List<String>,
      categories: null == categories
          ? _value._categories
          : categories // ignore: cast_nullable_to_non_nullable
              as List<int>,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$FilterCreateUpdateDtoImpl implements _FilterCreateUpdateDto {
  _$FilterCreateUpdateDtoImpl(
      {required this.name,
      required this.type,
      required final List<String> values,
      final List<int> categories = const []})
      : _values = values,
        _categories = categories;

  factory _$FilterCreateUpdateDtoImpl.fromJson(Map<String, dynamic> json) =>
      _$$FilterCreateUpdateDtoImplFromJson(json);

  @override
  final String name;
  @override
  final String type;
  final List<String> _values;
  @override
  List<String> get values {
    if (_values is EqualUnmodifiableListView) return _values;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_values);
  }

  final List<int> _categories;
  @override
  @JsonKey()
  List<int> get categories {
    if (_categories is EqualUnmodifiableListView) return _categories;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_categories);
  }

  @override
  String toString() {
    return 'FilterCreateUpdateDto(name: $name, type: $type, values: $values, categories: $categories)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$FilterCreateUpdateDtoImpl &&
            (identical(other.name, name) || other.name == name) &&
            (identical(other.type, type) || other.type == type) &&
            const DeepCollectionEquality().equals(other._values, _values) &&
            const DeepCollectionEquality()
                .equals(other._categories, _categories));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      name,
      type,
      const DeepCollectionEquality().hash(_values),
      const DeepCollectionEquality().hash(_categories));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$FilterCreateUpdateDtoImplCopyWith<_$FilterCreateUpdateDtoImpl>
      get copyWith => __$$FilterCreateUpdateDtoImplCopyWithImpl<
          _$FilterCreateUpdateDtoImpl>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$FilterCreateUpdateDtoImplToJson(
      this,
    );
  }
}

abstract class _FilterCreateUpdateDto implements FilterCreateUpdateDto {
  factory _FilterCreateUpdateDto(
      {required final String name,
      required final String type,
      required final List<String> values,
      final List<int> categories}) = _$FilterCreateUpdateDtoImpl;

  factory _FilterCreateUpdateDto.fromJson(Map<String, dynamic> json) =
      _$FilterCreateUpdateDtoImpl.fromJson;

  @override
  String get name;
  @override
  String get type;
  @override
  List<String> get values;
  @override
  List<int> get categories;
  @override
  @JsonKey(ignore: true)
  _$$FilterCreateUpdateDtoImplCopyWith<_$FilterCreateUpdateDtoImpl>
      get copyWith => throw _privateConstructorUsedError;
}
