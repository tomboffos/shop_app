import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shop_app_mobile/core/injection/injection.dart';
import 'package:shop_app_mobile/core/utils/constants/gaps.dart';
import 'package:shop_app_mobile/core/utils/constants/insets.dart';
import 'package:shop_app_mobile/features/filters/presentation/cubit/custom_field_cubit.dart';
import 'package:shop_app_mobile/features/filters/presentation/widgets/filter_create_or_update_dialog.dart';

@RoutePage()
class FiltersAdminPage extends StatefulWidget {
  const FiltersAdminPage({super.key});

  @override
  State<FiltersAdminPage> createState() => _FiltersAdminPageState();
}

class _FiltersAdminPageState extends State<FiltersAdminPage> {
  final _filterCubit = getIt.get<CustomFieldCubit>();
  @override
  void initState() {
    _filterCubit.getAdminFields();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CustomFieldCubit, CustomFieldState>(
      bloc: _filterCubit,
      builder: (context, state) {
        return Padding(
          padding: Insets.dimens2x,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'Фильтры',
                    style: Theme.of(context).textTheme.headlineMedium,
                  ),
                  FilledButton(
                    onPressed: () async {
                      await FitlerCreateOrUpdateDialog.show(context);

                      _filterCubit.getAdminFields();
                    },
                    child: const Icon(
                      Icons.add,
                    ),
                  )
                ],
              ),
              Gaps.gap2,
              state.when(
                  initial: () => const Center(
                        child: CircularProgressIndicator(),
                      ),
                  loaded: (val) {
                    return ListView.builder(
                      itemBuilder: (context, index) => ListTile(
                        title: Text(val[index].name),
                        onTap: () async {
                          await FitlerCreateOrUpdateDialog.show(context,
                              field: val[index]);

                          _filterCubit.getAdminFields();
                        },
                      ),
                      shrinkWrap: true,
                      itemCount: val.length,
                      physics: const NeverScrollableScrollPhysics(),
                    );
                  }),
            ],
          ),
        );
      },
    );
  }
}
