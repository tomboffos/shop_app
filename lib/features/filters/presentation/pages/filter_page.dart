import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shop_app_mobile/core/injection/injection.dart';
import 'package:shop_app_mobile/core/utils/constants/gaps.dart';
import 'package:shop_app_mobile/core/utils/constants/insets.dart';
import 'package:shop_app_mobile/features/category/domain/models/category.dart';
import 'package:shop_app_mobile/features/filters/presentation/cubit/custom_field_cubit.dart';
import 'package:shop_app_mobile/features/filters/presentation/widgets/custom_field_item.dart';
import 'package:shop_app_mobile/features/product/domain/dto/product_get_dto.dart';

@RoutePage<ProductGetDto>()
class FilterPage extends StatefulWidget {
  final Category category;

  const FilterPage({super.key, required this.category});

  @override
  State<FilterPage> createState() => _FilterPageState();
}

class _FilterPageState extends State<FilterPage> {
  final _filterCubit = getIt.get<CustomFieldCubit>();

  @override
  void initState() {
    _filterCubit.getCustomFields(widget.category.id);
    super.initState();
  }

  Map<String, dynamic> filters = {};

  final TextEditingController _priceFromController = TextEditingController();

  final TextEditingController _priceToController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Фильтр'),
        leading: const SizedBox.shrink(),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: SizedBox(
        width: MediaQuery.of(context).size.width - 32,
        height: 50,
        child: FilledButton(
          onPressed: () {
            context.router.pop(
              ProductGetDto(
                priceFrom: _priceFromController.text.isEmpty
                    ? null
                    : _priceFromController.text,
                priceTo: _priceToController.text.isEmpty
                    ? null
                    : _priceToController.text,
                filters: filters.isEmpty ? null : filters,
              ),
            );
          },
          child: const Text('Поиск'),
        ),
      ),
      body: BlocBuilder<CustomFieldCubit, CustomFieldState>(
        bloc: _filterCubit,
        builder: (context, state) {
          return SingleChildScrollView(
            child: Padding(
              padding: Insets.dimens2x,
              child: Form(
                child: Column(
                  children: [
                    Card(
                      child: Padding(
                        padding: Insets.dimens2x,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'Цена',
                              style: Theme.of(context).textTheme.headlineSmall,
                            ),
                            Gaps.gap,
                            Row(
                              children: [
                                Expanded(
                                  child: TextFormField(
                                    controller: _priceFromController,
                                    decoration: const InputDecoration(
                                      prefixText: 'От ',
                                    ),
                                  ),
                                ),
                                const SizedBox(
                                  width: 20,
                                ),
                                Expanded(
                                  child: TextFormField(
                                    controller: _priceToController,
                                    decoration: const InputDecoration(
                                      prefixText: 'До ',
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                    state.maybeWhen(
                      orElse: () => const SizedBox.shrink(),
                      loaded: (fields) => ListView.builder(
                        itemCount: fields.length,
                        shrinkWrap: true,
                        physics: const NeverScrollableScrollPhysics(),
                        itemBuilder: (context, index) => CustomFieldItem(
                          field: fields[index],
                          onChange: (values) {
                            filters[fields[index].name] = values;
                          },
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
