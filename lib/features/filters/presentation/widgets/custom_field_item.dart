import 'package:flutter/material.dart';
import 'package:shop_app_mobile/core/utils/constants/gaps.dart';
import 'package:shop_app_mobile/core/utils/constants/insets.dart';
import 'package:shop_app_mobile/features/filters/domain/models/custom_field.dart';

class CustomFieldItem extends StatefulWidget {
  final CustomField field;
  final Function(dynamic)? onChange;

  const CustomFieldItem({
    super.key,
    required this.field,
    this.onChange,
  });

  @override
  State<CustomFieldItem> createState() => _CustomFieldItemState();
}

class _CustomFieldItemState extends State<CustomFieldItem> {
  dynamic fieldValue;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Gaps.gap,
        Card(
          child: Padding(
            padding: Insets.dimens2x,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  widget.field.name,
                  style: Theme.of(context).textTheme.headlineSmall,
                ),
                Gaps.gap,
                Row(
                  children: [
                    if (widget.field.type == 'choose')
                      Expanded(
                        child: DropdownButtonFormField<dynamic>(
                          value: fieldValue,
                          isExpanded: true,
                          items: widget.field.values
                                  ?.map(
                                    (e) => DropdownMenuItem(
                                      value: e,
                                      child: Text(
                                        e.toString(),
                                      ),
                                    ),
                                  )
                                  .toList() ??
                              [],
                          onChanged: (values) {
                            setState(() {
                              fieldValue = values;
                            });

                            widget.onChange?.call(values);
                          },
                        ),
                      )
                    else
                      Expanded(
                        child: TextFormField(
                          decoration: const InputDecoration(
                            hintText: 'От',
                          ),
                        ),
                      ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
