import 'package:auto_route/auto_route.dart';
import 'package:field_suggestion/field_suggestion.dart';
import 'package:flutter/material.dart';
import 'package:flutter_colorpicker/flutter_colorpicker.dart';
import 'package:shop_app_mobile/core/injection/injection.dart';
import 'package:shop_app_mobile/core/utils/constants/gaps.dart';
import 'package:shop_app_mobile/core/utils/constants/insets.dart';
import 'package:shop_app_mobile/features/app/widgets/confirmation_dialog.dart';
import 'package:shop_app_mobile/features/category/domain/models/category.dart';
import 'package:shop_app_mobile/features/category/domain/repository/category_repository.dart';
import 'package:shop_app_mobile/features/filters/domain/dto/filter_create_update_dto.dart';
import 'package:shop_app_mobile/features/filters/domain/models/custom_field.dart';
import 'package:shop_app_mobile/features/filters/presentation/cubit/custom_field_cubit.dart';
import 'package:shop_app_mobile/features/settings/presentation/widgets/dialogs/color_pick_dialog.dart';

class FitlerCreateOrUpdateDialog extends StatefulWidget {
  final CustomField? field;
  const FitlerCreateOrUpdateDialog({super.key, this.field});

  static show(BuildContext context, {CustomField? field}) =>
      showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        builder: (context) => FitlerCreateOrUpdateDialog(
          field: field,
        ),
      );

  @override
  State<FitlerCreateOrUpdateDialog> createState() =>
      _FitlerCreateOrUpdateDialogState();
}

class _FitlerCreateOrUpdateDialogState
    extends State<FitlerCreateOrUpdateDialog> {
  final _categoriesController = TextEditingController();
  final _optionsController = TextEditingController();
  final _nameController = TextEditingController();
  List<Category> categories = [];
  List<String> values = [];
  String? type;

  @override
  void initState() {
    if (widget.field != null) {
      _nameController.text = widget.field?.name ?? '';

      values = widget.field?.values?.map((e) => e.toString()).toList() ?? [];

      type = widget.field?.type;
      categories = [...(widget.field?.categories ?? [])];
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: MediaQuery.of(context).size.height * 0.7,
      child: SingleChildScrollView(
        child: Padding(
          padding: Insets.dimens2x,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Gaps.gap,
              Text(
                'Имя',
                style: Theme.of(context).textTheme.headlineSmall,
              ),
              Gaps.gap,
              TextField(
                controller: _nameController,
              ),
              Gaps.gap,
              Text(
                'Тип фильтра',
                style: Theme.of(context).textTheme.headlineSmall,
              ),
              Gaps.gap,
              DropdownButtonFormField<dynamic>(
                items: [
                  DropdownMenuItem(
                    child: Text('Цвета'),
                    value: 'color',
                  ),
                  DropdownMenuItem(
                    child: Text('Выбор по тексту'),
                    value: 'choose',
                  )
                ],
                value: type,
                onChanged: (value) {
                  setState(() {
                    if (type != value) {
                      values = [];
                    }
                    type = value;
                  });
                },
              ),
              Gaps.gap,
              Text(
                'Опции',
                style: Theme.of(context).textTheme.headlineSmall,
              ),
              TextField(
                controller: _optionsController,
                onTap: type != null && type == 'color'
                    ? () => showDialog(
                          context: context,
                          builder: (context) => ColorPickDialog(
                            givenColor: Colors.white,
                            onColorChange: (val) {
                              setState(() {
                                values.add(val.toHexString());
                              });
                            },
                          ),
                        )
                    : null,
                readOnly: type != null && type == 'color',
                onSubmitted: (val) {
                  if (type != null && type == 'choose') {
                    values.add(val);
                    _optionsController.clear();
                  }
                },
              ),
              Gaps.gap,
              ListView.builder(
                itemBuilder: (context, index) => ListTile(
                  title: type != null && type == 'color'
                      ? Container(
                          width: 50,
                          height: 50,
                          constraints: const BoxConstraints(maxWidth: 50),
                          decoration: BoxDecoration(
                            color: values[index].toColor(),
                          ),
                        )
                      : Text(
                          values[index],
                        ),
                  trailing: IconButton(
                    onPressed: () {
                      setState(() {
                        values.removeAt(index);
                      });
                    },
                    icon: const Icon(Icons.close),
                  ),
                ),
                itemCount: values.length,
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
              ),
              Gaps.gap,
              Text(
                'Категории',
                style: Theme.of(context).textTheme.headlineSmall,
              ),
              Gaps.gap,
              FieldSuggestion.network(
                textController: _categoriesController,
                builder: (context, value) => ListView.builder(
                  padding: EdgeInsets.zero,
                  itemBuilder: (context, index) => InkWell(
                    onTap: () {
                      setState(() {
                        if (!categories.contains(value.data![index])) {
                          categories.add(value.data![index]);
                        }
                      });
                      _categoriesController.clear();
                    },
                    child: ListTile(
                      contentPadding: EdgeInsets.zero,
                      title: Text(
                        value.data![index].name ?? '',
                        style: Theme.of(context).textTheme.bodyLarge,
                      ),
                      trailing: Text(
                        'Количество субкатегории : ${value.data![index].children}',
                        style: Theme.of(context).textTheme.bodyLarge,
                      ),
                    ),
                  ),
                  itemCount: value.data?.length ?? 0,
                ),
                future: (input) =>
                    getIt.get<CategoryRepository>().getAdmin(search: input),
              ),
              Gaps.gap,
              ListView.builder(
                itemBuilder: (context, index) => ListTile(
                  title: Text(
                    categories[index].name ?? '',
                  ),
                  trailing: IconButton(
                    onPressed: () {
                      setState(() {
                        categories.removeAt(index);
                      });
                    },
                    icon: const Icon(Icons.close),
                  ),
                ),
                itemCount: categories.length,
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
              ),
              Gaps.gap,
              OutlinedButton(
                style: const ButtonStyle(
                  minimumSize: MaterialStatePropertyAll(
                    Size(
                      double.infinity,
                      double.minPositive,
                    ),
                  ),
                ),
                onPressed: () async {
                  if (widget.field != null) {
                    await getIt.get<CustomFieldCubit>().update(
                          dto: FilterCreateUpdateDto(
                            name: _nameController.text,
                            type: type ?? 'choose',
                            values: values,
                            categories: categories.map((e) => e.id).toList(),
                          ),
                          id: widget.field!.id,
                        );
                  } else {
                    await getIt.get<CustomFieldCubit>().create(
                          dto: FilterCreateUpdateDto(
                            name: _nameController.text,
                            type: type ?? 'choose',
                            values: values,
                            categories: categories.map((e) => e.id).toList(),
                          ),
                        );
                  }

                  context.router.pop();
                },
                child: const Padding(
                  padding: EdgeInsets.symmetric(vertical: 10),
                  child: Text('Сохранить'),
                ),
              ),
              Gaps.gap,
              if (widget.field != null)
                FilledButton(
                  style: const ButtonStyle(
                    minimumSize: MaterialStatePropertyAll(
                      Size(
                        double.infinity,
                        double.minPositive,
                      ),
                    ),
                    backgroundColor: MaterialStatePropertyAll(
                      Colors.red,
                    ),
                  ),
                  onPressed: () async {
                    await showDialog(
                      context: context,
                      builder: (context) => ConfirmationDialog(
                        onConfirm: () => getIt
                            .get<CustomFieldCubit>()
                            .delete(id: widget.field!.id),
                      ),
                    );

                    context.router.pop();
                  },
                  child: const Padding(
                    padding: EdgeInsets.symmetric(vertical: 10),
                    child: Text('Удалить'),
                  ),
                ),
              SizedBox(
                height: 200,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
