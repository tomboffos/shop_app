part of 'custom_field_cubit.dart';

@freezed
class CustomFieldState with _$CustomFieldState {
  const factory CustomFieldState.initial() = _Initial;

  const factory CustomFieldState.loaded(List<CustomField> customFields) =
      _Loaded;
}
