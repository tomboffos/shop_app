import 'package:injectable/injectable.dart';
import 'package:shop_app_mobile/features/filters/domain/dto/filter_create_update_dto.dart';
import 'package:shop_app_mobile/features/filters/domain/models/custom_field.dart';
import 'package:shop_app_mobile/features/filters/domain/repository/custom_field_repository.dart';
import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'custom_field_state.dart';
part 'custom_field_cubit.freezed.dart';

@injectable
class CustomFieldCubit extends Cubit<CustomFieldState> {
  final CustomFieldRepository _customFieldRepository;

  CustomFieldCubit(this._customFieldRepository)
      : super(
          const CustomFieldState.initial(),
        );

  Future<void> getCustomFields(int categoryId) async {
    try {
      final fields = await _customFieldRepository.getCustomFields(categoryId);

      emit(CustomFieldState.loaded(fields));
    } on Exception {}
  }

  Future<void> getAdminFields() async {
    try {
      final fields = await _customFieldRepository.getAdmin(search: '');

      emit(CustomFieldState.loaded(fields));
    } on Exception {}
  }

  Future<void> create({required FilterCreateUpdateDto dto}) =>
      _customFieldRepository.create(dto: dto);

  Future<void> update({required FilterCreateUpdateDto dto, required int id}) =>
      _customFieldRepository.update(
        dto: dto,
        id: id,
      );

  Future<void> delete({required int id}) => _customFieldRepository.delete(
        id: id,
      );
}
