import 'package:injectable/injectable.dart';
import 'package:shop_app_mobile/features/category/data/data_source/category_remote_data_source.dart';
import 'package:shop_app_mobile/features/filters/data/data_source/custom_field_remote_data_source.dart';
import 'package:dio/dio.dart';
import 'package:shop_app_mobile/features/filters/domain/dto/filter_create_update_dto.dart';

@Injectable(as: CustomFieldRemoteDataSource)
class CustomFieldRemoteDataSourceImpl implements CustomFieldRemoteDataSource {
  final Dio _dio;

  CustomFieldRemoteDataSourceImpl(this._dio) {
    _dio.options.baseUrl += CategoryRemoteDataSource.shop;
  }

  @override
  Future<Response> getCustomFields(int categoryId) => _dio.get(
        '${CustomFieldRemoteDataSource.filters}/$categoryId',
      );

  @override
  Future<Response> getAdmin({required String search}) => _dio.get(
        CustomFieldRemoteDataSource.filtersAdmin,
        data: {
          'search': search,
        },
      );

  @override
  Future<void> createField({required FilterCreateUpdateDto dto}) => _dio.post(
        CustomFieldRemoteDataSource.filtersAdmin,
        data: dto.toJson(),
      );

  @override
  Future<void> delete({required int id}) => _dio.delete(
        '${CustomFieldRemoteDataSource.filtersAdmin}/$id',
      );

  @override
  Future<void> update({required FilterCreateUpdateDto dto, required int id}) =>
      _dio.post(
        '${CustomFieldRemoteDataSource.filtersAdmin}/$id',
        data: dto.toJson(),
      );
}
