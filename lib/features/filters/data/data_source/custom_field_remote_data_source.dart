import 'package:dio/dio.dart';
import 'package:shop_app_mobile/features/filters/domain/dto/filter_create_update_dto.dart';

abstract class CustomFieldRemoteDataSource {
  static String filters = '/filters';
  static String filtersAdmin = '/filters/admin';

  Future<Response> getCustomFields(int categoryId);

  Future<Response> getAdmin({required String search});

  Future<void> createField({required FilterCreateUpdateDto dto});

  Future<void> update({required FilterCreateUpdateDto dto, required int id});

  Future<void> delete({required int id});
}
