import 'package:shop_app_mobile/features/settings/domain/models/setting.dart';

abstract class SettingLocalDataSource {
  static String setting = 'setting';

  Future<void> saveSetting(Setting setting);

  Future<Setting> getSetting();
}
