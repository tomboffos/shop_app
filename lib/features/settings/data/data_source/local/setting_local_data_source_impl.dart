import 'package:hive/hive.dart';
import 'package:injectable/injectable.dart';
import 'package:shop_app_mobile/features/settings/data/data_source/local/setting_local_data_source.dart';
import 'package:shop_app_mobile/features/settings/domain/models/setting.dart';

@Injectable(as: SettingLocalDataSource)
class SettingLocalDataSourceImpl implements SettingLocalDataSource {
  @override
  Future<Setting> getSetting() async {
    var setting = await Hive.box(SettingLocalDataSource.setting).values.first;
    return Setting.fromJson(
        (setting as Map).map((key, value) => MapEntry(key.toString(), value)));
  }

  @override
  Future<void> saveSetting(Setting setting) async {
    await Hive.box(SettingLocalDataSource.setting).clear();

    await Hive.box(SettingLocalDataSource.setting).add(setting.toJson());
  }
}
