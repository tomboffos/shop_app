import 'package:flutter/foundation.dart';
import 'package:image_picker/image_picker.dart';
import 'package:shop_app_mobile/features/settings/data/data_source/remote/setting_remote_data_source.dart';
import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:shop_app_mobile/features/settings/domain/dto/setting_update_dto.dart';

@Injectable(as: SettingRemoteDataSource)
class SettingRemoteDataSourceImpl implements SettingRemoteDataSource {
  final Dio _dio;

  SettingRemoteDataSourceImpl(this._dio);

  @override
  Future<Response> getSetting() => _dio.get(SettingRemoteDataSource.api);

  @override
  Future<void> updateSettings(SettingUpdateDto dto) => _dio.post(
        SettingRemoteDataSource.apiUpdate,
        data: dto.toJson(),
      );

  @override
  Future<void> uploadFile(String name, XFile image) async {
    FormData formData = FormData.fromMap({
      'name': name,
    });
    String fileName = image.path.split('/').last;
    formData.files.add(
      MapEntry(
        'value',
        kIsWeb
            ? MultipartFile.fromBytes(
                await image.readAsBytes(),
                filename: fileName,
              )
            : await MultipartFile.fromFile(
                image.path,
                filename: fileName,
              ),
      ),
    );
    await _dio.post(
      SettingRemoteDataSource.apiUpdate,
      data: formData,
    );
  }
}
