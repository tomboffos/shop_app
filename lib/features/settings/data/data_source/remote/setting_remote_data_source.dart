import 'package:dio/dio.dart';
import 'package:image_picker/image_picker.dart';
import 'package:shop_app_mobile/features/settings/domain/dto/setting_update_dto.dart';

abstract class SettingRemoteDataSource {
  static String api = '/settings';
  static String apiUpdate = '/admin/settings';

  Future<Response> getSetting();

  Future<void> updateSettings(SettingUpdateDto dto);

  Future<void> uploadFile(String name, XFile image);
}
