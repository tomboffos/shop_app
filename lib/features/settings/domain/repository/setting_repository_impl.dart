import 'package:image_picker/image_picker.dart';
import 'package:shop_app_mobile/features/settings/data/data_source/local/setting_local_data_source.dart';
import 'package:shop_app_mobile/features/settings/data/data_source/remote/setting_remote_data_source.dart';
import 'package:shop_app_mobile/features/settings/domain/dto/setting_update_dto.dart';
import 'package:shop_app_mobile/features/settings/domain/models/setting.dart';
import 'package:shop_app_mobile/features/settings/domain/repository/setting_repository.dart';
import 'package:injectable/injectable.dart';

@Injectable(as: SettingRepository)
class SettingRepositoryImpl implements SettingRepository {
  final SettingRemoteDataSource _settingRemoteDataSource;
  final SettingLocalDataSource _settingLocalDataSource;
  SettingRepositoryImpl(
    this._settingRemoteDataSource,
    this._settingLocalDataSource,
  );

  @override
  Future<Setting> getSetting() async {
    final response = await _settingRemoteDataSource.getSetting();

    var setting = Setting.fromJson(response.data);

    await saveSetting(setting);

    return setting;
  }

  @override
  Future<Setting> getSettingFromCache() => _settingLocalDataSource.getSetting();

  @override
  Future<void> saveSetting(Setting setting) =>
      _settingLocalDataSource.saveSetting(setting);

  @override
  Future<void> updateSettings(SettingUpdateDto dto) =>
      _settingRemoteDataSource.updateSettings(dto);

  @override
  Future<void> uploadFile({required String name, required XFile image}) =>
      _settingRemoteDataSource.uploadFile(name, image);
}
