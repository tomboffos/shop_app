import 'package:image_picker/image_picker.dart';
import 'package:shop_app_mobile/features/settings/domain/dto/setting_update_dto.dart';
import 'package:shop_app_mobile/features/settings/domain/models/setting.dart';

abstract class SettingRepository {
  Future<Setting> getSetting();

  Future<void> saveSetting(Setting setting);

  Future<Setting> getSettingFromCache();

  Future<void> updateSettings(SettingUpdateDto dto);

  Future<void> uploadFile({required String name, required XFile image});
}
