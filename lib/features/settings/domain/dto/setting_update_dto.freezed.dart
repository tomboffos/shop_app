// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'setting_update_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

SettingUpdateDto _$SettingUpdateDtoFromJson(Map<String, dynamic> json) {
  return _SettingUpdateDto.fromJson(json);
}

/// @nodoc
mixin _$SettingUpdateDto {
  String get name => throw _privateConstructorUsedError;
  String get value => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $SettingUpdateDtoCopyWith<SettingUpdateDto> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $SettingUpdateDtoCopyWith<$Res> {
  factory $SettingUpdateDtoCopyWith(
          SettingUpdateDto value, $Res Function(SettingUpdateDto) then) =
      _$SettingUpdateDtoCopyWithImpl<$Res, SettingUpdateDto>;
  @useResult
  $Res call({String name, String value});
}

/// @nodoc
class _$SettingUpdateDtoCopyWithImpl<$Res, $Val extends SettingUpdateDto>
    implements $SettingUpdateDtoCopyWith<$Res> {
  _$SettingUpdateDtoCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? name = null,
    Object? value = null,
  }) {
    return _then(_value.copyWith(
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      value: null == value
          ? _value.value
          : value // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$SettingUpdateDtoImplCopyWith<$Res>
    implements $SettingUpdateDtoCopyWith<$Res> {
  factory _$$SettingUpdateDtoImplCopyWith(_$SettingUpdateDtoImpl value,
          $Res Function(_$SettingUpdateDtoImpl) then) =
      __$$SettingUpdateDtoImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String name, String value});
}

/// @nodoc
class __$$SettingUpdateDtoImplCopyWithImpl<$Res>
    extends _$SettingUpdateDtoCopyWithImpl<$Res, _$SettingUpdateDtoImpl>
    implements _$$SettingUpdateDtoImplCopyWith<$Res> {
  __$$SettingUpdateDtoImplCopyWithImpl(_$SettingUpdateDtoImpl _value,
      $Res Function(_$SettingUpdateDtoImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? name = null,
    Object? value = null,
  }) {
    return _then(_$SettingUpdateDtoImpl(
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      value: null == value
          ? _value.value
          : value // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$SettingUpdateDtoImpl implements _SettingUpdateDto {
  _$SettingUpdateDtoImpl({required this.name, required this.value});

  factory _$SettingUpdateDtoImpl.fromJson(Map<String, dynamic> json) =>
      _$$SettingUpdateDtoImplFromJson(json);

  @override
  final String name;
  @override
  final String value;

  @override
  String toString() {
    return 'SettingUpdateDto(name: $name, value: $value)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$SettingUpdateDtoImpl &&
            (identical(other.name, name) || other.name == name) &&
            (identical(other.value, value) || other.value == value));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, name, value);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$SettingUpdateDtoImplCopyWith<_$SettingUpdateDtoImpl> get copyWith =>
      __$$SettingUpdateDtoImplCopyWithImpl<_$SettingUpdateDtoImpl>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$SettingUpdateDtoImplToJson(
      this,
    );
  }
}

abstract class _SettingUpdateDto implements SettingUpdateDto {
  factory _SettingUpdateDto(
      {required final String name,
      required final String value}) = _$SettingUpdateDtoImpl;

  factory _SettingUpdateDto.fromJson(Map<String, dynamic> json) =
      _$SettingUpdateDtoImpl.fromJson;

  @override
  String get name;
  @override
  String get value;
  @override
  @JsonKey(ignore: true)
  _$$SettingUpdateDtoImplCopyWith<_$SettingUpdateDtoImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
