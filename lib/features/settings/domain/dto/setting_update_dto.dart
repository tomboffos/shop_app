import 'package:freezed_annotation/freezed_annotation.dart';

part 'setting_update_dto.g.dart';
part 'setting_update_dto.freezed.dart';

@freezed
class SettingUpdateDto with _$SettingUpdateDto {
  factory SettingUpdateDto({
    required String name,
    required String value,
  }) = _SettingUpdateDto;

  factory SettingUpdateDto.fromJson(Map<String, dynamic> json) =>
      _$SettingUpdateDtoFromJson(json);
}
