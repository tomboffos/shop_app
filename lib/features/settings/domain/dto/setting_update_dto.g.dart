// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'setting_update_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$SettingUpdateDtoImpl _$$SettingUpdateDtoImplFromJson(
        Map<String, dynamic> json) =>
    _$SettingUpdateDtoImpl(
      name: json['name'] as String,
      value: json['value'] as String,
    );

Map<String, dynamic> _$$SettingUpdateDtoImplToJson(
        _$SettingUpdateDtoImpl instance) =>
    <String, dynamic>{
      'name': instance.name,
      'value': instance.value,
    };
