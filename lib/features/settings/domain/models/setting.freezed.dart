// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'setting.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

Setting _$SettingFromJson(Map<String, dynamic> json) {
  return _Setting.fromJson(json);
}

/// @nodoc
mixin _$Setting {
  @HiveField(1)
  @HexColorConverter()
  @JsonKey(name: 'primary_color_light')
  @HiveField(2)
  Color get primaryColorLight => throw _privateConstructorUsedError;
  @HiveField(3)
  @HexColorConverter()
  @JsonKey(name: 'primary_color_dark')
  Color get primaryColorDark => throw _privateConstructorUsedError;
  @HiveField(4)
  @HexColorConverter()
  @JsonKey(name: 'secondary_color_light')
  Color get secondaryColorLight => throw _privateConstructorUsedError;
  @HiveField(5)
  @HexColorConverter()
  @JsonKey(name: 'secondary_color_dark')
  @HiveField(6)
  @HiveField(7)
  Color get secondaryColorDark => throw _privateConstructorUsedError;
  @HiveField(8)
  String get logo => throw _privateConstructorUsedError;
  @HiveField(9)
  String get title => throw _privateConstructorUsedError;
  @HiveField(10)
  @HexColorConverter()
  @JsonKey(name: 'filled_button_color')
  @HiveField(11)
  Color get filledButtonColor => throw _privateConstructorUsedError;
  @HiveField(12)
  @HexColorConverter()
  @JsonKey(name: 'icon_color')
  Color get iconColor => throw _privateConstructorUsedError;
  @HiveField(13)
  @HexColorConverter()
  @JsonKey(name: 'border_color_light')
  @HiveField(14)
  Color get borderColorLight => throw _privateConstructorUsedError;
  @HiveField(15)
  @JsonKey(name: 'delivery_price')
  String? get deliveryPrice => throw _privateConstructorUsedError;
  @HiveField(16)
  String get drawer => throw _privateConstructorUsedError;
  @HiveField(17)
  @HexColorConverter()
  @JsonKey(name: 'icon_color_dark')
  Color get iconColorDark => throw _privateConstructorUsedError;
  @HiveField(18)
  @HexColorConverter()
  @JsonKey(name: 'text_color')
  Color get textColor => throw _privateConstructorUsedError;
  @HiveField(19)
  @HexColorConverter()
  @JsonKey(name: 'text_color_dark')
  Color get textColorDark => throw _privateConstructorUsedError;
  @HiveField(20)
  @JsonKey(name: 'text_theme')
  GoogleFont get textTheme => throw _privateConstructorUsedError;
  @HiveField(21)
  @HexColorConverter()
  @JsonKey(name: 'filled_button_color_dark')
  Color get fileedButtonColorDark => throw _privateConstructorUsedError;
  @HexColorConverter()
  @JsonKey(name: 'border_color_dark')
  @HiveField(22)
  Color get borderColorDark => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $SettingCopyWith<Setting> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $SettingCopyWith<$Res> {
  factory $SettingCopyWith(Setting value, $Res Function(Setting) then) =
      _$SettingCopyWithImpl<$Res, Setting>;
  @useResult
  $Res call(
      {@HiveField(1)
      @HexColorConverter()
      @JsonKey(name: 'primary_color_light')
      @HiveField(2)
      Color primaryColorLight,
      @HiveField(3)
      @HexColorConverter()
      @JsonKey(name: 'primary_color_dark')
      Color primaryColorDark,
      @HiveField(4)
      @HexColorConverter()
      @JsonKey(name: 'secondary_color_light')
      Color secondaryColorLight,
      @HiveField(5)
      @HexColorConverter()
      @JsonKey(name: 'secondary_color_dark')
      @HiveField(6)
      @HiveField(7)
      Color secondaryColorDark,
      @HiveField(8) String logo,
      @HiveField(9) String title,
      @HiveField(10)
      @HexColorConverter()
      @JsonKey(name: 'filled_button_color')
      @HiveField(11)
      Color filledButtonColor,
      @HiveField(12)
      @HexColorConverter()
      @JsonKey(name: 'icon_color')
      Color iconColor,
      @HiveField(13)
      @HexColorConverter()
      @JsonKey(name: 'border_color_light')
      @HiveField(14)
      Color borderColorLight,
      @HiveField(15) @JsonKey(name: 'delivery_price') String? deliveryPrice,
      @HiveField(16) String drawer,
      @HiveField(17)
      @HexColorConverter()
      @JsonKey(name: 'icon_color_dark')
      Color iconColorDark,
      @HiveField(18)
      @HexColorConverter()
      @JsonKey(name: 'text_color')
      Color textColor,
      @HiveField(19)
      @HexColorConverter()
      @JsonKey(name: 'text_color_dark')
      Color textColorDark,
      @HiveField(20) @JsonKey(name: 'text_theme') GoogleFont textTheme,
      @HiveField(21)
      @HexColorConverter()
      @JsonKey(name: 'filled_button_color_dark')
      Color fileedButtonColorDark,
      @HexColorConverter()
      @JsonKey(name: 'border_color_dark')
      @HiveField(22)
      Color borderColorDark});
}

/// @nodoc
class _$SettingCopyWithImpl<$Res, $Val extends Setting>
    implements $SettingCopyWith<$Res> {
  _$SettingCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? primaryColorLight = null,
    Object? primaryColorDark = null,
    Object? secondaryColorLight = null,
    Object? secondaryColorDark = null,
    Object? logo = null,
    Object? title = null,
    Object? filledButtonColor = null,
    Object? iconColor = null,
    Object? borderColorLight = null,
    Object? deliveryPrice = freezed,
    Object? drawer = null,
    Object? iconColorDark = null,
    Object? textColor = null,
    Object? textColorDark = null,
    Object? textTheme = null,
    Object? fileedButtonColorDark = null,
    Object? borderColorDark = null,
  }) {
    return _then(_value.copyWith(
      primaryColorLight: null == primaryColorLight
          ? _value.primaryColorLight
          : primaryColorLight // ignore: cast_nullable_to_non_nullable
              as Color,
      primaryColorDark: null == primaryColorDark
          ? _value.primaryColorDark
          : primaryColorDark // ignore: cast_nullable_to_non_nullable
              as Color,
      secondaryColorLight: null == secondaryColorLight
          ? _value.secondaryColorLight
          : secondaryColorLight // ignore: cast_nullable_to_non_nullable
              as Color,
      secondaryColorDark: null == secondaryColorDark
          ? _value.secondaryColorDark
          : secondaryColorDark // ignore: cast_nullable_to_non_nullable
              as Color,
      logo: null == logo
          ? _value.logo
          : logo // ignore: cast_nullable_to_non_nullable
              as String,
      title: null == title
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String,
      filledButtonColor: null == filledButtonColor
          ? _value.filledButtonColor
          : filledButtonColor // ignore: cast_nullable_to_non_nullable
              as Color,
      iconColor: null == iconColor
          ? _value.iconColor
          : iconColor // ignore: cast_nullable_to_non_nullable
              as Color,
      borderColorLight: null == borderColorLight
          ? _value.borderColorLight
          : borderColorLight // ignore: cast_nullable_to_non_nullable
              as Color,
      deliveryPrice: freezed == deliveryPrice
          ? _value.deliveryPrice
          : deliveryPrice // ignore: cast_nullable_to_non_nullable
              as String?,
      drawer: null == drawer
          ? _value.drawer
          : drawer // ignore: cast_nullable_to_non_nullable
              as String,
      iconColorDark: null == iconColorDark
          ? _value.iconColorDark
          : iconColorDark // ignore: cast_nullable_to_non_nullable
              as Color,
      textColor: null == textColor
          ? _value.textColor
          : textColor // ignore: cast_nullable_to_non_nullable
              as Color,
      textColorDark: null == textColorDark
          ? _value.textColorDark
          : textColorDark // ignore: cast_nullable_to_non_nullable
              as Color,
      textTheme: null == textTheme
          ? _value.textTheme
          : textTheme // ignore: cast_nullable_to_non_nullable
              as GoogleFont,
      fileedButtonColorDark: null == fileedButtonColorDark
          ? _value.fileedButtonColorDark
          : fileedButtonColorDark // ignore: cast_nullable_to_non_nullable
              as Color,
      borderColorDark: null == borderColorDark
          ? _value.borderColorDark
          : borderColorDark // ignore: cast_nullable_to_non_nullable
              as Color,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$SettingImplCopyWith<$Res> implements $SettingCopyWith<$Res> {
  factory _$$SettingImplCopyWith(
          _$SettingImpl value, $Res Function(_$SettingImpl) then) =
      __$$SettingImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {@HiveField(1)
      @HexColorConverter()
      @JsonKey(name: 'primary_color_light')
      @HiveField(2)
      Color primaryColorLight,
      @HiveField(3)
      @HexColorConverter()
      @JsonKey(name: 'primary_color_dark')
      Color primaryColorDark,
      @HiveField(4)
      @HexColorConverter()
      @JsonKey(name: 'secondary_color_light')
      Color secondaryColorLight,
      @HiveField(5)
      @HexColorConverter()
      @JsonKey(name: 'secondary_color_dark')
      @HiveField(6)
      @HiveField(7)
      Color secondaryColorDark,
      @HiveField(8) String logo,
      @HiveField(9) String title,
      @HiveField(10)
      @HexColorConverter()
      @JsonKey(name: 'filled_button_color')
      @HiveField(11)
      Color filledButtonColor,
      @HiveField(12)
      @HexColorConverter()
      @JsonKey(name: 'icon_color')
      Color iconColor,
      @HiveField(13)
      @HexColorConverter()
      @JsonKey(name: 'border_color_light')
      @HiveField(14)
      Color borderColorLight,
      @HiveField(15) @JsonKey(name: 'delivery_price') String? deliveryPrice,
      @HiveField(16) String drawer,
      @HiveField(17)
      @HexColorConverter()
      @JsonKey(name: 'icon_color_dark')
      Color iconColorDark,
      @HiveField(18)
      @HexColorConverter()
      @JsonKey(name: 'text_color')
      Color textColor,
      @HiveField(19)
      @HexColorConverter()
      @JsonKey(name: 'text_color_dark')
      Color textColorDark,
      @HiveField(20) @JsonKey(name: 'text_theme') GoogleFont textTheme,
      @HiveField(21)
      @HexColorConverter()
      @JsonKey(name: 'filled_button_color_dark')
      Color fileedButtonColorDark,
      @HexColorConverter()
      @JsonKey(name: 'border_color_dark')
      @HiveField(22)
      Color borderColorDark});
}

/// @nodoc
class __$$SettingImplCopyWithImpl<$Res>
    extends _$SettingCopyWithImpl<$Res, _$SettingImpl>
    implements _$$SettingImplCopyWith<$Res> {
  __$$SettingImplCopyWithImpl(
      _$SettingImpl _value, $Res Function(_$SettingImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? primaryColorLight = null,
    Object? primaryColorDark = null,
    Object? secondaryColorLight = null,
    Object? secondaryColorDark = null,
    Object? logo = null,
    Object? title = null,
    Object? filledButtonColor = null,
    Object? iconColor = null,
    Object? borderColorLight = null,
    Object? deliveryPrice = freezed,
    Object? drawer = null,
    Object? iconColorDark = null,
    Object? textColor = null,
    Object? textColorDark = null,
    Object? textTheme = null,
    Object? fileedButtonColorDark = null,
    Object? borderColorDark = null,
  }) {
    return _then(_$SettingImpl(
      primaryColorLight: null == primaryColorLight
          ? _value.primaryColorLight
          : primaryColorLight // ignore: cast_nullable_to_non_nullable
              as Color,
      primaryColorDark: null == primaryColorDark
          ? _value.primaryColorDark
          : primaryColorDark // ignore: cast_nullable_to_non_nullable
              as Color,
      secondaryColorLight: null == secondaryColorLight
          ? _value.secondaryColorLight
          : secondaryColorLight // ignore: cast_nullable_to_non_nullable
              as Color,
      secondaryColorDark: null == secondaryColorDark
          ? _value.secondaryColorDark
          : secondaryColorDark // ignore: cast_nullable_to_non_nullable
              as Color,
      logo: null == logo
          ? _value.logo
          : logo // ignore: cast_nullable_to_non_nullable
              as String,
      title: null == title
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String,
      filledButtonColor: null == filledButtonColor
          ? _value.filledButtonColor
          : filledButtonColor // ignore: cast_nullable_to_non_nullable
              as Color,
      iconColor: null == iconColor
          ? _value.iconColor
          : iconColor // ignore: cast_nullable_to_non_nullable
              as Color,
      borderColorLight: null == borderColorLight
          ? _value.borderColorLight
          : borderColorLight // ignore: cast_nullable_to_non_nullable
              as Color,
      deliveryPrice: freezed == deliveryPrice
          ? _value.deliveryPrice
          : deliveryPrice // ignore: cast_nullable_to_non_nullable
              as String?,
      drawer: null == drawer
          ? _value.drawer
          : drawer // ignore: cast_nullable_to_non_nullable
              as String,
      iconColorDark: null == iconColorDark
          ? _value.iconColorDark
          : iconColorDark // ignore: cast_nullable_to_non_nullable
              as Color,
      textColor: null == textColor
          ? _value.textColor
          : textColor // ignore: cast_nullable_to_non_nullable
              as Color,
      textColorDark: null == textColorDark
          ? _value.textColorDark
          : textColorDark // ignore: cast_nullable_to_non_nullable
              as Color,
      textTheme: null == textTheme
          ? _value.textTheme
          : textTheme // ignore: cast_nullable_to_non_nullable
              as GoogleFont,
      fileedButtonColorDark: null == fileedButtonColorDark
          ? _value.fileedButtonColorDark
          : fileedButtonColorDark // ignore: cast_nullable_to_non_nullable
              as Color,
      borderColorDark: null == borderColorDark
          ? _value.borderColorDark
          : borderColorDark // ignore: cast_nullable_to_non_nullable
              as Color,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$SettingImpl implements _Setting {
  const _$SettingImpl(
      {@HiveField(1)
      @HexColorConverter()
      @JsonKey(name: 'primary_color_light')
      @HiveField(2)
      required this.primaryColorLight,
      @HiveField(3)
      @HexColorConverter()
      @JsonKey(name: 'primary_color_dark')
      required this.primaryColorDark,
      @HiveField(4)
      @HexColorConverter()
      @JsonKey(name: 'secondary_color_light')
      required this.secondaryColorLight,
      @HiveField(5)
      @HexColorConverter()
      @JsonKey(name: 'secondary_color_dark')
      @HiveField(6)
      @HiveField(7)
      required this.secondaryColorDark,
      @HiveField(8) required this.logo,
      @HiveField(9) required this.title,
      @HiveField(10)
      @HexColorConverter()
      @JsonKey(name: 'filled_button_color')
      @HiveField(11)
      required this.filledButtonColor,
      @HiveField(12)
      @HexColorConverter()
      @JsonKey(name: 'icon_color')
      required this.iconColor,
      @HiveField(13)
      @HexColorConverter()
      @JsonKey(name: 'border_color_light')
      @HiveField(14)
      required this.borderColorLight,
      @HiveField(15) @JsonKey(name: 'delivery_price') this.deliveryPrice,
      @HiveField(16) required this.drawer,
      @HiveField(17)
      @HexColorConverter()
      @JsonKey(name: 'icon_color_dark')
      required this.iconColorDark,
      @HiveField(18)
      @HexColorConverter()
      @JsonKey(name: 'text_color')
      required this.textColor,
      @HiveField(19)
      @HexColorConverter()
      @JsonKey(name: 'text_color_dark')
      required this.textColorDark,
      @HiveField(20) @JsonKey(name: 'text_theme') required this.textTheme,
      @HiveField(21)
      @HexColorConverter()
      @JsonKey(name: 'filled_button_color_dark')
      required this.fileedButtonColorDark,
      @HexColorConverter()
      @JsonKey(name: 'border_color_dark')
      @HiveField(22)
      required this.borderColorDark});

  factory _$SettingImpl.fromJson(Map<String, dynamic> json) =>
      _$$SettingImplFromJson(json);

  @override
  @HiveField(1)
  @HexColorConverter()
  @JsonKey(name: 'primary_color_light')
  @HiveField(2)
  final Color primaryColorLight;
  @override
  @HiveField(3)
  @HexColorConverter()
  @JsonKey(name: 'primary_color_dark')
  final Color primaryColorDark;
  @override
  @HiveField(4)
  @HexColorConverter()
  @JsonKey(name: 'secondary_color_light')
  final Color secondaryColorLight;
  @override
  @HiveField(5)
  @HexColorConverter()
  @JsonKey(name: 'secondary_color_dark')
  @HiveField(6)
  @HiveField(7)
  final Color secondaryColorDark;
  @override
  @HiveField(8)
  final String logo;
  @override
  @HiveField(9)
  final String title;
  @override
  @HiveField(10)
  @HexColorConverter()
  @JsonKey(name: 'filled_button_color')
  @HiveField(11)
  final Color filledButtonColor;
  @override
  @HiveField(12)
  @HexColorConverter()
  @JsonKey(name: 'icon_color')
  final Color iconColor;
  @override
  @HiveField(13)
  @HexColorConverter()
  @JsonKey(name: 'border_color_light')
  @HiveField(14)
  final Color borderColorLight;
  @override
  @HiveField(15)
  @JsonKey(name: 'delivery_price')
  final String? deliveryPrice;
  @override
  @HiveField(16)
  final String drawer;
  @override
  @HiveField(17)
  @HexColorConverter()
  @JsonKey(name: 'icon_color_dark')
  final Color iconColorDark;
  @override
  @HiveField(18)
  @HexColorConverter()
  @JsonKey(name: 'text_color')
  final Color textColor;
  @override
  @HiveField(19)
  @HexColorConverter()
  @JsonKey(name: 'text_color_dark')
  final Color textColorDark;
  @override
  @HiveField(20)
  @JsonKey(name: 'text_theme')
  final GoogleFont textTheme;
  @override
  @HiveField(21)
  @HexColorConverter()
  @JsonKey(name: 'filled_button_color_dark')
  final Color fileedButtonColorDark;
  @override
  @HexColorConverter()
  @JsonKey(name: 'border_color_dark')
  @HiveField(22)
  final Color borderColorDark;

  @override
  String toString() {
    return 'Setting(primaryColorLight: $primaryColorLight, primaryColorDark: $primaryColorDark, secondaryColorLight: $secondaryColorLight, secondaryColorDark: $secondaryColorDark, logo: $logo, title: $title, filledButtonColor: $filledButtonColor, iconColor: $iconColor, borderColorLight: $borderColorLight, deliveryPrice: $deliveryPrice, drawer: $drawer, iconColorDark: $iconColorDark, textColor: $textColor, textColorDark: $textColorDark, textTheme: $textTheme, fileedButtonColorDark: $fileedButtonColorDark, borderColorDark: $borderColorDark)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$SettingImpl &&
            (identical(other.primaryColorLight, primaryColorLight) ||
                other.primaryColorLight == primaryColorLight) &&
            (identical(other.primaryColorDark, primaryColorDark) ||
                other.primaryColorDark == primaryColorDark) &&
            (identical(other.secondaryColorLight, secondaryColorLight) ||
                other.secondaryColorLight == secondaryColorLight) &&
            (identical(other.secondaryColorDark, secondaryColorDark) ||
                other.secondaryColorDark == secondaryColorDark) &&
            (identical(other.logo, logo) || other.logo == logo) &&
            (identical(other.title, title) || other.title == title) &&
            (identical(other.filledButtonColor, filledButtonColor) ||
                other.filledButtonColor == filledButtonColor) &&
            (identical(other.iconColor, iconColor) ||
                other.iconColor == iconColor) &&
            (identical(other.borderColorLight, borderColorLight) ||
                other.borderColorLight == borderColorLight) &&
            (identical(other.deliveryPrice, deliveryPrice) ||
                other.deliveryPrice == deliveryPrice) &&
            (identical(other.drawer, drawer) || other.drawer == drawer) &&
            (identical(other.iconColorDark, iconColorDark) ||
                other.iconColorDark == iconColorDark) &&
            (identical(other.textColor, textColor) ||
                other.textColor == textColor) &&
            (identical(other.textColorDark, textColorDark) ||
                other.textColorDark == textColorDark) &&
            (identical(other.textTheme, textTheme) ||
                other.textTheme == textTheme) &&
            (identical(other.fileedButtonColorDark, fileedButtonColorDark) ||
                other.fileedButtonColorDark == fileedButtonColorDark) &&
            (identical(other.borderColorDark, borderColorDark) ||
                other.borderColorDark == borderColorDark));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      primaryColorLight,
      primaryColorDark,
      secondaryColorLight,
      secondaryColorDark,
      logo,
      title,
      filledButtonColor,
      iconColor,
      borderColorLight,
      deliveryPrice,
      drawer,
      iconColorDark,
      textColor,
      textColorDark,
      textTheme,
      fileedButtonColorDark,
      borderColorDark);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$SettingImplCopyWith<_$SettingImpl> get copyWith =>
      __$$SettingImplCopyWithImpl<_$SettingImpl>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$SettingImplToJson(
      this,
    );
  }
}

abstract class _Setting implements Setting {
  const factory _Setting(
      {@HiveField(1)
      @HexColorConverter()
      @JsonKey(name: 'primary_color_light')
      @HiveField(2)
      required final Color primaryColorLight,
      @HiveField(3)
      @HexColorConverter()
      @JsonKey(name: 'primary_color_dark')
      required final Color primaryColorDark,
      @HiveField(4)
      @HexColorConverter()
      @JsonKey(name: 'secondary_color_light')
      required final Color secondaryColorLight,
      @HiveField(5)
      @HexColorConverter()
      @JsonKey(name: 'secondary_color_dark')
      @HiveField(6)
      @HiveField(7)
      required final Color secondaryColorDark,
      @HiveField(8) required final String logo,
      @HiveField(9) required final String title,
      @HiveField(10)
      @HexColorConverter()
      @JsonKey(name: 'filled_button_color')
      @HiveField(11)
      required final Color filledButtonColor,
      @HiveField(12)
      @HexColorConverter()
      @JsonKey(name: 'icon_color')
      required final Color iconColor,
      @HiveField(13)
      @HexColorConverter()
      @JsonKey(name: 'border_color_light')
      @HiveField(14)
      required final Color borderColorLight,
      @HiveField(15)
      @JsonKey(name: 'delivery_price')
      final String? deliveryPrice,
      @HiveField(16) required final String drawer,
      @HiveField(17)
      @HexColorConverter()
      @JsonKey(name: 'icon_color_dark')
      required final Color iconColorDark,
      @HiveField(18)
      @HexColorConverter()
      @JsonKey(name: 'text_color')
      required final Color textColor,
      @HiveField(19)
      @HexColorConverter()
      @JsonKey(name: 'text_color_dark')
      required final Color textColorDark,
      @HiveField(20)
      @JsonKey(name: 'text_theme')
      required final GoogleFont textTheme,
      @HiveField(21)
      @HexColorConverter()
      @JsonKey(name: 'filled_button_color_dark')
      required final Color fileedButtonColorDark,
      @HexColorConverter()
      @JsonKey(name: 'border_color_dark')
      @HiveField(22)
      required final Color borderColorDark}) = _$SettingImpl;

  factory _Setting.fromJson(Map<String, dynamic> json) = _$SettingImpl.fromJson;

  @override
  @HiveField(1)
  @HexColorConverter()
  @JsonKey(name: 'primary_color_light')
  @HiveField(2)
  Color get primaryColorLight;
  @override
  @HiveField(3)
  @HexColorConverter()
  @JsonKey(name: 'primary_color_dark')
  Color get primaryColorDark;
  @override
  @HiveField(4)
  @HexColorConverter()
  @JsonKey(name: 'secondary_color_light')
  Color get secondaryColorLight;
  @override
  @HiveField(5)
  @HexColorConverter()
  @JsonKey(name: 'secondary_color_dark')
  @HiveField(6)
  @HiveField(7)
  Color get secondaryColorDark;
  @override
  @HiveField(8)
  String get logo;
  @override
  @HiveField(9)
  String get title;
  @override
  @HiveField(10)
  @HexColorConverter()
  @JsonKey(name: 'filled_button_color')
  @HiveField(11)
  Color get filledButtonColor;
  @override
  @HiveField(12)
  @HexColorConverter()
  @JsonKey(name: 'icon_color')
  Color get iconColor;
  @override
  @HiveField(13)
  @HexColorConverter()
  @JsonKey(name: 'border_color_light')
  @HiveField(14)
  Color get borderColorLight;
  @override
  @HiveField(15)
  @JsonKey(name: 'delivery_price')
  String? get deliveryPrice;
  @override
  @HiveField(16)
  String get drawer;
  @override
  @HiveField(17)
  @HexColorConverter()
  @JsonKey(name: 'icon_color_dark')
  Color get iconColorDark;
  @override
  @HiveField(18)
  @HexColorConverter()
  @JsonKey(name: 'text_color')
  Color get textColor;
  @override
  @HiveField(19)
  @HexColorConverter()
  @JsonKey(name: 'text_color_dark')
  Color get textColorDark;
  @override
  @HiveField(20)
  @JsonKey(name: 'text_theme')
  GoogleFont get textTheme;
  @override
  @HiveField(21)
  @HexColorConverter()
  @JsonKey(name: 'filled_button_color_dark')
  Color get fileedButtonColorDark;
  @override
  @HexColorConverter()
  @JsonKey(name: 'border_color_dark')
  @HiveField(22)
  Color get borderColorDark;
  @override
  @JsonKey(ignore: true)
  _$$SettingImplCopyWith<_$SettingImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
