// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'setting.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class SettingAdapter extends TypeAdapter<Setting> {
  @override
  final int typeId = 6;

  @override
  Setting read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return Setting(
      primaryColorLight: fields[1] as Color,
      primaryColorDark: fields[3] as Color,
      secondaryColorLight: fields[4] as Color,
      secondaryColorDark: fields[5] as Color,
      logo: fields[8] as String,
      title: fields[9] as String,
      filledButtonColor: fields[10] as Color,
      iconColor: fields[12] as Color,
      borderColorLight: fields[13] as Color,
      deliveryPrice: fields[15] as String?,
      drawer: fields[16] as String,
      iconColorDark: fields[17] as Color,
      textColor: fields[18] as Color,
      textColorDark: fields[19] as Color,
      textTheme: fields[20] as GoogleFont,
      fileedButtonColorDark: fields[21] as Color,
      borderColorDark: fields[22] as Color,
    );
  }

  @override
  void write(BinaryWriter writer, Setting obj) {
    writer
      ..writeByte(17)
      ..writeByte(1)
      ..write(obj.primaryColorLight)
      ..writeByte(3)
      ..write(obj.primaryColorDark)
      ..writeByte(4)
      ..write(obj.secondaryColorLight)
      ..writeByte(5)
      ..write(obj.secondaryColorDark)
      ..writeByte(8)
      ..write(obj.logo)
      ..writeByte(9)
      ..write(obj.title)
      ..writeByte(10)
      ..write(obj.filledButtonColor)
      ..writeByte(12)
      ..write(obj.iconColor)
      ..writeByte(13)
      ..write(obj.borderColorLight)
      ..writeByte(15)
      ..write(obj.deliveryPrice)
      ..writeByte(16)
      ..write(obj.drawer)
      ..writeByte(17)
      ..write(obj.iconColorDark)
      ..writeByte(18)
      ..write(obj.textColor)
      ..writeByte(19)
      ..write(obj.textColorDark)
      ..writeByte(20)
      ..write(obj.textTheme)
      ..writeByte(21)
      ..write(obj.fileedButtonColorDark)
      ..writeByte(22)
      ..write(obj.borderColorDark);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is SettingAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$SettingImpl _$$SettingImplFromJson(Map<String, dynamic> json) =>
    _$SettingImpl(
      primaryColorLight: const HexColorConverter()
          .fromJson(json['primary_color_light'] as String?),
      primaryColorDark: const HexColorConverter()
          .fromJson(json['primary_color_dark'] as String?),
      secondaryColorLight: const HexColorConverter()
          .fromJson(json['secondary_color_light'] as String?),
      secondaryColorDark: const HexColorConverter()
          .fromJson(json['secondary_color_dark'] as String?),
      logo: json['logo'] as String,
      title: json['title'] as String,
      filledButtonColor: const HexColorConverter()
          .fromJson(json['filled_button_color'] as String?),
      iconColor:
          const HexColorConverter().fromJson(json['icon_color'] as String?),
      borderColorLight: const HexColorConverter()
          .fromJson(json['border_color_light'] as String?),
      deliveryPrice: json['delivery_price'] as String?,
      drawer: json['drawer'] as String,
      iconColorDark: const HexColorConverter()
          .fromJson(json['icon_color_dark'] as String?),
      textColor:
          const HexColorConverter().fromJson(json['text_color'] as String?),
      textColorDark: const HexColorConverter()
          .fromJson(json['text_color_dark'] as String?),
      textTheme: $enumDecode(_$GoogleFontEnumMap, json['text_theme']),
      fileedButtonColorDark: const HexColorConverter()
          .fromJson(json['filled_button_color_dark'] as String?),
      borderColorDark: const HexColorConverter()
          .fromJson(json['border_color_dark'] as String?),
    );

Map<String, dynamic> _$$SettingImplToJson(_$SettingImpl instance) =>
    <String, dynamic>{
      'primary_color_light':
          const HexColorConverter().toJson(instance.primaryColorLight),
      'primary_color_dark':
          const HexColorConverter().toJson(instance.primaryColorDark),
      'secondary_color_light':
          const HexColorConverter().toJson(instance.secondaryColorLight),
      'secondary_color_dark':
          const HexColorConverter().toJson(instance.secondaryColorDark),
      'logo': instance.logo,
      'title': instance.title,
      'filled_button_color':
          const HexColorConverter().toJson(instance.filledButtonColor),
      'icon_color': const HexColorConverter().toJson(instance.iconColor),
      'border_color_light':
          const HexColorConverter().toJson(instance.borderColorLight),
      'delivery_price': instance.deliveryPrice,
      'drawer': instance.drawer,
      'icon_color_dark':
          const HexColorConverter().toJson(instance.iconColorDark),
      'text_color': const HexColorConverter().toJson(instance.textColor),
      'text_color_dark':
          const HexColorConverter().toJson(instance.textColorDark),
      'text_theme': _$GoogleFontEnumMap[instance.textTheme]!,
      'filled_button_color_dark':
          const HexColorConverter().toJson(instance.fileedButtonColorDark),
      'border_color_dark':
          const HexColorConverter().toJson(instance.borderColorDark),
    };

const _$GoogleFontEnumMap = {
  GoogleFont.roboto: 'roboto',
  GoogleFont.lato: 'lato',
  GoogleFont.openSans: 'openSans',
  GoogleFont.montserrat: 'montserrat',
  GoogleFont.sourceSansPro: 'sourceSansPro',
};
