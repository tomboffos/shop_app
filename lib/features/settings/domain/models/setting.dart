import 'package:hive/hive.dart';
import 'package:shop_app_mobile/core/services/font_service.dart';
import 'package:shop_app_mobile/core/utils/converters/color_converter.dart';
import 'package:flutter/services.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'setting.g.dart';
part 'setting.freezed.dart';

@freezed
@HiveType(typeId: 6)
class Setting with _$Setting {
  const factory Setting({
    @HiveField(1)
    @HexColorConverter()
    @JsonKey(name: 'primary_color_light')
    @HiveField(2)
    required Color primaryColorLight,
    @HiveField(3)
    @HexColorConverter()
    @JsonKey(name: 'primary_color_dark')
    required Color primaryColorDark,
    @HiveField(4)
    @HexColorConverter()
    @JsonKey(name: 'secondary_color_light')
    required Color secondaryColorLight,
    @HiveField(5)
    @HexColorConverter()
    @JsonKey(name: 'secondary_color_dark')
    @HiveField(6)
    @HiveField(7)
    required Color secondaryColorDark,
    @HiveField(8) required String logo,
    @HiveField(9) required String title,
    @HiveField(10)
    @HexColorConverter()
    @JsonKey(name: 'filled_button_color')
    @HiveField(11)
    required Color filledButtonColor,
    @HiveField(12)
    @HexColorConverter()
    @JsonKey(name: 'icon_color')
    required Color iconColor,
    @HiveField(13)
    @HexColorConverter()
    @JsonKey(name: 'border_color_light')
    @HiveField(14)
    required Color borderColorLight,
    @HiveField(15) @JsonKey(name: 'delivery_price') String? deliveryPrice,
    @HiveField(16) required String drawer,
    @HiveField(17)
    @HexColorConverter()
    @JsonKey(name: 'icon_color_dark')
    required Color iconColorDark,
    @HiveField(18)
    @HexColorConverter()
    @JsonKey(name: 'text_color')
    required Color textColor,
    @HiveField(19)
    @HexColorConverter()
    @JsonKey(name: 'text_color_dark')
    required Color textColorDark,
    @HiveField(20) @JsonKey(name: 'text_theme') required GoogleFont textTheme,
    @HiveField(21)
    @HexColorConverter()
    @JsonKey(name: 'filled_button_color_dark')
    required Color fileedButtonColorDark,
    @HexColorConverter()
    @JsonKey(name: 'border_color_dark')
    @HiveField(22)
    required Color borderColorDark,
  }) = _Setting;

  factory Setting.fromJson(Map<String, dynamic> json) =>
      _$SettingFromJson(json);
}
