import 'package:image_picker/image_picker.dart';
import 'package:shop_app_mobile/features/settings/domain/dto/setting_update_dto.dart';
import 'package:shop_app_mobile/features/settings/domain/models/setting.dart';
import 'package:shop_app_mobile/features/settings/domain/repository/setting_repository.dart';
import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';

part 'setting_state.dart';
part 'setting_cubit.freezed.dart';

@singleton
class SettingCubit extends Cubit<SettingState> {
  final SettingRepository _settingRepository;

  SettingCubit(this._settingRepository) : super(const SettingState.initial());

  void initSettings() async {
    try {
      final setting = await _settingRepository.getSetting();

      emit(SettingState.loaded(setting));
    } on Exception {
      final setting = await _settingRepository.getSettingFromCache();

      emit(SettingState.loaded(setting));
    }
  }

  void updateSettings(SettingUpdateDto dto) async {
    await _settingRepository.updateSettings(dto);

    initSettings();
  }

  Future<void> uploadFile(String name, XFile image) async {
    await _settingRepository.uploadFile(name: name, image: image);

    initSettings();
  }
}
