import 'package:auto_route/auto_route.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shop_app_mobile/core/injection/injection.dart';
import 'package:shop_app_mobile/core/services/font_service.dart';
import 'package:shop_app_mobile/core/utils/constants/gaps.dart';
import 'package:shop_app_mobile/core/utils/constants/insets.dart';
import 'package:shop_app_mobile/features/app/widgets/image_picker_dialog.dart';
import 'package:shop_app_mobile/features/settings/domain/dto/setting_update_dto.dart';
import 'package:shop_app_mobile/features/settings/presentation/cubit/setting_cubit.dart';
import 'package:shop_app_mobile/features/settings/presentation/widgets/color_button.dart';

@RoutePage()
class SettingUpdatePage extends StatefulWidget {
  const SettingUpdatePage({super.key});

  @override
  State<SettingUpdatePage> createState() => _SettingUpdatePageState();
}

class _SettingUpdatePageState extends State<SettingUpdatePage> {
  final _settingCubit = getIt.get<SettingCubit>();

  final _endController = TextEditingController();
  final _nameController = TextEditingController();

  @override
  void initState() {
    _settingCubit.state.whenOrNull(
      loaded: (setting) {
        _nameController.text = setting.title;
        _endController.text = setting.deliveryPrice ?? '';
      },
    );

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SettingCubit, SettingState>(
      bloc: _settingCubit,
      builder: (context, state) {
        return Scaffold(
          body: state.maybeWhen(
            orElse: () => const SizedBox.shrink(),
            loaded: (setting) => SingleChildScrollView(
              child: Padding(
                padding: Insets.dimens2x,
                child: Card(
                  child: Padding(
                    padding: Insets.dimens2x,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Логотип',
                          style: Theme.of(context).textTheme.headlineSmall,
                        ),
                        Gaps.gap,
                        if (setting.logo.isNotEmpty)
                          InkWell(
                            onTap: () async {
                              await _pickImage(context);
                            },
                            child: CachedNetworkImage(
                              imageUrl: setting.logo,
                              width: 150,
                            ),
                          )
                        else
                          OutlinedButton(
                            onPressed: () async {
                              await _pickImage(context);
                            },
                            child: const Icon(Icons.add),
                          ),
                        Gaps.gap2,
                        Text(
                          'Название приложения',
                          style: Theme.of(context).textTheme.headlineSmall,
                        ),
                        Gaps.gap,
                        TextField(
                          controller: _nameController,
                          decoration: const InputDecoration(
                              hintText: 'Название приложения'),
                          onChanged: (val) {
                            _settingCubit.updateSettings(
                              SettingUpdateDto(
                                name: 'title',
                                value: val,
                              ),
                            );
                          },
                        ),
                        Gaps.gap2,
                        Text(
                          'Цена доставки',
                          style: Theme.of(context).textTheme.headlineSmall,
                        ),
                        Gaps.gap,
                        TextField(
                          controller: _endController,
                          decoration: const InputDecoration(
                            hintText: 'Цена доставки',
                          ),
                          onChanged: (val) {
                            _settingCubit.updateSettings(
                              SettingUpdateDto(
                                name: 'delivery_price',
                                value: val,
                              ),
                            );
                          },
                        ),
                        Gaps.gap,
                        Text(
                          'Шрифт',
                          style: Theme.of(context).textTheme.headlineSmall,
                        ),
                        Gaps.gap,
                        DropdownButtonFormField(
                          value: setting.textTheme.name,
                          decoration: const InputDecoration(hintText: 'Шрифт'),
                          items: GoogleFont.values
                              .map(
                                (e) => DropdownMenuItem(
                                  value: e.name,
                                  child: Text(e.name),
                                ),
                              )
                              .toList(),
                          onChanged: (val) {
                            _settingCubit.updateSettings(
                              SettingUpdateDto(
                                name: 'text_theme',
                                value: val ?? '',
                              ),
                            );
                          },
                        ),
                        Gaps.gap2,
                        CheckboxListTile.adaptive(
                          title: const Text('Нижнее меню'),
                          value: setting.drawer != 'true',
                          onChanged: (val) {
                            _settingCubit.updateSettings(
                              SettingUpdateDto(
                                name: 'drawer',
                                value: (val != true).toString(),
                              ),
                            );
                          },
                        ),
                        Gaps.gap2,
                        Text(
                          'Светлая тема',
                          style: Theme.of(context).textTheme.headlineSmall,
                        ),
                        Gaps.gap,
                        GridView(
                          shrinkWrap: true,
                          physics: const NeverScrollableScrollPhysics(),
                          gridDelegate:
                              const SliverGridDelegateWithFixedCrossAxisCount(
                            crossAxisCount: 2,
                            childAspectRatio: 1.4,
                          ),
                          children: [
                            ColorButton(
                              text: 'Основной цвет',
                              settingCubit: _settingCubit,
                              givenColor: setting.primaryColorLight,
                              keyColor: 'primary_color_light',
                            ),
                            ColorButton(
                              settingCubit: _settingCubit,
                              givenColor: setting.secondaryColorLight,
                              keyColor: 'secondary_color_light',
                              text: 'Вторичный цвет',
                            ),
                            ColorButton(
                              settingCubit: _settingCubit,
                              givenColor: setting.iconColor,
                              keyColor: 'icon_color',
                              text: 'Цвет иконок',
                            ),
                            ColorButton(
                              settingCubit: _settingCubit,
                              givenColor: setting.borderColorLight,
                              keyColor: 'border_color_light',
                              text: 'Цвет краев',
                            ),
                            ColorButton(
                              settingCubit: _settingCubit,
                              givenColor: setting.textColor,
                              keyColor: 'text_color',
                              text: 'Цвет текста',
                            ),
                            ColorButton(
                              settingCubit: _settingCubit,
                              givenColor: setting.filledButtonColor,
                              keyColor: 'filled_button_color',
                              text: 'Цвет кнопки',
                            ),
                          ],
                        ),
                        Gaps.gap2,
                        Text(
                          'Темная тема',
                          style: Theme.of(context).textTheme.headlineSmall,
                        ),
                        Gaps.gap,
                        GridView(
                          shrinkWrap: true,
                          physics: const NeverScrollableScrollPhysics(),
                          gridDelegate:
                              const SliverGridDelegateWithFixedCrossAxisCount(
                            crossAxisCount: 2,
                            childAspectRatio: 1.4,
                          ),
                          children: [
                            ColorButton(
                              text: 'Основной цвет',
                              settingCubit: _settingCubit,
                              givenColor: setting.primaryColorDark,
                              keyColor: 'primary_color_dark',
                            ),
                            ColorButton(
                              settingCubit: _settingCubit,
                              givenColor: setting.secondaryColorDark,
                              keyColor: 'secondary_color_dark',
                              text: 'Вторичный цвет',
                            ),
                            ColorButton(
                              settingCubit: _settingCubit,
                              givenColor: setting.iconColorDark,
                              keyColor: 'icon_color_dark',
                              text: 'Цвет иконок',
                            ),
                            ColorButton(
                              settingCubit: _settingCubit,
                              givenColor: setting.borderColorDark,
                              keyColor: 'border_color_dark',
                              text: 'Цвет краев',
                            ),
                            ColorButton(
                              settingCubit: _settingCubit,
                              givenColor: setting.textColorDark,
                              keyColor: 'text_color_dark',
                              text: 'Цвет текста',
                            ),
                            ColorButton(
                              settingCubit: _settingCubit,
                              givenColor: setting.fileedButtonColorDark,
                              keyColor: 'filled_button_color_dark',
                              text: 'Цвет кнопки',
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        );
      },
    );
  }

  Future<void> _pickImage(BuildContext context) async {
    final file = await ImagePickerDialog.showPickerDialog(context);
    if (file != null) {
      _settingCubit.uploadFile('logo', file);
    }
  }
}
