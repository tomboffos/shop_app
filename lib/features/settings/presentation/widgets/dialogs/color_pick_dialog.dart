import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_colorpicker/flutter_colorpicker.dart';

class ColorPickDialog extends StatefulWidget {
  final Color givenColor;
  final Function(Color) onColorChange;
  const ColorPickDialog({
    super.key,
    required this.givenColor,
    required this.onColorChange,
  });

  @override
  State<ColorPickDialog> createState() => _ColorPickDialogState();
}

class _ColorPickDialogState extends State<ColorPickDialog> {
  Color? pickerColor;

  @override
  void initState() {
    pickerColor = widget.givenColor;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      content: SizedBox(
        height: 450,
        child: Column(
          children: [
            ColorPicker(
              enableAlpha: false,
              showLabel: false,
              pickerColor: pickerColor ?? Colors.black,
              onColorChanged: (value) {
                setState(() {
                  pickerColor = value;
                });
              },
            ),
            OutlinedButton(
              onPressed: () {
                if (pickerColor != null) {
                  widget.onColorChange(pickerColor!);
                  context.router.pop();
                }
              },
              child: Text('Сохранить'),
            ),
          ],
        ),
      ),
    );
  }
}
