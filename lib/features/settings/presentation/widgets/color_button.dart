import 'package:flutter/material.dart';
import 'package:flutter_colorpicker/flutter_colorpicker.dart';
import 'package:shop_app_mobile/core/utils/constants/gaps.dart';
import 'package:shop_app_mobile/core/utils/constants/insets.dart';
import 'package:shop_app_mobile/core/utils/constants/radiuses.dart';
import 'package:shop_app_mobile/features/settings/domain/dto/setting_update_dto.dart';
import 'package:shop_app_mobile/features/settings/presentation/cubit/setting_cubit.dart';
import 'package:shop_app_mobile/features/settings/presentation/widgets/dialogs/color_pick_dialog.dart';

class ColorButton extends StatelessWidget {
  const ColorButton({
    super.key,
    required SettingCubit settingCubit,
    required this.givenColor,
    required this.keyColor,
    required this.text,
  }) : _settingCubit = settingCubit;

  final SettingCubit _settingCubit;
  final Color givenColor;
  final String keyColor;
  final String text;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: Insets.dimens2x.copyWith(left: 0, top: 0, bottom: 0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Gaps.gap2,
          Text(text),
          Gaps.gap,
          InkWell(
            onTap: () => showDialog(
              context: context,
              builder: (BuildContext context) => ColorPickDialog(
                givenColor: givenColor,
                onColorChange: (p0) {
                  _settingCubit.updateSettings(
                    SettingUpdateDto(
                      name: keyColor,
                      value: p0.toHexString(),
                    ),
                  );
                },
              ),
            ),
            child: Container(
              width: 40,
              height: 40,
              decoration: BoxDecoration(
                color: givenColor,
                border: Border.all(
                  color: Colors.black.withOpacity(0.5),
                ),
                borderRadius: Radiuses.radius,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
