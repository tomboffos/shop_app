import 'package:freezed_annotation/freezed_annotation.dart';

part 'recommendation_category_dto.g.dart';
part 'recommendation_category_dto.freezed.dart';

@freezed
class RecommendationCategoryDto with _$RecommendationCategoryDto {
  factory RecommendationCategoryDto({
    required int id,
    @Default(0) int order,
  }) = _RecommendationCategoryDto;

  factory RecommendationCategoryDto.fromJson(Map<String, dynamic> json) =>
      _$RecommendationCategoryDtoFromJson(json);
}
