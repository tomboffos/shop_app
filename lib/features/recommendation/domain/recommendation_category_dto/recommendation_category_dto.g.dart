// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'recommendation_category_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$RecommendationCategoryDtoImpl _$$RecommendationCategoryDtoImplFromJson(
        Map<String, dynamic> json) =>
    _$RecommendationCategoryDtoImpl(
      id: json['id'] as int,
      order: json['order'] as int? ?? 0,
    );

Map<String, dynamic> _$$RecommendationCategoryDtoImplToJson(
        _$RecommendationCategoryDtoImpl instance) =>
    <String, dynamic>{
      'id': instance.id,
      'order': instance.order,
    };
