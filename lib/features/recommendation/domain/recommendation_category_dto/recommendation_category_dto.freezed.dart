// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'recommendation_category_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

RecommendationCategoryDto _$RecommendationCategoryDtoFromJson(
    Map<String, dynamic> json) {
  return _RecommendationCategoryDto.fromJson(json);
}

/// @nodoc
mixin _$RecommendationCategoryDto {
  int get id => throw _privateConstructorUsedError;
  int get order => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $RecommendationCategoryDtoCopyWith<RecommendationCategoryDto> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $RecommendationCategoryDtoCopyWith<$Res> {
  factory $RecommendationCategoryDtoCopyWith(RecommendationCategoryDto value,
          $Res Function(RecommendationCategoryDto) then) =
      _$RecommendationCategoryDtoCopyWithImpl<$Res, RecommendationCategoryDto>;
  @useResult
  $Res call({int id, int order});
}

/// @nodoc
class _$RecommendationCategoryDtoCopyWithImpl<$Res,
        $Val extends RecommendationCategoryDto>
    implements $RecommendationCategoryDtoCopyWith<$Res> {
  _$RecommendationCategoryDtoCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? order = null,
  }) {
    return _then(_value.copyWith(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      order: null == order
          ? _value.order
          : order // ignore: cast_nullable_to_non_nullable
              as int,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$RecommendationCategoryDtoImplCopyWith<$Res>
    implements $RecommendationCategoryDtoCopyWith<$Res> {
  factory _$$RecommendationCategoryDtoImplCopyWith(
          _$RecommendationCategoryDtoImpl value,
          $Res Function(_$RecommendationCategoryDtoImpl) then) =
      __$$RecommendationCategoryDtoImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({int id, int order});
}

/// @nodoc
class __$$RecommendationCategoryDtoImplCopyWithImpl<$Res>
    extends _$RecommendationCategoryDtoCopyWithImpl<$Res,
        _$RecommendationCategoryDtoImpl>
    implements _$$RecommendationCategoryDtoImplCopyWith<$Res> {
  __$$RecommendationCategoryDtoImplCopyWithImpl(
      _$RecommendationCategoryDtoImpl _value,
      $Res Function(_$RecommendationCategoryDtoImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? order = null,
  }) {
    return _then(_$RecommendationCategoryDtoImpl(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      order: null == order
          ? _value.order
          : order // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$RecommendationCategoryDtoImpl implements _RecommendationCategoryDto {
  _$RecommendationCategoryDtoImpl({required this.id, this.order = 0});

  factory _$RecommendationCategoryDtoImpl.fromJson(Map<String, dynamic> json) =>
      _$$RecommendationCategoryDtoImplFromJson(json);

  @override
  final int id;
  @override
  @JsonKey()
  final int order;

  @override
  String toString() {
    return 'RecommendationCategoryDto(id: $id, order: $order)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$RecommendationCategoryDtoImpl &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.order, order) || other.order == order));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, id, order);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$RecommendationCategoryDtoImplCopyWith<_$RecommendationCategoryDtoImpl>
      get copyWith => __$$RecommendationCategoryDtoImplCopyWithImpl<
          _$RecommendationCategoryDtoImpl>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$RecommendationCategoryDtoImplToJson(
      this,
    );
  }
}

abstract class _RecommendationCategoryDto implements RecommendationCategoryDto {
  factory _RecommendationCategoryDto({required final int id, final int order}) =
      _$RecommendationCategoryDtoImpl;

  factory _RecommendationCategoryDto.fromJson(Map<String, dynamic> json) =
      _$RecommendationCategoryDtoImpl.fromJson;

  @override
  int get id;
  @override
  int get order;
  @override
  @JsonKey(ignore: true)
  _$$RecommendationCategoryDtoImplCopyWith<_$RecommendationCategoryDtoImpl>
      get copyWith => throw _privateConstructorUsedError;
}
