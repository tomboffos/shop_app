import 'package:freezed_annotation/freezed_annotation.dart';

part 'recommendation_service_dto.g.dart';
part 'recommendation_service_dto.freezed.dart';

@freezed
class RecommendationServiceDto with _$RecommendationServiceDto {
  factory RecommendationServiceDto({
    required int id,
    @Default(0) int order,
  }) = _RecommendationServiceDto;

  factory RecommendationServiceDto.fromJson(Map<String, dynamic> json) =>
      _$RecommendationServiceDtoFromJson(json);
}
