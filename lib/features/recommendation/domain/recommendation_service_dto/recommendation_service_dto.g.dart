// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'recommendation_service_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$RecommendationServiceDtoImpl _$$RecommendationServiceDtoImplFromJson(
        Map<String, dynamic> json) =>
    _$RecommendationServiceDtoImpl(
      id: json['id'] as int,
      order: json['order'] as int? ?? 0,
    );

Map<String, dynamic> _$$RecommendationServiceDtoImplToJson(
        _$RecommendationServiceDtoImpl instance) =>
    <String, dynamic>{
      'id': instance.id,
      'order': instance.order,
    };
