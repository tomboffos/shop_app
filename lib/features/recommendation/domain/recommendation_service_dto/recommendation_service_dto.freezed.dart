// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'recommendation_service_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

RecommendationServiceDto _$RecommendationServiceDtoFromJson(
    Map<String, dynamic> json) {
  return _RecommendationServiceDto.fromJson(json);
}

/// @nodoc
mixin _$RecommendationServiceDto {
  int get id => throw _privateConstructorUsedError;
  int get order => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $RecommendationServiceDtoCopyWith<RecommendationServiceDto> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $RecommendationServiceDtoCopyWith<$Res> {
  factory $RecommendationServiceDtoCopyWith(RecommendationServiceDto value,
          $Res Function(RecommendationServiceDto) then) =
      _$RecommendationServiceDtoCopyWithImpl<$Res, RecommendationServiceDto>;
  @useResult
  $Res call({int id, int order});
}

/// @nodoc
class _$RecommendationServiceDtoCopyWithImpl<$Res,
        $Val extends RecommendationServiceDto>
    implements $RecommendationServiceDtoCopyWith<$Res> {
  _$RecommendationServiceDtoCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? order = null,
  }) {
    return _then(_value.copyWith(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      order: null == order
          ? _value.order
          : order // ignore: cast_nullable_to_non_nullable
              as int,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$RecommendationServiceDtoImplCopyWith<$Res>
    implements $RecommendationServiceDtoCopyWith<$Res> {
  factory _$$RecommendationServiceDtoImplCopyWith(
          _$RecommendationServiceDtoImpl value,
          $Res Function(_$RecommendationServiceDtoImpl) then) =
      __$$RecommendationServiceDtoImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({int id, int order});
}

/// @nodoc
class __$$RecommendationServiceDtoImplCopyWithImpl<$Res>
    extends _$RecommendationServiceDtoCopyWithImpl<$Res,
        _$RecommendationServiceDtoImpl>
    implements _$$RecommendationServiceDtoImplCopyWith<$Res> {
  __$$RecommendationServiceDtoImplCopyWithImpl(
      _$RecommendationServiceDtoImpl _value,
      $Res Function(_$RecommendationServiceDtoImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? order = null,
  }) {
    return _then(_$RecommendationServiceDtoImpl(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      order: null == order
          ? _value.order
          : order // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$RecommendationServiceDtoImpl implements _RecommendationServiceDto {
  _$RecommendationServiceDtoImpl({required this.id, this.order = 0});

  factory _$RecommendationServiceDtoImpl.fromJson(Map<String, dynamic> json) =>
      _$$RecommendationServiceDtoImplFromJson(json);

  @override
  final int id;
  @override
  @JsonKey()
  final int order;

  @override
  String toString() {
    return 'RecommendationServiceDto(id: $id, order: $order)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$RecommendationServiceDtoImpl &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.order, order) || other.order == order));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, id, order);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$RecommendationServiceDtoImplCopyWith<_$RecommendationServiceDtoImpl>
      get copyWith => __$$RecommendationServiceDtoImplCopyWithImpl<
          _$RecommendationServiceDtoImpl>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$RecommendationServiceDtoImplToJson(
      this,
    );
  }
}

abstract class _RecommendationServiceDto implements RecommendationServiceDto {
  factory _RecommendationServiceDto({required final int id, final int order}) =
      _$RecommendationServiceDtoImpl;

  factory _RecommendationServiceDto.fromJson(Map<String, dynamic> json) =
      _$RecommendationServiceDtoImpl.fromJson;

  @override
  int get id;
  @override
  int get order;
  @override
  @JsonKey(ignore: true)
  _$$RecommendationServiceDtoImplCopyWith<_$RecommendationServiceDtoImpl>
      get copyWith => throw _privateConstructorUsedError;
}
