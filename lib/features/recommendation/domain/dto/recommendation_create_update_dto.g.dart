// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'recommendation_create_update_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$RecommendationCreateUpdateDtoImpl
    _$$RecommendationCreateUpdateDtoImplFromJson(Map<String, dynamic> json) =>
        _$RecommendationCreateUpdateDtoImpl(
          title: json['title'] as String,
          order: json['order'] as int? ?? 0,
          categories: (json['categories'] as List<dynamic>?)
                  ?.map((e) => RecommendationCategoryDto.fromJson(
                      e as Map<String, dynamic>))
                  .toList() ??
              const [],
          products: (json['products'] as List<dynamic>?)
                  ?.map((e) => RecommendationServiceDto.fromJson(
                      e as Map<String, dynamic>))
                  .toList() ??
              const [],
        );

Map<String, dynamic> _$$RecommendationCreateUpdateDtoImplToJson(
        _$RecommendationCreateUpdateDtoImpl instance) =>
    <String, dynamic>{
      'title': instance.title,
      'order': instance.order,
      'categories': instance.categories.map((e) => e.toJson()).toList(),
      'products': instance.products.map((e) => e.toJson()).toList(),
    };
