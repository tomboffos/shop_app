import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:shop_app_mobile/features/recommendation/domain/recommendation_category_dto/recommendation_category_dto.dart';
import 'package:shop_app_mobile/features/recommendation/domain/recommendation_master_dto/recommendation_master_dto.dart';
import 'package:shop_app_mobile/features/recommendation/domain/recommendation_service_dto/recommendation_service_dto.dart';

part 'recommendation_create_update_dto.g.dart';
part 'recommendation_create_update_dto.freezed.dart';

@freezed
class RecommendationCreateUpdateDto with _$RecommendationCreateUpdateDto {
  @JsonSerializable(explicitToJson: true)
  factory RecommendationCreateUpdateDto({
    required String title,
    @Default(0) int order,
    @Default([]) List<RecommendationCategoryDto> categories,
    @Default([]) List<RecommendationServiceDto> products,
  }) = _RecommendationCreateUpdateDto;

  factory RecommendationCreateUpdateDto.fromJson(Map<String, dynamic> json) =>
      _$RecommendationCreateUpdateDtoFromJson(json);
}
