// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'recommendation_create_update_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

RecommendationCreateUpdateDto _$RecommendationCreateUpdateDtoFromJson(
    Map<String, dynamic> json) {
  return _RecommendationCreateUpdateDto.fromJson(json);
}

/// @nodoc
mixin _$RecommendationCreateUpdateDto {
  String get title => throw _privateConstructorUsedError;
  int get order => throw _privateConstructorUsedError;
  List<RecommendationCategoryDto> get categories =>
      throw _privateConstructorUsedError;
  List<RecommendationServiceDto> get products =>
      throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $RecommendationCreateUpdateDtoCopyWith<RecommendationCreateUpdateDto>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $RecommendationCreateUpdateDtoCopyWith<$Res> {
  factory $RecommendationCreateUpdateDtoCopyWith(
          RecommendationCreateUpdateDto value,
          $Res Function(RecommendationCreateUpdateDto) then) =
      _$RecommendationCreateUpdateDtoCopyWithImpl<$Res,
          RecommendationCreateUpdateDto>;
  @useResult
  $Res call(
      {String title,
      int order,
      List<RecommendationCategoryDto> categories,
      List<RecommendationServiceDto> products});
}

/// @nodoc
class _$RecommendationCreateUpdateDtoCopyWithImpl<$Res,
        $Val extends RecommendationCreateUpdateDto>
    implements $RecommendationCreateUpdateDtoCopyWith<$Res> {
  _$RecommendationCreateUpdateDtoCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? title = null,
    Object? order = null,
    Object? categories = null,
    Object? products = null,
  }) {
    return _then(_value.copyWith(
      title: null == title
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String,
      order: null == order
          ? _value.order
          : order // ignore: cast_nullable_to_non_nullable
              as int,
      categories: null == categories
          ? _value.categories
          : categories // ignore: cast_nullable_to_non_nullable
              as List<RecommendationCategoryDto>,
      products: null == products
          ? _value.products
          : products // ignore: cast_nullable_to_non_nullable
              as List<RecommendationServiceDto>,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$RecommendationCreateUpdateDtoImplCopyWith<$Res>
    implements $RecommendationCreateUpdateDtoCopyWith<$Res> {
  factory _$$RecommendationCreateUpdateDtoImplCopyWith(
          _$RecommendationCreateUpdateDtoImpl value,
          $Res Function(_$RecommendationCreateUpdateDtoImpl) then) =
      __$$RecommendationCreateUpdateDtoImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {String title,
      int order,
      List<RecommendationCategoryDto> categories,
      List<RecommendationServiceDto> products});
}

/// @nodoc
class __$$RecommendationCreateUpdateDtoImplCopyWithImpl<$Res>
    extends _$RecommendationCreateUpdateDtoCopyWithImpl<$Res,
        _$RecommendationCreateUpdateDtoImpl>
    implements _$$RecommendationCreateUpdateDtoImplCopyWith<$Res> {
  __$$RecommendationCreateUpdateDtoImplCopyWithImpl(
      _$RecommendationCreateUpdateDtoImpl _value,
      $Res Function(_$RecommendationCreateUpdateDtoImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? title = null,
    Object? order = null,
    Object? categories = null,
    Object? products = null,
  }) {
    return _then(_$RecommendationCreateUpdateDtoImpl(
      title: null == title
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String,
      order: null == order
          ? _value.order
          : order // ignore: cast_nullable_to_non_nullable
              as int,
      categories: null == categories
          ? _value._categories
          : categories // ignore: cast_nullable_to_non_nullable
              as List<RecommendationCategoryDto>,
      products: null == products
          ? _value._products
          : products // ignore: cast_nullable_to_non_nullable
              as List<RecommendationServiceDto>,
    ));
  }
}

/// @nodoc

@JsonSerializable(explicitToJson: true)
class _$RecommendationCreateUpdateDtoImpl
    implements _RecommendationCreateUpdateDto {
  _$RecommendationCreateUpdateDtoImpl(
      {required this.title,
      this.order = 0,
      final List<RecommendationCategoryDto> categories = const [],
      final List<RecommendationServiceDto> products = const []})
      : _categories = categories,
        _products = products;

  factory _$RecommendationCreateUpdateDtoImpl.fromJson(
          Map<String, dynamic> json) =>
      _$$RecommendationCreateUpdateDtoImplFromJson(json);

  @override
  final String title;
  @override
  @JsonKey()
  final int order;
  final List<RecommendationCategoryDto> _categories;
  @override
  @JsonKey()
  List<RecommendationCategoryDto> get categories {
    if (_categories is EqualUnmodifiableListView) return _categories;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_categories);
  }

  final List<RecommendationServiceDto> _products;
  @override
  @JsonKey()
  List<RecommendationServiceDto> get products {
    if (_products is EqualUnmodifiableListView) return _products;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_products);
  }

  @override
  String toString() {
    return 'RecommendationCreateUpdateDto(title: $title, order: $order, categories: $categories, products: $products)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$RecommendationCreateUpdateDtoImpl &&
            (identical(other.title, title) || other.title == title) &&
            (identical(other.order, order) || other.order == order) &&
            const DeepCollectionEquality()
                .equals(other._categories, _categories) &&
            const DeepCollectionEquality().equals(other._products, _products));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      title,
      order,
      const DeepCollectionEquality().hash(_categories),
      const DeepCollectionEquality().hash(_products));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$RecommendationCreateUpdateDtoImplCopyWith<
          _$RecommendationCreateUpdateDtoImpl>
      get copyWith => __$$RecommendationCreateUpdateDtoImplCopyWithImpl<
          _$RecommendationCreateUpdateDtoImpl>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$RecommendationCreateUpdateDtoImplToJson(
      this,
    );
  }
}

abstract class _RecommendationCreateUpdateDto
    implements RecommendationCreateUpdateDto {
  factory _RecommendationCreateUpdateDto(
          {required final String title,
          final int order,
          final List<RecommendationCategoryDto> categories,
          final List<RecommendationServiceDto> products}) =
      _$RecommendationCreateUpdateDtoImpl;

  factory _RecommendationCreateUpdateDto.fromJson(Map<String, dynamic> json) =
      _$RecommendationCreateUpdateDtoImpl.fromJson;

  @override
  String get title;
  @override
  int get order;
  @override
  List<RecommendationCategoryDto> get categories;
  @override
  List<RecommendationServiceDto> get products;
  @override
  @JsonKey(ignore: true)
  _$$RecommendationCreateUpdateDtoImplCopyWith<
          _$RecommendationCreateUpdateDtoImpl>
      get copyWith => throw _privateConstructorUsedError;
}
