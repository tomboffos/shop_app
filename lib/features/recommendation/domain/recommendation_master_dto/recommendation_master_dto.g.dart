// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'recommendation_master_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$RecommendationMasterDtoImpl _$$RecommendationMasterDtoImplFromJson(
        Map<String, dynamic> json) =>
    _$RecommendationMasterDtoImpl(
      id: json['id'] as int,
      order: json['order'] as int? ?? 0,
    );

Map<String, dynamic> _$$RecommendationMasterDtoImplToJson(
        _$RecommendationMasterDtoImpl instance) =>
    <String, dynamic>{
      'id': instance.id,
      'order': instance.order,
    };
