import 'package:freezed_annotation/freezed_annotation.dart';

part 'recommendation_master_dto.g.dart';
part 'recommendation_master_dto.freezed.dart';

@freezed
class RecommendationMasterDto with _$RecommendationMasterDto {
  factory RecommendationMasterDto({
    required int id,
    @Default(0) int order,
  }) = _RecommendationMasterDto;

  factory RecommendationMasterDto.fromJson(Map<String, dynamic> json) =>
      _$RecommendationMasterDtoFromJson(json);
}
