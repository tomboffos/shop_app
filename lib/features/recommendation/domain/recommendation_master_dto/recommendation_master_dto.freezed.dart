// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'recommendation_master_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

RecommendationMasterDto _$RecommendationMasterDtoFromJson(
    Map<String, dynamic> json) {
  return _RecommendationMasterDto.fromJson(json);
}

/// @nodoc
mixin _$RecommendationMasterDto {
  int get id => throw _privateConstructorUsedError;
  int get order => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $RecommendationMasterDtoCopyWith<RecommendationMasterDto> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $RecommendationMasterDtoCopyWith<$Res> {
  factory $RecommendationMasterDtoCopyWith(RecommendationMasterDto value,
          $Res Function(RecommendationMasterDto) then) =
      _$RecommendationMasterDtoCopyWithImpl<$Res, RecommendationMasterDto>;
  @useResult
  $Res call({int id, int order});
}

/// @nodoc
class _$RecommendationMasterDtoCopyWithImpl<$Res,
        $Val extends RecommendationMasterDto>
    implements $RecommendationMasterDtoCopyWith<$Res> {
  _$RecommendationMasterDtoCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? order = null,
  }) {
    return _then(_value.copyWith(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      order: null == order
          ? _value.order
          : order // ignore: cast_nullable_to_non_nullable
              as int,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$RecommendationMasterDtoImplCopyWith<$Res>
    implements $RecommendationMasterDtoCopyWith<$Res> {
  factory _$$RecommendationMasterDtoImplCopyWith(
          _$RecommendationMasterDtoImpl value,
          $Res Function(_$RecommendationMasterDtoImpl) then) =
      __$$RecommendationMasterDtoImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({int id, int order});
}

/// @nodoc
class __$$RecommendationMasterDtoImplCopyWithImpl<$Res>
    extends _$RecommendationMasterDtoCopyWithImpl<$Res,
        _$RecommendationMasterDtoImpl>
    implements _$$RecommendationMasterDtoImplCopyWith<$Res> {
  __$$RecommendationMasterDtoImplCopyWithImpl(
      _$RecommendationMasterDtoImpl _value,
      $Res Function(_$RecommendationMasterDtoImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? order = null,
  }) {
    return _then(_$RecommendationMasterDtoImpl(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      order: null == order
          ? _value.order
          : order // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$RecommendationMasterDtoImpl implements _RecommendationMasterDto {
  _$RecommendationMasterDtoImpl({required this.id, this.order = 0});

  factory _$RecommendationMasterDtoImpl.fromJson(Map<String, dynamic> json) =>
      _$$RecommendationMasterDtoImplFromJson(json);

  @override
  final int id;
  @override
  @JsonKey()
  final int order;

  @override
  String toString() {
    return 'RecommendationMasterDto(id: $id, order: $order)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$RecommendationMasterDtoImpl &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.order, order) || other.order == order));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, id, order);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$RecommendationMasterDtoImplCopyWith<_$RecommendationMasterDtoImpl>
      get copyWith => __$$RecommendationMasterDtoImplCopyWithImpl<
          _$RecommendationMasterDtoImpl>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$RecommendationMasterDtoImplToJson(
      this,
    );
  }
}

abstract class _RecommendationMasterDto implements RecommendationMasterDto {
  factory _RecommendationMasterDto({required final int id, final int order}) =
      _$RecommendationMasterDtoImpl;

  factory _RecommendationMasterDto.fromJson(Map<String, dynamic> json) =
      _$RecommendationMasterDtoImpl.fromJson;

  @override
  int get id;
  @override
  int get order;
  @override
  @JsonKey(ignore: true)
  _$$RecommendationMasterDtoImplCopyWith<_$RecommendationMasterDtoImpl>
      get copyWith => throw _privateConstructorUsedError;
}
