import 'package:auto_route/auto_route.dart';
import 'package:field_suggestion/field_suggestion.dart';
import 'package:flutter/material.dart';
import 'package:shop_app_mobile/core/injection/injection.dart';
import 'package:shop_app_mobile/core/utils/constants/gaps.dart';
import 'package:shop_app_mobile/core/utils/constants/insets.dart';
import 'package:shop_app_mobile/features/app/widgets/confirmation_dialog.dart';
import 'package:shop_app_mobile/features/category/domain/models/category.dart';
import 'package:shop_app_mobile/features/category/domain/repository/category_repository.dart';
import 'package:shop_app_mobile/features/home/domain/models/recommendation/recommendation.dart';
import 'package:shop_app_mobile/features/home/presentation/cubit/recommendation_cubit.dart';
import 'package:shop_app_mobile/features/product/domain/dto/product_get_dto.dart';
import 'package:shop_app_mobile/features/product/domain/models/product.dart';
import 'package:shop_app_mobile/features/product/domain/repository/product_repository.dart';
import 'package:shop_app_mobile/features/recommendation/domain/dto/recommendation_create_update_dto.dart';
import 'package:shop_app_mobile/features/recommendation/domain/recommendation_category_dto/recommendation_category_dto.dart';
import 'package:shop_app_mobile/features/recommendation/domain/recommendation_service_dto/recommendation_service_dto.dart';

class RecommendationCreateOrUpdateDialog extends StatefulWidget {
  final Recommendation? recommendation;

  const RecommendationCreateOrUpdateDialog({
    super.key,
    this.recommendation,
  });

  static Future<void> show(
    BuildContext context, {
    Recommendation? recommendation,
  }) =>
      showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        builder: (context) => RecommendationCreateOrUpdateDialog(
          recommendation: recommendation,
        ),
      );

  @override
  State<RecommendationCreateOrUpdateDialog> createState() =>
      _RecommendationCreateOrUpdateDialogState();
}

class _RecommendationCreateOrUpdateDialogState
    extends State<RecommendationCreateOrUpdateDialog> {
  final _nameController = TextEditingController();

  final FocusNode _focusNode = FocusNode();
  final _serviceController = TextEditingController();
  final _categoriesController = TextEditingController();

  List<Product> services = [];
  List<Category> categories = [];

  @override
  void initState() {
    if (widget.recommendation != null) {
      _nameController.text = widget.recommendation!.title;
      services = [...widget.recommendation!.products];
      categories = [...widget.recommendation!.categories];
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: MediaQuery.of(context).size.height * 0.7,
      child: SingleChildScrollView(
        child: Padding(
          padding: Insets.dimens2x,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Align(
                alignment: Alignment.topRight,
                child: InkWell(
                  onTap: () => context.router.pop(),
                  child: const Icon(
                    Icons.close,
                    color: Colors.black,
                    size: 30,
                  ),
                ),
              ),
              Gaps.gap,
              Text(
                'Имя',
                style: Theme.of(context).textTheme.headlineSmall,
              ),
              Gaps.gap,
              TextField(
                controller: _nameController,
                decoration: const InputDecoration(
                  hintText: 'Имя',
                ),
              ),
              Gaps.gap,
              Text(
                'Продукты',
                style: Theme.of(context).textTheme.headlineSmall,
              ),
              Gaps.gap,
              FieldSuggestion.network(
                focusNode: _focusNode,
                textController: _serviceController,
                builder: (context, value) => ListView.builder(
                  padding: EdgeInsets.zero,
                  itemBuilder: (context, index) => InkWell(
                    onTap: () {
                      setState(() {
                        if (!services.contains(value.data![index])) {
                          services.add(value.data![index]);
                        }
                      });
                      _serviceController.clear();
                    },
                    child: ListTile(
                      contentPadding: EdgeInsets.zero,
                      title: Text(
                        value.data![index].name,
                        style: Theme.of(context).textTheme.bodyLarge,
                      ),
                    ),
                  ),
                  itemCount: value.data?.length ?? 0,
                ),
                future: (input) => getIt.get<ProductRepository>().getProducts(
                      ProductGetDto(
                        search: input,
                      ),
                    ),
              ),
              Gaps.gap,
              ReorderableListView.builder(
                itemBuilder: (context, index) => Card(
                  key: ValueKey(services[index]),
                  child: ListTile(
                    title: Text(
                      services[index].name,
                    ),
                    trailing: IconButton(
                      onPressed: () {
                        setState(() {
                          services.removeAt(index);
                        });
                      },
                      icon: const Icon(Icons.close),
                    ),
                  ),
                ),
                itemCount: services.length,
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
                onReorder: (int oldIndex, int newIndex) {
                  setState(() {
                    if (newIndex > oldIndex) {
                      newIndex -= 1;
                    }
                    final items = services.removeAt(oldIndex);
                    services.insert(newIndex, items);
                  });
                },
              ),
              Gaps.gap,
              Text(
                'Категории',
                style: Theme.of(context).textTheme.headlineSmall,
              ),
              Gaps.gap,
              FieldSuggestion.network(
                textController: _categoriesController,
                builder: (context, value) => ListView.builder(
                  padding: EdgeInsets.zero,
                  itemBuilder: (context, index) => InkWell(
                    onTap: () {
                      setState(() {
                        if (!categories.contains(value.data![index])) {
                          categories.add(value.data![index]);
                        }
                      });
                      _categoriesController.clear();
                    },
                    child: ListTile(
                      contentPadding: EdgeInsets.zero,
                      title: Text(
                        value.data![index].name ?? '',
                        style: Theme.of(context).textTheme.bodyLarge,
                      ),
                    ),
                  ),
                  itemCount: value.data?.length ?? 0,
                ),
                future: (input) => getIt.get<CategoryRepository>().getAdmin(
                      search: input,
                    ),
              ),
              Gaps.gap,
              ReorderableListView.builder(
                itemBuilder: (context, index) => Card(
                  key: ValueKey(categories[index]),
                  child: ListTile(
                    title: Text(
                      categories[index].name ?? '',
                    ),
                    trailing: IconButton(
                      onPressed: () {
                        setState(() {
                          categories.removeAt(index);
                        });
                      },
                      icon: const Icon(Icons.close),
                    ),
                  ),
                ),
                itemCount: categories.length,
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
                onReorder: (int oldIndex, int newIndex) {
                  setState(() {
                    if (newIndex > oldIndex) {
                      newIndex -= 1;
                    }
                    final items = categories.removeAt(oldIndex);
                    categories.insert(newIndex, items);
                  });
                },
              ),
              Gaps.gap,
              OutlinedButton(
                style: const ButtonStyle(
                  minimumSize: MaterialStatePropertyAll(
                    Size(
                      double.infinity,
                      double.minPositive,
                    ),
                  ),
                ),
                onPressed: () async {
                  if (widget.recommendation != null) {
                    await getIt.get<RecommendationCubit>().updateRecommendation(
                          dto: RecommendationCreateUpdateDto(
                            title: _nameController.text,
                            categories: categories
                                .map(
                                  (e) => RecommendationCategoryDto(
                                    id: e.id,
                                    order: categories.indexOf(e),
                                  ),
                                )
                                .toList(),
                            products: services
                                .map(
                                  (e) => RecommendationServiceDto(
                                    id: e.id,
                                    order: services.indexOf(e),
                                  ),
                                )
                                .toList(),
                          ),
                          id: widget.recommendation!.id,
                        );
                  } else {
                    await getIt.get<RecommendationCubit>().createRecommendation(
                          dto: RecommendationCreateUpdateDto(
                            title: _nameController.text,
                            categories: categories
                                .map(
                                  (e) => RecommendationCategoryDto(
                                    id: e.id,
                                    order: categories.indexOf(e),
                                  ),
                                )
                                .toList(),
                            products: services
                                .map(
                                  (e) => RecommendationServiceDto(
                                    id: e.id,
                                    order: services.indexOf(e),
                                  ),
                                )
                                .toList(),
                          ),
                        );
                  }

                  context.router.pop();
                },
                child: const Padding(
                  padding: EdgeInsets.symmetric(vertical: 10),
                  child: Text('Сохранить'),
                ),
              ),
              Gaps.gap,
              if (widget.recommendation != null)
                FilledButton(
                  style: const ButtonStyle(
                    minimumSize: MaterialStatePropertyAll(
                      Size(
                        double.infinity,
                        double.minPositive,
                      ),
                    ),
                    backgroundColor: MaterialStatePropertyAll(
                      Colors.red,
                    ),
                  ),
                  onPressed: () async {
                    await showDialog(
                      context: context,
                      builder: (context) => ConfirmationDialog(
                        onConfirm: () => getIt
                            .get<RecommendationCubit>()
                            .deleteRecommendation(
                                id: widget.recommendation!.id),
                      ),
                    );

                    context.router.pop();
                  },
                  child: const Padding(
                    padding: EdgeInsets.symmetric(vertical: 10),
                    child: Text('Удалить'),
                  ),
                ),
              const SizedBox(
                height: 200,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
