import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shop_app_mobile/core/injection/injection.dart';
import 'package:shop_app_mobile/core/utils/constants/gaps.dart';
import 'package:shop_app_mobile/core/utils/constants/insets.dart';
import 'package:shop_app_mobile/features/home/domain/models/recommendation/recommendation.dart';
import 'package:shop_app_mobile/features/home/presentation/cubit/recommendation_cubit.dart';
import 'package:shop_app_mobile/features/recommendation/presentation/widgets/recommendation_create_or_update_dialog.dart';

@RoutePage()
class RecommendationAdminPage extends StatefulWidget {
  const RecommendationAdminPage({super.key});

  @override
  State<RecommendationAdminPage> createState() =>
      _RecommendationAdminPageState();
}

class _RecommendationAdminPageState extends State<RecommendationAdminPage> {
  final _cubit = getIt.get<RecommendationCubit>();

  @override
  void initState() {
    _cubit.getRecommendations();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<RecommendationCubit, RecommendationState>(
      bloc: _cubit,
      builder: (context, state) {
        return SingleChildScrollView(
          child: Padding(
            padding: Insets.dimens2x,
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'Рекоммендации',
                      style: Theme.of(context).textTheme.headlineMedium,
                    ),
                    FilledButton(
                      onPressed: () async {
                        await RecommendationCreateOrUpdateDialog.show(context);

                        _cubit.getRecommendations();
                      },
                      child: const Icon(
                        Icons.add,
                      ),
                    )
                  ],
                ),
                Gaps.gap2,
                state.when(
                    initial: () => Padding(
                          padding: Insets.dimens2x,
                          child: const Center(
                            child: CircularProgressIndicator(),
                          ),
                        ),
                    loaded: (data) {
                      List<Recommendation> recommendations = [...data];

                      return Column(
                        children: [
                          ReorderableListView(
                            shrinkWrap: true,
                            physics: const NeverScrollableScrollPhysics(),
                            children: recommendations
                                .map(
                                  (e) => GestureDetector(
                                    onTap: () =>
                                        RecommendationCreateOrUpdateDialog.show(
                                      context,
                                      recommendation: e,
                                    ).then((value) =>
                                            _cubit.getRecommendations()),
                                    key: ValueKey(e),
                                    child: Card(
                                      child: ListTile(
                                        title: Text(e.title),
                                        trailing: const Icon(
                                          Icons.menu,
                                        ),
                                      ),
                                    ),
                                  ),
                                )
                                .toList(),
                            onReorder: (oldIndex, newIndex) {
                              setState(() {
                                if (newIndex > oldIndex) {
                                  newIndex -= 1;
                                }
                                final items =
                                    recommendations.removeAt(oldIndex);
                                recommendations.insert(newIndex, items);
                              });
                            },
                          ),
                        ],
                      );
                    }),
              ],
            ),
          ),
        );
      },
    );
  }
}
