import 'package:shop_app_mobile/features/review/domain/dto/review_create_dto.dart';
import 'package:dio/dio.dart';

abstract class ReviewRemoteDataSource {
  static String review = '/review';

  Future<Response> getReviews(int productId);

  Future<Response> createReview(ReviewCreateDto dto);
}
