import 'package:shop_app_mobile/features/category/data/data_source/category_remote_data_source.dart';
import 'package:shop_app_mobile/features/review/data/data_source/review_remote_data_source.dart';
import 'package:shop_app_mobile/features/review/domain/dto/review_create_dto.dart';
import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';

@Injectable(as: ReviewRemoteDataSource)
class ReviewRemoteDataSourceImpl implements ReviewRemoteDataSource {
  final Dio _dio;

  ReviewRemoteDataSourceImpl(this._dio) {
    _dio.options.baseUrl += CategoryRemoteDataSource.shop;
  }

  @override
  Future<Response> getReviews(int productId) =>
      _dio.get('${ReviewRemoteDataSource.review}/$productId');

  @override
  Future<Response> createReview(ReviewCreateDto dto) => _dio.post(
        ReviewRemoteDataSource.review,
        data: dto.toJson(),
      );
}
