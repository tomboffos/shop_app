import 'package:freezed_annotation/freezed_annotation.dart';

part 'review_create_dto.g.dart';
part 'review_create_dto.freezed.dart';

@freezed
class ReviewCreateDto with _$ReviewCreateDto {
  factory ReviewCreateDto({
    required int productId,
    String? text,
    double? rating,
  }) = _ReviewCreateDto;

  factory ReviewCreateDto.fromJson(Map<String, dynamic> json) =>
      _$ReviewCreateDtoFromJson(json);
}
