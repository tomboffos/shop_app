// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'review_create_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$ReviewCreateDtoImpl _$$ReviewCreateDtoImplFromJson(
        Map<String, dynamic> json) =>
    _$ReviewCreateDtoImpl(
      productId: json['productId'] as int,
      text: json['text'] as String?,
      rating: (json['rating'] as num?)?.toDouble(),
    );

Map<String, dynamic> _$$ReviewCreateDtoImplToJson(
        _$ReviewCreateDtoImpl instance) =>
    <String, dynamic>{
      'productId': instance.productId,
      'text': instance.text,
      'rating': instance.rating,
    };
