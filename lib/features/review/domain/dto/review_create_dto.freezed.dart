// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'review_create_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

ReviewCreateDto _$ReviewCreateDtoFromJson(Map<String, dynamic> json) {
  return _ReviewCreateDto.fromJson(json);
}

/// @nodoc
mixin _$ReviewCreateDto {
  int get productId => throw _privateConstructorUsedError;
  String? get text => throw _privateConstructorUsedError;
  double? get rating => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ReviewCreateDtoCopyWith<ReviewCreateDto> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ReviewCreateDtoCopyWith<$Res> {
  factory $ReviewCreateDtoCopyWith(
          ReviewCreateDto value, $Res Function(ReviewCreateDto) then) =
      _$ReviewCreateDtoCopyWithImpl<$Res, ReviewCreateDto>;
  @useResult
  $Res call({int productId, String? text, double? rating});
}

/// @nodoc
class _$ReviewCreateDtoCopyWithImpl<$Res, $Val extends ReviewCreateDto>
    implements $ReviewCreateDtoCopyWith<$Res> {
  _$ReviewCreateDtoCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? productId = null,
    Object? text = freezed,
    Object? rating = freezed,
  }) {
    return _then(_value.copyWith(
      productId: null == productId
          ? _value.productId
          : productId // ignore: cast_nullable_to_non_nullable
              as int,
      text: freezed == text
          ? _value.text
          : text // ignore: cast_nullable_to_non_nullable
              as String?,
      rating: freezed == rating
          ? _value.rating
          : rating // ignore: cast_nullable_to_non_nullable
              as double?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$ReviewCreateDtoImplCopyWith<$Res>
    implements $ReviewCreateDtoCopyWith<$Res> {
  factory _$$ReviewCreateDtoImplCopyWith(_$ReviewCreateDtoImpl value,
          $Res Function(_$ReviewCreateDtoImpl) then) =
      __$$ReviewCreateDtoImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({int productId, String? text, double? rating});
}

/// @nodoc
class __$$ReviewCreateDtoImplCopyWithImpl<$Res>
    extends _$ReviewCreateDtoCopyWithImpl<$Res, _$ReviewCreateDtoImpl>
    implements _$$ReviewCreateDtoImplCopyWith<$Res> {
  __$$ReviewCreateDtoImplCopyWithImpl(
      _$ReviewCreateDtoImpl _value, $Res Function(_$ReviewCreateDtoImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? productId = null,
    Object? text = freezed,
    Object? rating = freezed,
  }) {
    return _then(_$ReviewCreateDtoImpl(
      productId: null == productId
          ? _value.productId
          : productId // ignore: cast_nullable_to_non_nullable
              as int,
      text: freezed == text
          ? _value.text
          : text // ignore: cast_nullable_to_non_nullable
              as String?,
      rating: freezed == rating
          ? _value.rating
          : rating // ignore: cast_nullable_to_non_nullable
              as double?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$ReviewCreateDtoImpl implements _ReviewCreateDto {
  _$ReviewCreateDtoImpl({required this.productId, this.text, this.rating});

  factory _$ReviewCreateDtoImpl.fromJson(Map<String, dynamic> json) =>
      _$$ReviewCreateDtoImplFromJson(json);

  @override
  final int productId;
  @override
  final String? text;
  @override
  final double? rating;

  @override
  String toString() {
    return 'ReviewCreateDto(productId: $productId, text: $text, rating: $rating)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ReviewCreateDtoImpl &&
            (identical(other.productId, productId) ||
                other.productId == productId) &&
            (identical(other.text, text) || other.text == text) &&
            (identical(other.rating, rating) || other.rating == rating));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, productId, text, rating);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$ReviewCreateDtoImplCopyWith<_$ReviewCreateDtoImpl> get copyWith =>
      __$$ReviewCreateDtoImplCopyWithImpl<_$ReviewCreateDtoImpl>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$ReviewCreateDtoImplToJson(
      this,
    );
  }
}

abstract class _ReviewCreateDto implements ReviewCreateDto {
  factory _ReviewCreateDto(
      {required final int productId,
      final String? text,
      final double? rating}) = _$ReviewCreateDtoImpl;

  factory _ReviewCreateDto.fromJson(Map<String, dynamic> json) =
      _$ReviewCreateDtoImpl.fromJson;

  @override
  int get productId;
  @override
  String? get text;
  @override
  double? get rating;
  @override
  @JsonKey(ignore: true)
  _$$ReviewCreateDtoImplCopyWith<_$ReviewCreateDtoImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
