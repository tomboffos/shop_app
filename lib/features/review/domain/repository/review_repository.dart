import 'package:shop_app_mobile/features/review/domain/dto/review_create_dto.dart';
import 'package:shop_app_mobile/features/review/domain/models/review.dart';

abstract class ReviewRepository {
  Future<List<Review>> getReviews(int productId);

  Future<void> createReview(ReviewCreateDto dto);
}
