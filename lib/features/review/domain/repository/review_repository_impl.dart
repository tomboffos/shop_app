import 'package:shop_app_mobile/features/review/data/data_source/review_remote_data_source.dart';
import 'package:shop_app_mobile/features/review/domain/dto/review_create_dto.dart';
import 'package:shop_app_mobile/features/review/domain/models/review.dart';
import 'package:shop_app_mobile/features/review/domain/repository/review_repository.dart';
import 'package:injectable/injectable.dart';

@Injectable(as: ReviewRepository)
class ReviewRepositoryImpl implements ReviewRepository {
  final ReviewRemoteDataSource _reviewRemoteDataSource;

  ReviewRepositoryImpl(this._reviewRemoteDataSource);

  @override
  Future<void> createReview(ReviewCreateDto dto) =>
      _reviewRemoteDataSource.createReview(dto);

  @override
  Future<List<Review>> getReviews(int productId) async {
    final response = await _reviewRemoteDataSource.getReviews(productId);

    return (response.data['data'] as List)
        .map((e) => Review.fromJson(e))
        .toList();
  }
}
