part of 'review_cubit.dart';

@freezed
class ReviewState with _$ReviewState {
  const factory ReviewState.initial() = _Initial;
  const factory ReviewState.created() = _Created;
  const factory ReviewState.error(String message) = _Error;
  const factory ReviewState.loaded(List<Review> reviews) = _Loaded;
}
