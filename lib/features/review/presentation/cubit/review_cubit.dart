import 'package:shop_app_mobile/features/review/domain/dto/review_create_dto.dart';
import 'package:shop_app_mobile/features/review/domain/models/review.dart';
import 'package:shop_app_mobile/features/review/domain/repository/review_repository.dart';
import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';

part 'review_state.dart';
part 'review_cubit.freezed.dart';

@injectable
class ReviewCubit extends Cubit<ReviewState> {
  final ReviewRepository _reviewRepository;

  ReviewCubit(this._reviewRepository) : super(ReviewState.initial());

  Future<void> getReviews(
    int productId,
  ) async {
    try {
      final reviews = await _reviewRepository.getReviews(productId);

      emit(ReviewState.loaded(reviews));
    } on Exception {}
  }

  Future<void> createReviews(ReviewCreateDto dto) async {
    try {
      await _reviewRepository.createReview(dto);

      emit(ReviewState.created());

      await getReviews(dto.productId);
    } on DioException catch (e) {
      emit(
        ReviewState.error(e.response?.statusMessage ?? ''),
      );
    }
  }
}
