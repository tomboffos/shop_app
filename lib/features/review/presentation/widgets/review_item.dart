import 'package:flutter/material.dart';
import 'package:shop_app_mobile/core/utils/constants/insets.dart';
import 'package:shop_app_mobile/features/review/domain/models/review.dart';

class ReviewItem extends StatelessWidget {
  final Review review;

  const ReviewItem({
    super.key,
    required this.review,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: Insets.dimens.copyWith(left: 20),
      child: Text(
        review.text!,
        style: Theme.of(context).textTheme.bodyLarge,
      ),
    );
  }
}
