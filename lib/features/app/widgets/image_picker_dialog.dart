import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class ImagePickerDialog extends StatefulWidget {
  final bool multiple;
  const ImagePickerDialog({super.key, this.multiple = false});

  static Future<XFile?> showPickerDialog(context) => showDialog<XFile?>(
        context: context,
        builder: (context) => const ImagePickerDialog(),
      );

  @override
  State<ImagePickerDialog> createState() => _ImagePickerDialogState();
}

class _ImagePickerDialogState extends State<ImagePickerDialog> {
  final _picker = ImagePicker();

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      scrollable: true,
      content: Column(
        children: [
          Align(
            alignment: Alignment.topRight,
            child: InkWell(
              onTap: () => context.router.pop(),
              child: const Icon(
                Icons.close,
                color: Colors.black,
                size: 30,
              ),
            ),
          ),
          Row(
            children: [
              Expanded(
                child: FilledButton(
                  onPressed: () async {
                    final image =
                        await _picker.pickImage(source: ImageSource.camera);

                    if (image != null) {
                      context.router.pop(image);
                    }
                  },
                  child: const Text('Фото'),
                ),
              ),
              const SizedBox(
                width: 10,
              ),
              Expanded(
                child: FilledButton(
                  onPressed: () async {
                    final image =
                        await _picker.pickImage(source: ImageSource.gallery);

                    if (image != null) {
                      context.router.pop(image);
                    }
                  },
                  child: const Text('Галерея'),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}
