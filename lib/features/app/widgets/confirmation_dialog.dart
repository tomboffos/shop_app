import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:shop_app_mobile/core/utils/constants/insets.dart';

class ConfirmationDialog extends StatelessWidget {
  final Future Function()? onConfirm;
  const ConfirmationDialog({super.key, this.onConfirm});

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      scrollable: true,
      insetPadding: Insets.dimens,
      content: Column(
        children: [
          Align(
            alignment: Alignment.topRight,
            child: InkWell(
              onTap: () => context.router.pop(),
              child: const Icon(
                Icons.close,
                color: Colors.black,
                size: 30,
              ),
            ),
          ),
          Row(
            children: [
              Expanded(
                child: FilledButton(
                  onPressed: () async {
                    context.router.pop();
                  },
                  child: const Text('Отмена'),
                ),
              ),
              const SizedBox(
                width: 10,
              ),
              Expanded(
                child: FilledButton(
                  onPressed: () async {
                    await onConfirm?.call();
                    context.router.pop();
                  },
                  child: const Text('Подтвердить'),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}
