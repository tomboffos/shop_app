import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shop_app_mobile/core/injection/injection.dart';
import 'package:shop_app_mobile/core/routing/app_router.dart';
import 'package:shop_app_mobile/core/services/font_service.dart';
import 'package:shop_app_mobile/core/utils/constants/insets.dart';
import 'package:shop_app_mobile/core/utils/constants/radiuses.dart';
import 'package:shop_app_mobile/features/settings/presentation/cubit/setting_cubit.dart';

class App extends StatelessWidget {
  App({super.key});

  final _appRouter = AppRouter();

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SettingCubit, SettingState>(
      bloc: getIt.get<SettingCubit>()..initSettings(),
      builder: (context, state) {
        return MaterialApp.router(
          debugShowCheckedModeBanner: false,
          routerConfig: _appRouter.config(),
          themeMode: kIsWeb ? ThemeMode.light : ThemeMode.system,
          darkTheme: state.whenOrNull(
            loaded: (settings) => ThemeData(
              useMaterial3: true,
              textTheme: getFontFamily(settings.textTheme).apply(
                bodyColor: settings.textColorDark,
                displayColor: settings.textColorDark,
              ),
              colorSchemeSeed: settings.primaryColorDark,
              scaffoldBackgroundColor: settings.secondaryColorDark,
              iconTheme: IconThemeData(
                color: settings.iconColor,
                size: 16,
              ),
              iconButtonTheme: IconButtonThemeData(
                  style: ButtonStyle(
                iconColor: MaterialStatePropertyAll(
                  settings.iconColor,
                ),
              )),
              primaryIconTheme: IconThemeData(
                color: settings.iconColor,
              ),
              inputDecorationTheme: InputDecorationTheme(
                filled: true,
                contentPadding: Insets.dimens,
                fillColor: settings.secondaryColorLight,
                enabledBorder: OutlineInputBorder(
                  borderRadius: Radiuses.radius,
                  borderSide: BorderSide(
                    color: settings.borderColorLight,
                  ),
                ),
                border: OutlineInputBorder(
                  borderRadius: Radiuses.radius,
                  borderSide: BorderSide(
                    color: settings.borderColorLight,
                  ),
                ),
                focusedBorder: OutlineInputBorder(
                  borderRadius: Radiuses.radius,
                  borderSide: BorderSide(
                    color: settings.borderColorLight,
                  ),
                ),
              ),
              appBarTheme: AppBarTheme(
                backgroundColor: settings.primaryColorDark,
              ),
              listTileTheme: ListTileThemeData(
                shape: RoundedRectangleBorder(
                  borderRadius: Radiuses.radius,
                  side: BorderSide(color: settings.secondaryColorDark),
                ),
                tileColor: settings.primaryColorDark,
                iconColor: settings.iconColorDark,
              ),
              cardTheme: CardTheme(
                color: settings.primaryColorDark,
                elevation: 2.0,
              ),
              filledButtonTheme: FilledButtonThemeData(
                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.resolveWith(
                    (states) {
                      return settings.filledButtonColor;
                    },
                  ),
                  shape: MaterialStatePropertyAll(
                    RoundedRectangleBorder(
                      borderRadius: Radiuses.radius,
                    ),
                  ),
                ),
              ),
              bottomNavigationBarTheme: BottomNavigationBarThemeData(
                backgroundColor: settings.primaryColorDark,
                unselectedItemColor: settings.textColorDark,
                selectedItemColor: settings.textColorDark,
                type: BottomNavigationBarType.fixed,
                unselectedIconTheme: IconThemeData(
                  color: settings.iconColorDark,
                ),
                selectedIconTheme: IconThemeData(
                  color: settings.iconColorDark,
                ),
              ),
              expansionTileTheme: ExpansionTileThemeData(
                shape: RoundedRectangleBorder(
                  borderRadius: Radiuses.radius,
                ),
                collapsedShape: RoundedRectangleBorder(
                  borderRadius: Radiuses.radius,
                ),
                backgroundColor: settings.primaryColorDark,
              ),
            ),
          ),
          theme: state.whenOrNull(
            loaded: (settings) => ThemeData(
              useMaterial3: true,
              textTheme: getFontFamily(settings.textTheme).apply(
                bodyColor: settings.textColor,
                displayColor: settings.textColor,
              ),
              colorSchemeSeed: settings.primaryColorLight,
              scaffoldBackgroundColor: settings.secondaryColorLight,
              iconTheme: IconThemeData(
                color: settings.iconColor,
                size: 16,
              ),
              iconButtonTheme: IconButtonThemeData(
                  style: ButtonStyle(
                iconColor: MaterialStatePropertyAll(
                  settings.iconColor,
                ),
              )),
              primaryIconTheme: IconThemeData(
                color: settings.iconColor,
              ),
              inputDecorationTheme: InputDecorationTheme(
                filled: true,
                contentPadding: Insets.dimens,
                fillColor: settings.secondaryColorLight,
                enabledBorder: OutlineInputBorder(
                  borderRadius: Radiuses.radius,
                  borderSide: BorderSide(
                    color: settings.borderColorLight,
                  ),
                ),
                border: OutlineInputBorder(
                  borderRadius: Radiuses.radius,
                  borderSide: BorderSide(
                    color: settings.borderColorLight,
                  ),
                ),
                focusedBorder: OutlineInputBorder(
                  borderRadius: Radiuses.radius,
                  borderSide: BorderSide(
                    color: settings.borderColorLight,
                  ),
                ),
              ),
              appBarTheme: AppBarTheme(
                backgroundColor: settings.primaryColorLight,
              ),
              listTileTheme: ListTileThemeData(
                iconColor: settings.iconColor,
                shape: RoundedRectangleBorder(
                  borderRadius: Radiuses.radius,
                  side: BorderSide(color: settings.secondaryColorLight),
                ),
                tileColor: settings.primaryColorLight,
              ),
              cardTheme: CardTheme(
                color: settings.primaryColorLight,
                elevation: 2.0,
              ),
              filledButtonTheme: FilledButtonThemeData(
                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.resolveWith(
                    (states) {
                      return settings.filledButtonColor;
                    },
                  ),
                  shape: MaterialStatePropertyAll(
                    RoundedRectangleBorder(
                      borderRadius: Radiuses.radius,
                    ),
                  ),
                ),
              ),
              bottomNavigationBarTheme: BottomNavigationBarThemeData(
                backgroundColor: settings.primaryColorLight,
                unselectedItemColor: settings.textColor,
                selectedItemColor: settings.textColor,
                type: BottomNavigationBarType.fixed,
                unselectedIconTheme: IconThemeData(
                  color: settings.iconColor,
                ),
                selectedIconTheme: IconThemeData(
                  color: settings.iconColor,
                ),
              ),
              expansionTileTheme: ExpansionTileThemeData(
                shape: RoundedRectangleBorder(
                  borderRadius: Radiuses.radius,
                ),
                collapsedShape: RoundedRectangleBorder(
                  borderRadius: Radiuses.radius,
                ),
                backgroundColor: settings.primaryColorLight,
              ),
            ),
          ),
        );
      },
    );
  }
}
