import 'package:injectable/injectable.dart';
import 'package:shop_app_mobile/features/cart/data/data_source/cart_local_data_source.dart';
import 'package:shop_app_mobile/features/cart/domain/models/cart_item/cart_item.dart';
import 'package:shop_app_mobile/features/cart/domain/repository/cart_repository.dart';

@Injectable(as: CartRepository)
class CartRepositoryImpl implements CartRepository {
  final CartLocalDataSouce _cartLocalDataSouce;

  CartRepositoryImpl(this._cartLocalDataSouce);

  @override
  Future<void> addProductToCart(CartItem cartItem) =>
      _cartLocalDataSouce.addProductToCart(cartItem);

  @override
  List<CartItem> getCart() => _cartLocalDataSouce.getCart();

  @override
  Future<void> removeProductFromCart(CartItem cartItem) =>
      _cartLocalDataSouce.removeProductFromCart(cartItem);

  @override
  Future<void> clearAll() => _cartLocalDataSouce.clearAll();
}
