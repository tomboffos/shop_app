import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:hive/hive.dart';
import 'package:shop_app_mobile/features/product/domain/models/product.dart';

part 'cart_item.g.dart';
part 'cart_item.freezed.dart';

@freezed
@HiveType(typeId: 5)
class CartItem with _$CartItem {
  factory CartItem({
    @HiveField(1) required Product product,
    @HiveField(2) required int count,
  }) = _CartItem;

  factory CartItem.fromJson(Map<String, dynamic> json) =>
      _$CartItemFromJson(json);
}
