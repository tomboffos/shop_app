import 'package:hive/hive.dart';
import 'package:injectable/injectable.dart';
import 'package:shop_app_mobile/features/cart/data/data_source/cart_local_data_source.dart';
import 'package:shop_app_mobile/features/cart/domain/models/cart_item/cart_item.dart';

@Injectable(as: CartLocalDataSouce)
class CartLocalDataSourceImpl implements CartLocalDataSouce {
  @override
  Future<void> addProductToCart(CartItem cartItem) async {
    final cartItemToDelete =
        getCart().where((e) => e.product == cartItem.product).firstOrNull;

    if (cartItemToDelete != null) {
      await removeProductFromCart(cartItemToDelete);
    }

    await Hive.box(CartLocalDataSouce.cart).add(cartItem);
  }

  @override
  List<CartItem> getCart() => Hive.box(CartLocalDataSouce.cart)
      .values
      .map((e) => e as CartItem)
      .toList();

  @override
  Future<void> removeProductFromCart(CartItem cartItem) =>
      Hive.box(CartLocalDataSouce.cart).deleteAt(getCart().indexOf(cartItem));

  @override
  Future<void> clearAll() async {
    await Hive.box(CartLocalDataSouce.cart).clear();
  }
}
