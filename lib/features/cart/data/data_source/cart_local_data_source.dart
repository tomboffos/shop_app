import 'package:shop_app_mobile/features/cart/domain/models/cart_item/cart_item.dart';

abstract class CartLocalDataSouce {
  static String cart = 'cart';

  Future<void> addProductToCart(CartItem cartItem);

  List<CartItem> getCart();

  Future<void> removeProductFromCart(CartItem cartItem);

  Future<void> clearAll();
}
