import 'package:auto_route/auto_route.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:shop_app_mobile/core/injection/injection.dart';
import 'package:shop_app_mobile/core/routing/app_router.dart';
import 'package:shop_app_mobile/core/utils/extensions/string_extension.dart';
import 'package:shop_app_mobile/features/cart/domain/models/cart_item/cart_item.dart';
import 'package:shop_app_mobile/features/cart/presentation/cubit/cart_cubit.dart';

class CartItemWidget extends StatelessWidget {
  final CartItem item;

  const CartItemWidget({
    super.key,
    required this.item,
  });

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => context.router.push(
        ProductDetailRoute(
          product: item.product,
        ),
      ),
      child: Container(
        padding: const EdgeInsets.only(bottom: 20),
        child: ListTile(
          contentPadding: const EdgeInsets.symmetric(
            vertical: 20,
            horizontal: 10,
          ),
          title: Text(
              '${item.product.name} ${item.count} ${item.product.perPrice.name} : ${(item.product.price * item.count).toCurrency()} '),
          leading: CachedNetworkImage(
            imageUrl: item.product.medias.first.image,
            width: 50,
            fit: BoxFit.contain,
          ),
          trailing: SizedBox(
            width: MediaQuery.of(context).size.width * 0.45,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                IconButton(
                  onPressed: () {
                    getIt.get<CartCubit>().addItemToCart(item.product);
                  },
                  icon: const Icon(
                    Icons.add,
                  ),
                ),
                Text(
                  '${item.count}',
                  style: Theme.of(context).textTheme.bodyLarge,
                ),
                IconButton(
                  onPressed: () {
                    getIt.get<CartCubit>().reduceItemFromCart(item.product);
                  },
                  icon: const Icon(
                    Icons.remove,
                  ),
                ),
                IconButton(
                  onPressed: () {
                    getIt.get<CartCubit>().removeItemFromCart(item.product);
                  },
                  icon: const Icon(
                    Icons.close,
                    color: Colors.red,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
