import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:shop_app_mobile/features/cart/domain/models/cart_item/cart_item.dart';
import 'package:shop_app_mobile/features/cart/domain/repository/cart_repository.dart';
import 'package:shop_app_mobile/features/message/presentation/cubit/message_cubit.dart';
import 'package:shop_app_mobile/features/product/domain/models/product.dart';

part 'cart_state.dart';
part 'cart_cubit.freezed.dart';

@singleton
class CartCubit extends Cubit<CartState> {
  final CartRepository _cartRepository;
  final MessageCubit _messageCubit;

  CartCubit(
    this._cartRepository,
    this._messageCubit,
  ) : super(const CartState.initial());

  void getItems() {
    emit(
      CartState.loaded(
        items: _cartRepository.getCart()
          ..sort(
            (a, b) => a.product.id.compareTo(b.product.id),
          ),
      ),
    );
  }

  Future<void> addItemToCart(Product product) async {
    await state.whenOrNull(
      loaded: (items) async {
        CartItem? item =
            items.where((element) => element.product == product).firstOrNull;

        if (item != null) {
          await _cartRepository
              .addProductToCart(item.copyWith(count: item.count + 1));
        } else {
          await _cartRepository.addProductToCart(
            CartItem(
              product: product,
              count: 1,
            ),
          );
        }

        _messageCubit.addMessage(message: 'Продукт добавлен');

        getItems();
      },
      initial: () async {
        getItems();

        await addItemToCart(product);
      },
    );
  }

  Future<void> reduceItemFromCart(Product product) async {
    await state.whenOrNull(
      loaded: (items) async {
        CartItem item =
            items.firstWhere((element) => element.product == product);
        if (item.count - 1 > 0) {
          await _cartRepository
              .addProductToCart(item.copyWith(count: item.count - 1));
        } else {
          await _cartRepository.removeProductFromCart(item);
        }

        _messageCubit.addMessage(message: 'Продукт удален');

        getItems();
      },
      initial: () async {
        getItems();

        await reduceItemFromCart(product);
      },
    );
  }

  Future<void> removeItemFromCart(Product product) async {
    state.whenOrNull(
      loaded: (items) async {
        CartItem item =
            items.firstWhere((element) => element.product == product);

        await _cartRepository.removeProductFromCart(item);

        _messageCubit.addMessage(message: 'Продукт удален');
        getItems();
      },
      initial: () async {
        getItems();

        await removeItemFromCart(product);
      },
    );
  }

  Future<void> removeAll() async {
    await _cartRepository.clearAll();

    getItems();
  }
}
