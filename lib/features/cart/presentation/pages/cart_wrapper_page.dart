import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:shop_app_mobile/core/routing/app_router.dart';

@RoutePage()
class CartWrapperPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return const EmptyRouterPage();
  }
}
