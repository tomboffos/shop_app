import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shop_app_mobile/core/injection/injection.dart';
import 'package:shop_app_mobile/core/routing/app_router.dart';
import 'package:shop_app_mobile/core/utils/constants/gaps.dart';
import 'package:shop_app_mobile/features/cart/presentation/cubit/cart_cubit.dart';
import 'package:shop_app_mobile/features/cart/presentation/widgets/cart_item_widget.dart';

@RoutePage()
class CartPage extends StatefulWidget {
  const CartPage({super.key});

  @override
  State<CartPage> createState() => _CartPageState();
}

class _CartPageState extends State<CartPage> {
  final _cartCubit = getIt.get<CartCubit>();

  @override
  void initState() {
    super.initState();
    _cartCubit.getItems();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: Padding(
        padding: const EdgeInsets.symmetric(
          horizontal: 20,
        ),
        child: FilledButton(
          style: const ButtonStyle(
            minimumSize: MaterialStatePropertyAll(
              Size(double.infinity, 40),
            ),
          ),
          onPressed: () {
            context.router.push(OrderCreateRoute());
          },
          child: const Padding(
            padding: EdgeInsets.symmetric(vertical: 15),
            child: Text(
              'Заказать',
              style: TextStyle(fontSize: 18),
            ),
          ),
        ),
      ),
      body: BlocBuilder<CartCubit, CartState>(
        bloc: _cartCubit,
        builder: (context, state) => state.when(
          initial: () => const Center(
            child: CircularProgressIndicator(),
          ),
          loaded: (items) => Padding(
            padding: const EdgeInsets.symmetric(vertical: 12, horizontal: 16),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Ваша корзина',
                  style: Theme.of(context).textTheme.headlineMedium,
                ),
                Gaps.gap,
                ListView.builder(
                  itemBuilder: (context, index) => CartItemWidget(
                    item: items[index],
                  ),
                  itemCount: items.length,
                  shrinkWrap: true,
                  physics: const NeverScrollableScrollPhysics(),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
