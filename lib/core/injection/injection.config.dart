// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

// ignore_for_file: type=lint
// coverage:ignore-file

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'package:dio/dio.dart' as _i7;
import 'package:flutter_secure_storage/flutter_secure_storage.dart' as _i13;
import 'package:get_it/get_it.dart' as _i1;
import 'package:injectable/injectable.dart' as _i2;

import '../../features/banner/data/data_source/banner_remote_data_source.dart'
    as _i57;
import '../../features/banner/data/data_source/banner_remote_data_source_impl.dart'
    as _i58;
import '../../features/banner/domain/repository/banner_repository.dart' as _i59;
import '../../features/banner/domain/repository/banner_repository_impl.dart'
    as _i60;
import '../../features/banner/presentation/cubit/banner_cubit.dart' as _i79;
import '../../features/cart/data/data_source/cart_local_data_source.dart'
    as _i3;
import '../../features/cart/data/data_source/cart_local_data_source_impl.dart'
    as _i4;
import '../../features/cart/domain/repository/cart_repository.dart' as _i5;
import '../../features/cart/domain/repository/cart_repository_impl.dart' as _i6;
import '../../features/cart/presentation/cubit/cart_cubit.dart' as _i61;
import '../../features/category/data/data_source/category_remote_data_source.dart'
    as _i62;
import '../../features/category/data/data_source/category_remote_data_source_impl.dart'
    as _i63;
import '../../features/category/domain/repository/category_repository.dart'
    as _i64;
import '../../features/category/domain/repository/category_repository_impl.dart'
    as _i65;
import '../../features/category/presentation/cubit/category_cubit.dart' as _i80;
import '../../features/favorite/data/data_source/local/favorite_local_data_source.dart'
    as _i8;
import '../../features/favorite/data/data_source/local/favorite_local_data_source_impl.dart'
    as _i9;
import '../../features/favorite/domain/repository/favorite_repository_impl.dart'
    as _i11;
import '../../features/favorite/domain/repository/favorte_repository.dart'
    as _i10;
import '../../features/favorite/presentation/cubit/favorites_cubit.dart'
    as _i12;
import '../../features/filters/data/data_source/custom_field_remote_data_source.dart'
    as _i66;
import '../../features/filters/data/data_source/custom_field_remote_data_source_impl.dart'
    as _i67;
import '../../features/filters/domain/repository/custom_field_repository.dart'
    as _i68;
import '../../features/filters/domain/repository/custom_field_repository_impl.dart'
    as _i69;
import '../../features/filters/presentation/cubit/custom_field_cubit.dart'
    as _i81;
import '../../features/home/data/data_source/remote/recommendation_remote_data_source.dart'
    as _i37;
import '../../features/home/data/data_source/remote/recommendation_remote_data_source_impl.dart'
    as _i38;
import '../../features/home/domain/repository/recommendation_repository.dart'
    as _i39;
import '../../features/home/domain/repository/recommendation_repostiory_impl.dart'
    as _i40;
import '../../features/home/presentation/cubit/recommendation_cubit.dart'
    as _i75;
import '../../features/message/presentation/cubit/message_cubit.dart' as _i14;
import '../../features/news/data/data_source/remote/news_remote_data_source.dart'
    as _i15;
import '../../features/news/data/data_source/remote/news_remote_data_source_impl.dart'
    as _i16;
import '../../features/news/domain/repository/news_repository.dart' as _i17;
import '../../features/news/domain/repository/news_repository_impl.dart'
    as _i18;
import '../../features/news/presentation/cubit/news_cubit.dart' as _i70;
import '../../features/order/data/data_source/remote/order_remote_data_source.dart'
    as _i20;
import '../../features/order/data/data_source/remote/order_remote_data_source_impl.dart'
    as _i21;
import '../../features/order/domain/repository/order_repository.dart' as _i22;
import '../../features/order/domain/repository/order_repository_impl.dart'
    as _i23;
import '../../features/order/presentation/order_cubit/order_create_cubit.dart'
    as _i71;
import '../../features/order/presentation/orders_cubit/orders_cubit.dart'
    as _i24;
import '../../features/per_price_option/data/data_source/per_price_option_data_source.dart'
    as _i25;
import '../../features/per_price_option/data/data_source/per_price_option_data_source_impl.dart'
    as _i26;
import '../../features/per_price_option/domain/repository/per_price_option_repository.dart'
    as _i27;
import '../../features/per_price_option/domain/repository/per_price_option_repository_impl.dart'
    as _i28;
import '../../features/per_price_option/presentation/cubit/per_price_option_cubit.dart'
    as _i72;
import '../../features/product/data/data_source/product_remote_data_source.dart'
    as _i33;
import '../../features/product/data/data_source/product_remote_data_source_impl.dart'
    as _i34;
import '../../features/product/domain/repository/product_repository.dart'
    as _i35;
import '../../features/product/domain/repository/product_repository_impl.dart'
    as _i36;
import '../../features/product/presentation/cubit/product_cubit.dart' as _i73;
import '../../features/product_history/data/data_source/product_history_remote_data_source.dart'
    as _i29;
import '../../features/product_history/data/data_source/product_history_remote_data_source_impl.dart'
    as _i30;
import '../../features/product_history/domain/repository/product_history_repository.dart'
    as _i31;
import '../../features/product_history/domain/repository/product_history_repository_impl.dart'
    as _i32;
import '../../features/product_history/presentation/cubit/product_history_cubit.dart'
    as _i74;
import '../../features/profile/data/data_source/local/user_local_data_source.dart'
    as _i51;
import '../../features/profile/data/data_source/local/user_local_data_source_impl.dart'
    as _i52;
import '../../features/profile/data/data_source/remote/user_remote_data_source.dart'
    as _i53;
import '../../features/profile/data/data_source/remote/user_remote_data_source_impl.dart'
    as _i54;
import '../../features/profile/domain/repository/user_repository.dart' as _i55;
import '../../features/profile/domain/repository/user_repository_impl.dart'
    as _i56;
import '../../features/profile/presentation/cubit/user_cubit.dart' as _i78;
import '../../features/review/data/data_source/review_remote_data_source.dart'
    as _i41;
import '../../features/review/data/data_source/review_remote_data_source_impl.dart'
    as _i42;
import '../../features/review/domain/repository/review_repository.dart' as _i43;
import '../../features/review/domain/repository/review_repository_impl.dart'
    as _i44;
import '../../features/review/presentation/cubit/review_cubit.dart' as _i76;
import '../../features/settings/data/data_source/local/setting_local_data_source.dart'
    as _i45;
import '../../features/settings/data/data_source/local/setting_local_data_source_impl.dart'
    as _i46;
import '../../features/settings/data/data_source/remote/setting_remote_data_source.dart'
    as _i47;
import '../../features/settings/data/data_source/remote/setting_remote_data_source_impl.dart'
    as _i48;
import '../../features/settings/domain/repository/setting_repository.dart'
    as _i49;
import '../../features/settings/domain/repository/setting_repository_impl.dart'
    as _i50;
import '../../features/settings/presentation/cubit/setting_cubit.dart' as _i77;
import '../network/dio_client.dart' as _i82;
import '../services/notification_service.dart' as _i19;

extension GetItInjectableX on _i1.GetIt {
// initializes the registration of main-scope dependencies inside of GetIt
  _i1.GetIt init({
    String? environment,
    _i2.EnvironmentFilter? environmentFilter,
  }) {
    final gh = _i2.GetItHelper(
      this,
      environment,
      environmentFilter,
    );
    final registerModules = _$RegisterModules();
    gh.factory<_i3.CartLocalDataSouce>(() => _i4.CartLocalDataSourceImpl());
    gh.factory<_i5.CartRepository>(
        () => _i6.CartRepositoryImpl(gh<_i3.CartLocalDataSouce>()));
    gh.factory<_i7.Dio>(() => registerModules.client);
    gh.factory<_i8.FavoriteLocalDataSource>(
        () => _i9.FavoriteLocalDataSourceImpl());
    gh.factory<_i10.FavoriteRepository>(
        () => _i11.FavoriteRepositoryImpl(gh<_i8.FavoriteLocalDataSource>()));
    gh.singleton<_i12.FavoritesCubit>(
        _i12.FavoritesCubit(gh<_i10.FavoriteRepository>()));
    gh.factory<_i13.FlutterSecureStorage>(() => registerModules.secureStorage);
    gh.singleton<_i14.MessageCubit>(_i14.MessageCubit());
    gh.factory<_i15.NewsRemoteDataSource>(
        () => _i16.NewsRemoteDataSourceImpl(gh<_i7.Dio>()));
    gh.factory<_i17.NewsRepository>(
        () => _i18.NewsRepositoryImpl(gh<_i15.NewsRemoteDataSource>()));
    gh.factory<_i19.NotificationService>(
        () => _i19.NotificationService(gh<_i13.FlutterSecureStorage>()));
    gh.factory<_i20.OrderRemoteDataSource>(
        () => _i21.OrderRemoteDataSourceImpl(gh<_i7.Dio>()));
    gh.factory<_i22.OrderRepository>(
        () => _i23.OrderRepositoryImpl(gh<_i20.OrderRemoteDataSource>()));
    gh.factory<_i24.OrdersCubit>(
        () => _i24.OrdersCubit(gh<_i22.OrderRepository>()));
    gh.factory<_i25.PerPriceOptionDataSource>(
        () => _i26.PerPriceOptiondataSourceImpl(gh<_i7.Dio>()));
    gh.factory<_i27.PerPriceOptionRepository>(() =>
        _i28.PerPriceOptionsRepositoryImpl(
            gh<_i25.PerPriceOptionDataSource>()));
    gh.factory<_i29.ProductHistoryRemoteDataSource>(
        () => _i30.ProductHistoryRemoteDataSourceImpl(gh<_i7.Dio>()));
    gh.factory<_i31.ProductHistoryRepository>(() =>
        _i32.ProductHistoryRepositoryImpl(
            gh<_i29.ProductHistoryRemoteDataSource>()));
    gh.factory<_i33.ProductRemoteDataSource>(
        () => _i34.ProductRemoteDataSourceImpl(gh<_i7.Dio>()));
    gh.factory<_i35.ProductRepository>(
        () => _i36.ProductRepositoryImpl(gh<_i33.ProductRemoteDataSource>()));
    gh.factory<_i37.RecommendationRemoteDataSource>(
        () => _i38.RecommendationRemoteDataSourceImpl(gh<_i7.Dio>()));
    gh.factory<_i39.RecommendationRepository>(() =>
        _i40.RecommendationRepositoryImpl(
            gh<_i37.RecommendationRemoteDataSource>()));
    gh.factory<_i41.ReviewRemoteDataSource>(
        () => _i42.ReviewRemoteDataSourceImpl(gh<_i7.Dio>()));
    gh.factory<_i43.ReviewRepository>(
        () => _i44.ReviewRepositoryImpl(gh<_i41.ReviewRemoteDataSource>()));
    gh.factory<_i45.SettingLocalDataSource>(
        () => _i46.SettingLocalDataSourceImpl());
    gh.factory<_i47.SettingRemoteDataSource>(
        () => _i48.SettingRemoteDataSourceImpl(gh<_i7.Dio>()));
    gh.factory<_i49.SettingRepository>(() => _i50.SettingRepositoryImpl(
          gh<_i47.SettingRemoteDataSource>(),
          gh<_i45.SettingLocalDataSource>(),
        ));
    gh.factory<_i51.UserLocalDataSource>(
        () => _i52.UserLocalDataSourceImpl(gh<_i13.FlutterSecureStorage>()));
    gh.factory<_i53.UserRemoteDataSource>(
        () => _i54.UserRemoteDataSourceImpl(gh<_i7.Dio>()));
    gh.factory<_i55.UserRepository>(() => _i56.UserRepositoryImpl(
          gh<_i53.UserRemoteDataSource>(),
          gh<_i51.UserLocalDataSource>(),
          gh<_i19.NotificationService>(),
        ));
    gh.factory<_i57.BannerRemoteDataSource>(
        () => _i58.BannerRemoteDataSourceImpl(gh<_i7.Dio>()));
    gh.factory<_i59.BannerRepository>(
        () => _i60.BannerRepositoryImpl(gh<_i57.BannerRemoteDataSource>()));
    gh.singleton<_i61.CartCubit>(_i61.CartCubit(
      gh<_i5.CartRepository>(),
      gh<_i14.MessageCubit>(),
    ));
    gh.factory<_i62.CategoryRemoteDataSource>(
        () => _i63.CategoryRemoteDataSourceImpl(gh<_i7.Dio>()));
    gh.factory<_i64.CategoryRepository>(
        () => _i65.CategoryRepositoryImpl(gh<_i62.CategoryRemoteDataSource>()));
    gh.factory<_i66.CustomFieldRemoteDataSource>(
        () => _i67.CustomFieldRemoteDataSourceImpl(gh<_i7.Dio>()));
    gh.factory<_i68.CustomFieldRepository>(() =>
        _i69.CustomFieldRepositoryImpl(gh<_i66.CustomFieldRemoteDataSource>()));
    gh.factory<_i70.NewsCubit>(() => _i70.NewsCubit(gh<_i17.NewsRepository>()));
    gh.factory<_i71.OrderCreateCubit>(
        () => _i71.OrderCreateCubit(gh<_i22.OrderRepository>()));
    gh.factory<_i72.PerPriceOptionCubit>(() => _i72.PerPriceOptionCubit(
          gh<_i35.ProductRepository>(),
          gh<_i27.PerPriceOptionRepository>(),
        ));
    gh.factory<_i73.ProductCubit>(
        () => _i73.ProductCubit(gh<_i35.ProductRepository>()));
    gh.factory<_i74.ProductHistoryCubit>(
        () => _i74.ProductHistoryCubit(gh<_i31.ProductHistoryRepository>()));
    gh.factory<_i75.RecommendationCubit>(
        () => _i75.RecommendationCubit(gh<_i39.RecommendationRepository>()));
    gh.factory<_i76.ReviewCubit>(
        () => _i76.ReviewCubit(gh<_i43.ReviewRepository>()));
    gh.singleton<_i77.SettingCubit>(
        _i77.SettingCubit(gh<_i49.SettingRepository>()));
    gh.singleton<_i78.UserCubit>(_i78.UserCubit(gh<_i55.UserRepository>()));
    gh.factory<_i79.BannerCubit>(
        () => _i79.BannerCubit(gh<_i59.BannerRepository>()));
    gh.factory<_i80.CategoryCubit>(
        () => _i80.CategoryCubit(gh<_i64.CategoryRepository>()));
    gh.factory<_i81.CustomFieldCubit>(
        () => _i81.CustomFieldCubit(gh<_i68.CustomFieldRepository>()));
    return this;
  }
}

class _$RegisterModules extends _i82.RegisterModules {}
