import 'package:hive_flutter/hive_flutter.dart';
import 'package:shop_app_mobile/features/cart/data/data_source/cart_local_data_source.dart';
import 'package:shop_app_mobile/features/cart/domain/models/cart_item/cart_item.dart';
import 'package:shop_app_mobile/features/category/domain/models/category.dart';
import 'package:shop_app_mobile/features/favorite/data/data_source/local/favorite_local_data_source.dart';
import 'package:shop_app_mobile/features/filters/domain/models/custom_field.dart';
import 'package:shop_app_mobile/features/product/domain/models/per_price/per_price.dart';
import 'package:shop_app_mobile/features/product/domain/models/product.dart';
import 'package:shop_app_mobile/features/product/domain/models/product_media/product_media.dart';
import 'package:shop_app_mobile/features/settings/data/data_source/local/setting_local_data_source.dart';

class HiveAdapterIniter {
  static initAdapters() async {
    await Hive.initFlutter();

    Hive
      ..registerAdapter(ProductAdapter()) // 1
      ..registerAdapter(CategoryAdapter()) // 2
      ..registerAdapter(PerPriceAdapter()) // 3
      ..registerAdapter(ProductMediaAdapter()) // 4
      ..registerAdapter(CartItemAdapter())
      ..registerAdapter(CustomFieldAdapter()); // 7

    await Hive.openBox(FavoriteLocalDataSource.favorite);
    await Hive.openBox(CartLocalDataSouce.cart);
    await Hive.openBox(SettingLocalDataSource.setting);
  }
}
