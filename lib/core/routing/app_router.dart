import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:shop_app_mobile/features/cart/presentation/pages/cart_page.dart';
import 'package:shop_app_mobile/features/cart/presentation/pages/cart_wrapper_page.dart';
import 'package:shop_app_mobile/features/category/domain/models/category.dart';
import 'package:shop_app_mobile/features/category/presentation/pages/categories_detail_page.dart';
import 'package:shop_app_mobile/features/category/presentation/pages/categories_page.dart';
import 'package:shop_app_mobile/features/category/presentation/pages/categories_wrapper_page.dart';
import 'package:shop_app_mobile/features/favorite/presentation/pages/favorite_wrapper_page.dart';
import 'package:shop_app_mobile/features/favorite/presentation/pages/favorites_page.dart';
import 'package:shop_app_mobile/features/filters/presentation/pages/filter_page.dart';
import 'package:shop_app_mobile/features/filters/presentation/pages/filters_admin_page.dart';
import 'package:shop_app_mobile/features/home/domain/models/recommendation/recommendation.dart';
import 'package:shop_app_mobile/features/home/presentation/pages/home_page.dart';
import 'package:shop_app_mobile/features/home/presentation/pages/main_page.dart';
import 'package:shop_app_mobile/features/home/presentation/pages/main_wrapper_page.dart';
import 'package:shop_app_mobile/features/home/presentation/pages/recommendation_page.dart';
import 'package:shop_app_mobile/features/news/domain/models/news.dart';
import 'package:shop_app_mobile/features/news/presentation/pages/news_admin_page.dart';
import 'package:shop_app_mobile/features/news/presentation/pages/news_detail_page.dart';
import 'package:shop_app_mobile/features/news/presentation/pages/news_page.dart';
import 'package:shop_app_mobile/features/order/presentation/pages/order_create_page.dart';
import 'package:shop_app_mobile/features/order/presentation/pages/orders_page.dart';
import 'package:shop_app_mobile/features/per_price_option/presentation/pages/per_price_admin_page.dart';
import 'package:shop_app_mobile/features/product/domain/dto/product_get_dto.dart';
import 'package:shop_app_mobile/features/product/domain/models/product.dart';
import 'package:shop_app_mobile/features/product/presentation/pages/product_detail_page.dart';
import 'package:shop_app_mobile/features/product/presentation/pages/products_admin_page.dart';
import 'package:shop_app_mobile/features/product/presentation/pages/products_page.dart';
import 'package:shop_app_mobile/features/profile/presentation/pages/profile_page.dart';
import 'package:shop_app_mobile/features/profile/presentation/pages/register_page.dart';
import 'package:shop_app_mobile/features/recommendation/presentation/page/recommendation_admin_page.dart';
import 'package:shop_app_mobile/features/settings/presentation/pages/setting_update_page.dart';

part 'app_router.gr.dart';

@AutoRouterConfig(replaceInRouteName: 'Page,Route')
class AppRouter extends _$AppRouter {
  @override
  List<AutoRoute> get routes => [
        AutoRoute(
          path: '/',
          page: HomeRoute.page,
          initial: true,
          maintainState: true,
          children: [
            AutoRoute(
              path: 'main',
              page: MainWrapperRoute.page,
              children: [
                AutoRoute(
                  page: MainRoute.page,
                  initial: true,
                ),
                AutoRoute(
                  path: 'categories-details',
                  page: CategoriesDetailRoute.page,
                ),
                AutoRoute(
                  path: 'news',
                  page: NewsRoute.page,
                ),
                AutoRoute(
                  page: RecommendationRoute.page,
                ),
                AutoRoute(
                  page: ProductsRoute.page,
                ),
                AutoRoute(
                  page: FilterRoute.page,
                ),
                AutoRoute(
                  page: ProductDetailRoute.page,
                ),
                AutoRoute(
                  page: NewsDetailRoute.page,
                )
              ],
            ),
            AutoRoute(
              path: 'categories',
              page: CategoriesWrapperRoute.page,
              children: [
                AutoRoute(
                  page: CategoriesRoute.page,
                  initial: true,
                ),
                AutoRoute(
                  path: 'categories-details',
                  page: CategoriesDetailRoute.page,
                ),
                AutoRoute(
                  page: ProductsRoute.page,
                ),
                AutoRoute(
                  page: FilterRoute.page,
                ),
                AutoRoute(
                  page: ProductDetailRoute.page,
                ),
              ],
            ),
            AutoRoute(
              path: 'cart',
              page: CartWrapperRoute.page,
              children: [
                AutoRoute(
                  page: CartRoute.page,
                  initial: true,
                ),
                AutoRoute(
                  page: ProductDetailRoute.page,
                ),
                AutoRoute(
                  page: OrderCreateRoute.page,
                )
              ],
            ),
            AutoRoute(
              path: 'favorites',
              page: FavoriteWrapperRoute.page,
              children: [
                AutoRoute(
                  page: FavoritesRoute.page,
                  initial: true,
                ),
                AutoRoute(
                  page: ProductDetailRoute.page,
                ),
              ],
            ),
            AutoRoute(
              path: 'profile',
              page: ProfileWrapperRoute.page,
              children: [
                AutoRoute(
                  page: ProfileRoute.page,
                  initial: true,
                ),
                AutoRoute(
                  page: RegisterRoute.page,
                ),
                AutoRoute(
                  page: OrdersRoute.page,
                ),
                AutoRoute(
                  page: SettingUpdateRoute.page,
                ),
                AutoRoute(
                  page: RecommendationAdminRoute.page,
                ),
                AutoRoute(
                  page: ProductsAdminRoute.page,
                ),
                AutoRoute(
                  page: CategoriesRoute.page,
                ),
                AutoRoute(
                  page: FiltersAdminRoute.page,
                ),
                AutoRoute(
                  page: PerPriceAdminRoute.page,
                ),
                AutoRoute(
                  page: NewsAdminRoute.page,
                ),
              ],
            ),
            AutoRoute(
              path: 'news_detail',
              page: NewsDetailRoute.page,
            ),
          ],
        ),
      ];
}

/// Пустой роут, нужен пока как заглушка
class EmptyRouterPage extends AutoRouter {
  const EmptyRouterPage({super.key});
}
