// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouterGenerator
// **************************************************************************

// ignore_for_file: type=lint
// coverage:ignore-file

part of 'app_router.dart';

abstract class _$AppRouter extends RootStackRouter {
  // ignore: unused_element
  _$AppRouter({super.navigatorKey});

  @override
  final Map<String, PageFactory> pagesMap = {
    CartRoute.name: (routeData) {
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const CartPage(),
      );
    },
    CartWrapperRoute.name: (routeData) {
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: CartWrapperPage(),
      );
    },
    CategoriesDetailRoute.name: (routeData) {
      final args = routeData.argsAs<CategoriesDetailRouteArgs>();
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: CategoriesDetailPage(
          key: args.key,
          category: args.category,
        ),
      );
    },
    CategoriesRoute.name: (routeData) {
      final args = routeData.argsAs<CategoriesRouteArgs>(
          orElse: () => const CategoriesRouteArgs());
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: CategoriesPage(
          key: args.key,
          admin: args.admin,
        ),
      );
    },
    CategoriesWrapperRoute.name: (routeData) {
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const CategoriesWrapperPage(),
      );
    },
    FavoriteWrapperRoute.name: (routeData) {
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const FavoriteWrapperPage(),
      );
    },
    FavoritesRoute.name: (routeData) {
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const FavoritesPage(),
      );
    },
    FilterRoute.name: (routeData) {
      final args = routeData.argsAs<FilterRouteArgs>();
      return AutoRoutePage<ProductGetDto>(
        routeData: routeData,
        child: FilterPage(
          key: args.key,
          category: args.category,
        ),
      );
    },
    FiltersAdminRoute.name: (routeData) {
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const FiltersAdminPage(),
      );
    },
    HomeRoute.name: (routeData) {
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const HomePage(),
      );
    },
    MainRoute.name: (routeData) {
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const MainPage(),
      );
    },
    MainWrapperRoute.name: (routeData) {
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: WrappedRoute(child: const MainWrapperPage()),
      );
    },
    NewsAdminRoute.name: (routeData) {
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const NewsAdminPage(),
      );
    },
    NewsDetailRoute.name: (routeData) {
      final args = routeData.argsAs<NewsDetailRouteArgs>();
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: NewsDetailPage(
          key: args.key,
          news: args.news,
        ),
      );
    },
    NewsRoute.name: (routeData) {
      final args = routeData.argsAs<NewsRouteArgs>();
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: NewsPage(
          key: args.key,
          news: args.news,
        ),
      );
    },
    OrderCreateRoute.name: (routeData) {
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const OrderCreatePage(),
      );
    },
    OrdersRoute.name: (routeData) {
      final args = routeData.argsAs<OrdersRouteArgs>(
          orElse: () => const OrdersRouteArgs());
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: OrdersPage(
          key: args.key,
          admin: args.admin,
        ),
      );
    },
    PerPriceAdminRoute.name: (routeData) {
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const PerPriceAdminPage(),
      );
    },
    ProductDetailRoute.name: (routeData) {
      final args = routeData.argsAs<ProductDetailRouteArgs>();
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: ProductDetailPage(
          key: args.key,
          product: args.product,
        ),
      );
    },
    ProductsAdminRoute.name: (routeData) {
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const ProductsAdminPage(),
      );
    },
    ProductsRoute.name: (routeData) {
      final args = routeData.argsAs<ProductsRouteArgs>(
          orElse: () => const ProductsRouteArgs());
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: ProductsPage(
          key: args.key,
          category: args.category,
          search: args.search,
        ),
      );
    },
    ProfileRoute.name: (routeData) {
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const ProfilePage(),
      );
    },
    ProfileWrapperRoute.name: (routeData) {
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const ProfileWrapperPage(),
      );
    },
    RecommendationAdminRoute.name: (routeData) {
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const RecommendationAdminPage(),
      );
    },
    RecommendationRoute.name: (routeData) {
      final args = routeData.argsAs<RecommendationRouteArgs>();
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: RecommendationPage(
          key: args.key,
          recommendation: args.recommendation,
        ),
      );
    },
    RegisterRoute.name: (routeData) {
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const RegisterPage(),
      );
    },
    SettingUpdateRoute.name: (routeData) {
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const SettingUpdatePage(),
      );
    },
  };
}

/// generated route for
/// [CartPage]
class CartRoute extends PageRouteInfo<void> {
  const CartRoute({List<PageRouteInfo>? children})
      : super(
          CartRoute.name,
          initialChildren: children,
        );

  static const String name = 'CartRoute';

  static const PageInfo<void> page = PageInfo<void>(name);
}

/// generated route for
/// [CartWrapperPage]
class CartWrapperRoute extends PageRouteInfo<void> {
  const CartWrapperRoute({List<PageRouteInfo>? children})
      : super(
          CartWrapperRoute.name,
          initialChildren: children,
        );

  static const String name = 'CartWrapperRoute';

  static const PageInfo<void> page = PageInfo<void>(name);
}

/// generated route for
/// [CategoriesDetailPage]
class CategoriesDetailRoute extends PageRouteInfo<CategoriesDetailRouteArgs> {
  CategoriesDetailRoute({
    Key? key,
    required Category category,
    List<PageRouteInfo>? children,
  }) : super(
          CategoriesDetailRoute.name,
          args: CategoriesDetailRouteArgs(
            key: key,
            category: category,
          ),
          initialChildren: children,
        );

  static const String name = 'CategoriesDetailRoute';

  static const PageInfo<CategoriesDetailRouteArgs> page =
      PageInfo<CategoriesDetailRouteArgs>(name);
}

class CategoriesDetailRouteArgs {
  const CategoriesDetailRouteArgs({
    this.key,
    required this.category,
  });

  final Key? key;

  final Category category;

  @override
  String toString() {
    return 'CategoriesDetailRouteArgs{key: $key, category: $category}';
  }
}

/// generated route for
/// [CategoriesPage]
class CategoriesRoute extends PageRouteInfo<CategoriesRouteArgs> {
  CategoriesRoute({
    Key? key,
    bool admin = false,
    List<PageRouteInfo>? children,
  }) : super(
          CategoriesRoute.name,
          args: CategoriesRouteArgs(
            key: key,
            admin: admin,
          ),
          initialChildren: children,
        );

  static const String name = 'CategoriesRoute';

  static const PageInfo<CategoriesRouteArgs> page =
      PageInfo<CategoriesRouteArgs>(name);
}

class CategoriesRouteArgs {
  const CategoriesRouteArgs({
    this.key,
    this.admin = false,
  });

  final Key? key;

  final bool admin;

  @override
  String toString() {
    return 'CategoriesRouteArgs{key: $key, admin: $admin}';
  }
}

/// generated route for
/// [CategoriesWrapperPage]
class CategoriesWrapperRoute extends PageRouteInfo<void> {
  const CategoriesWrapperRoute({List<PageRouteInfo>? children})
      : super(
          CategoriesWrapperRoute.name,
          initialChildren: children,
        );

  static const String name = 'CategoriesWrapperRoute';

  static const PageInfo<void> page = PageInfo<void>(name);
}

/// generated route for
/// [FavoriteWrapperPage]
class FavoriteWrapperRoute extends PageRouteInfo<void> {
  const FavoriteWrapperRoute({List<PageRouteInfo>? children})
      : super(
          FavoriteWrapperRoute.name,
          initialChildren: children,
        );

  static const String name = 'FavoriteWrapperRoute';

  static const PageInfo<void> page = PageInfo<void>(name);
}

/// generated route for
/// [FavoritesPage]
class FavoritesRoute extends PageRouteInfo<void> {
  const FavoritesRoute({List<PageRouteInfo>? children})
      : super(
          FavoritesRoute.name,
          initialChildren: children,
        );

  static const String name = 'FavoritesRoute';

  static const PageInfo<void> page = PageInfo<void>(name);
}

/// generated route for
/// [FilterPage]
class FilterRoute extends PageRouteInfo<FilterRouteArgs> {
  FilterRoute({
    Key? key,
    required Category category,
    List<PageRouteInfo>? children,
  }) : super(
          FilterRoute.name,
          args: FilterRouteArgs(
            key: key,
            category: category,
          ),
          initialChildren: children,
        );

  static const String name = 'FilterRoute';

  static const PageInfo<FilterRouteArgs> page = PageInfo<FilterRouteArgs>(name);
}

class FilterRouteArgs {
  const FilterRouteArgs({
    this.key,
    required this.category,
  });

  final Key? key;

  final Category category;

  @override
  String toString() {
    return 'FilterRouteArgs{key: $key, category: $category}';
  }
}

/// generated route for
/// [FiltersAdminPage]
class FiltersAdminRoute extends PageRouteInfo<void> {
  const FiltersAdminRoute({List<PageRouteInfo>? children})
      : super(
          FiltersAdminRoute.name,
          initialChildren: children,
        );

  static const String name = 'FiltersAdminRoute';

  static const PageInfo<void> page = PageInfo<void>(name);
}

/// generated route for
/// [HomePage]
class HomeRoute extends PageRouteInfo<void> {
  const HomeRoute({List<PageRouteInfo>? children})
      : super(
          HomeRoute.name,
          initialChildren: children,
        );

  static const String name = 'HomeRoute';

  static const PageInfo<void> page = PageInfo<void>(name);
}

/// generated route for
/// [MainPage]
class MainRoute extends PageRouteInfo<void> {
  const MainRoute({List<PageRouteInfo>? children})
      : super(
          MainRoute.name,
          initialChildren: children,
        );

  static const String name = 'MainRoute';

  static const PageInfo<void> page = PageInfo<void>(name);
}

/// generated route for
/// [MainWrapperPage]
class MainWrapperRoute extends PageRouteInfo<void> {
  const MainWrapperRoute({List<PageRouteInfo>? children})
      : super(
          MainWrapperRoute.name,
          initialChildren: children,
        );

  static const String name = 'MainWrapperRoute';

  static const PageInfo<void> page = PageInfo<void>(name);
}

/// generated route for
/// [NewsAdminPage]
class NewsAdminRoute extends PageRouteInfo<void> {
  const NewsAdminRoute({List<PageRouteInfo>? children})
      : super(
          NewsAdminRoute.name,
          initialChildren: children,
        );

  static const String name = 'NewsAdminRoute';

  static const PageInfo<void> page = PageInfo<void>(name);
}

/// generated route for
/// [NewsDetailPage]
class NewsDetailRoute extends PageRouteInfo<NewsDetailRouteArgs> {
  NewsDetailRoute({
    Key? key,
    required News news,
    List<PageRouteInfo>? children,
  }) : super(
          NewsDetailRoute.name,
          args: NewsDetailRouteArgs(
            key: key,
            news: news,
          ),
          initialChildren: children,
        );

  static const String name = 'NewsDetailRoute';

  static const PageInfo<NewsDetailRouteArgs> page =
      PageInfo<NewsDetailRouteArgs>(name);
}

class NewsDetailRouteArgs {
  const NewsDetailRouteArgs({
    this.key,
    required this.news,
  });

  final Key? key;

  final News news;

  @override
  String toString() {
    return 'NewsDetailRouteArgs{key: $key, news: $news}';
  }
}

/// generated route for
/// [NewsPage]
class NewsRoute extends PageRouteInfo<NewsRouteArgs> {
  NewsRoute({
    Key? key,
    required List<News> news,
    List<PageRouteInfo>? children,
  }) : super(
          NewsRoute.name,
          args: NewsRouteArgs(
            key: key,
            news: news,
          ),
          initialChildren: children,
        );

  static const String name = 'NewsRoute';

  static const PageInfo<NewsRouteArgs> page = PageInfo<NewsRouteArgs>(name);
}

class NewsRouteArgs {
  const NewsRouteArgs({
    this.key,
    required this.news,
  });

  final Key? key;

  final List<News> news;

  @override
  String toString() {
    return 'NewsRouteArgs{key: $key, news: $news}';
  }
}

/// generated route for
/// [OrderCreatePage]
class OrderCreateRoute extends PageRouteInfo<void> {
  const OrderCreateRoute({List<PageRouteInfo>? children})
      : super(
          OrderCreateRoute.name,
          initialChildren: children,
        );

  static const String name = 'OrderCreateRoute';

  static const PageInfo<void> page = PageInfo<void>(name);
}

/// generated route for
/// [OrdersPage]
class OrdersRoute extends PageRouteInfo<OrdersRouteArgs> {
  OrdersRoute({
    Key? key,
    bool admin = false,
    List<PageRouteInfo>? children,
  }) : super(
          OrdersRoute.name,
          args: OrdersRouteArgs(
            key: key,
            admin: admin,
          ),
          initialChildren: children,
        );

  static const String name = 'OrdersRoute';

  static const PageInfo<OrdersRouteArgs> page = PageInfo<OrdersRouteArgs>(name);
}

class OrdersRouteArgs {
  const OrdersRouteArgs({
    this.key,
    this.admin = false,
  });

  final Key? key;

  final bool admin;

  @override
  String toString() {
    return 'OrdersRouteArgs{key: $key, admin: $admin}';
  }
}

/// generated route for
/// [PerPriceAdminPage]
class PerPriceAdminRoute extends PageRouteInfo<void> {
  const PerPriceAdminRoute({List<PageRouteInfo>? children})
      : super(
          PerPriceAdminRoute.name,
          initialChildren: children,
        );

  static const String name = 'PerPriceAdminRoute';

  static const PageInfo<void> page = PageInfo<void>(name);
}

/// generated route for
/// [ProductDetailPage]
class ProductDetailRoute extends PageRouteInfo<ProductDetailRouteArgs> {
  ProductDetailRoute({
    Key? key,
    required Product product,
    List<PageRouteInfo>? children,
  }) : super(
          ProductDetailRoute.name,
          args: ProductDetailRouteArgs(
            key: key,
            product: product,
          ),
          initialChildren: children,
        );

  static const String name = 'ProductDetailRoute';

  static const PageInfo<ProductDetailRouteArgs> page =
      PageInfo<ProductDetailRouteArgs>(name);
}

class ProductDetailRouteArgs {
  const ProductDetailRouteArgs({
    this.key,
    required this.product,
  });

  final Key? key;

  final Product product;

  @override
  String toString() {
    return 'ProductDetailRouteArgs{key: $key, product: $product}';
  }
}

/// generated route for
/// [ProductsAdminPage]
class ProductsAdminRoute extends PageRouteInfo<void> {
  const ProductsAdminRoute({List<PageRouteInfo>? children})
      : super(
          ProductsAdminRoute.name,
          initialChildren: children,
        );

  static const String name = 'ProductsAdminRoute';

  static const PageInfo<void> page = PageInfo<void>(name);
}

/// generated route for
/// [ProductsPage]
class ProductsRoute extends PageRouteInfo<ProductsRouteArgs> {
  ProductsRoute({
    Key? key,
    Category? category,
    String? search,
    List<PageRouteInfo>? children,
  }) : super(
          ProductsRoute.name,
          args: ProductsRouteArgs(
            key: key,
            category: category,
            search: search,
          ),
          initialChildren: children,
        );

  static const String name = 'ProductsRoute';

  static const PageInfo<ProductsRouteArgs> page =
      PageInfo<ProductsRouteArgs>(name);
}

class ProductsRouteArgs {
  const ProductsRouteArgs({
    this.key,
    this.category,
    this.search,
  });

  final Key? key;

  final Category? category;

  final String? search;

  @override
  String toString() {
    return 'ProductsRouteArgs{key: $key, category: $category, search: $search}';
  }
}

/// generated route for
/// [ProfilePage]
class ProfileRoute extends PageRouteInfo<void> {
  const ProfileRoute({List<PageRouteInfo>? children})
      : super(
          ProfileRoute.name,
          initialChildren: children,
        );

  static const String name = 'ProfileRoute';

  static const PageInfo<void> page = PageInfo<void>(name);
}

/// generated route for
/// [ProfileWrapperPage]
class ProfileWrapperRoute extends PageRouteInfo<void> {
  const ProfileWrapperRoute({List<PageRouteInfo>? children})
      : super(
          ProfileWrapperRoute.name,
          initialChildren: children,
        );

  static const String name = 'ProfileWrapperRoute';

  static const PageInfo<void> page = PageInfo<void>(name);
}

/// generated route for
/// [RecommendationAdminPage]
class RecommendationAdminRoute extends PageRouteInfo<void> {
  const RecommendationAdminRoute({List<PageRouteInfo>? children})
      : super(
          RecommendationAdminRoute.name,
          initialChildren: children,
        );

  static const String name = 'RecommendationAdminRoute';

  static const PageInfo<void> page = PageInfo<void>(name);
}

/// generated route for
/// [RecommendationPage]
class RecommendationRoute extends PageRouteInfo<RecommendationRouteArgs> {
  RecommendationRoute({
    Key? key,
    required Recommendation recommendation,
    List<PageRouteInfo>? children,
  }) : super(
          RecommendationRoute.name,
          args: RecommendationRouteArgs(
            key: key,
            recommendation: recommendation,
          ),
          initialChildren: children,
        );

  static const String name = 'RecommendationRoute';

  static const PageInfo<RecommendationRouteArgs> page =
      PageInfo<RecommendationRouteArgs>(name);
}

class RecommendationRouteArgs {
  const RecommendationRouteArgs({
    this.key,
    required this.recommendation,
  });

  final Key? key;

  final Recommendation recommendation;

  @override
  String toString() {
    return 'RecommendationRouteArgs{key: $key, recommendation: $recommendation}';
  }
}

/// generated route for
/// [RegisterPage]
class RegisterRoute extends PageRouteInfo<void> {
  const RegisterRoute({List<PageRouteInfo>? children})
      : super(
          RegisterRoute.name,
          initialChildren: children,
        );

  static const String name = 'RegisterRoute';

  static const PageInfo<void> page = PageInfo<void>(name);
}

/// generated route for
/// [SettingUpdatePage]
class SettingUpdateRoute extends PageRouteInfo<void> {
  const SettingUpdateRoute({List<PageRouteInfo>? children})
      : super(
          SettingUpdateRoute.name,
          initialChildren: children,
        );

  static const String name = 'SettingUpdateRoute';

  static const PageInfo<void> page = PageInfo<void>(name);
}
