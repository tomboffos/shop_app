import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:injectable/injectable.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';
import 'package:shop_app_mobile/core/network/auth_interceptor.dart';

@module
abstract class RegisterModules {
  Dio get client => Dio(
        BaseOptions(
          baseUrl: 'https://shop-app.app-rent.kz/api',
        ),
      )..interceptors.addAll(
          [
            if (!kIsWeb)
              PrettyDioLogger(
                requestHeader: true,
                requestBody: true,
              ),
            AuthInterceptor(),
          ],
        );

  FlutterSecureStorage get secureStorage => const FlutterSecureStorage();
}
