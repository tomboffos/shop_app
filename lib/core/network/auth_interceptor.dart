import 'package:dio/dio.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:shop_app_mobile/core/injection/injection.dart';
import 'package:shop_app_mobile/features/profile/data/data_source/local/user_local_data_source.dart';
import 'package:shop_app_mobile/features/profile/data/data_source/remote/user_remote_data_source.dart';

class AuthInterceptor extends Interceptor {
  @override
  void onRequest(
      RequestOptions options, RequestInterceptorHandler handler) async {
    options.headers.addAll({
      "Accept": "application/json",
    });

    if (options.path.contains(UserRemoteDataSource.userApi) ||
        options.path.contains('admin')) {
      final secure = getIt.get<FlutterSecureStorage>();
      final token = await secure.read(key: UserLocalDataSource.token);
      if (token != null) {
        options.headers.addAll({"Authorization": "Bearer $token"});
      }
    }

    super.onRequest(options, handler);
  }
}
