import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:injectable/injectable.dart';
import 'package:firebase_messaging/firebase_messaging.dart';

@injectable
class NotificationService {
  final FlutterSecureStorage _secureStorage;

  NotificationService(
    this._secureStorage,
  );

  static String token = 'firebaseToken';

  void saveNotificationToken() async {
    await FirebaseMessaging.instance.requestPermission(provisional: true);

    await FirebaseMessaging.instance.subscribeToTopic('global');

    final token = await FirebaseMessaging.instance.getToken();
    if (token != null) {
      await _secureStorage.write(key: NotificationService.token, value: token);
    }
  }

  Future<String?> getToken() =>
      _secureStorage.read(key: NotificationService.token);
}
