import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

enum GoogleFont {
  roboto,
  lato,
  openSans,
  montserrat,
  sourceSansPro,
}

TextTheme getFontFamily(GoogleFont font) {
  switch (font) {
    case GoogleFont.roboto:
      return GoogleFonts.robotoTextTheme();
    case GoogleFont.lato:
      return GoogleFonts.latoTextTheme();
    case GoogleFont.openSans:
      return GoogleFonts.openSansTextTheme();
    case GoogleFont.montserrat:
      return GoogleFonts.montserratTextTheme();
    case GoogleFont.sourceSansPro:
      return GoogleFonts.sourceSans3TextTheme();
    default:
      return GoogleFonts.robotoTextTheme();
  }
}
