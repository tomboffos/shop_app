import 'package:flutter/material.dart';

extension ThemeExtension on BuildContext {
  TextTheme get text => Theme.of(this).textTheme;

  Size get size => MediaQuery.of(this).size;
}
