import 'package:intl/intl.dart';

extension StringExtension on double {
  String toCurrency() => NumberFormat.currency(
        locale: 'ru',
        symbol: 'тг',
        decimalDigits: 0,
      ).format(this);
}
