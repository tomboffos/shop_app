import 'package:flutter/material.dart';

class Radiuses {
  Radiuses._();

  static BorderRadius radius = BorderRadius.circular(10);
  static BorderRadius radius2x = BorderRadius.circular(20);
  static BorderRadius radius3x = BorderRadius.circular(30);
  static BorderRadius radius4x = BorderRadius.circular(40);
  static BorderRadius radius05x = BorderRadius.circular(5);
}
