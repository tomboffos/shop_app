import 'package:flutter/material.dart';

class Gaps {
  Gaps._();

  static SizedBox gap = const SizedBox(
    height: 10,
  );
  static SizedBox gap2 = const SizedBox(
    height: 20,
  );
  static SizedBox gap3 = const SizedBox(
    height: 30,
  );
  static SizedBox gap4 = const SizedBox(
    height: 40,
  );
  static SizedBox gap05x = const SizedBox(
    height: 5,
  );
}
