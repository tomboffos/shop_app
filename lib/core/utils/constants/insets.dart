import 'package:flutter/material.dart';

class Insets {
  Insets._();

  static EdgeInsets dimens = const EdgeInsets.all(10);
  static EdgeInsets dimens05x = const EdgeInsets.all(5);
  static EdgeInsets dimens2x = const EdgeInsets.all(20);
  static EdgeInsets dimens3x = const EdgeInsets.all(30);
  static EdgeInsets dimens4x = const EdgeInsets.all(40);
}
