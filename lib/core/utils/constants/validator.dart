class Validator {
  static String? required(String? value) {
    if (value == null || value.isEmpty) {
      return 'Это поле обязательно';
    }
    return null;
  }

  static String? email(String? value) {
    if (value == null || value.isEmpty) {
      return 'Введите почту';
    }
    // Corrected regular expression for validating an email
    String pattern = r'^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$';
    RegExp regex = RegExp(pattern);
    if (!regex.hasMatch(value)) {
      return 'Введите правильную почту';
    }
    return null;
  }
}
