import 'package:flutter/services.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

class HexColorConverter implements JsonConverter<Color, String?> {
  const HexColorConverter();

  @override
  Color fromJson(String? json) {
    final buffer = StringBuffer();
    if (json?.length == 6 || json?.length == 7) buffer.write('ff');
    buffer.write(json?.replaceFirst('#', ''));
    return Color(int.parse(buffer.toString(), radix: 16));
  }

  @override
  String toJson(Color object) {
    return '#'
        '${object.alpha.toRadixString(16).padLeft(2, '0')}'
        '${object.red.toRadixString(16).padLeft(2, '0')}'
        '${object.green.toRadixString(16).padLeft(2, '0')}'
        '${object.blue.toRadixString(16).padLeft(2, '0')}';
  }
}
