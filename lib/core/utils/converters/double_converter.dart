import 'package:freezed_annotation/freezed_annotation.dart';

class DoubleConverter extends JsonConverter<double, String> {
  const DoubleConverter();

  @override
  double fromJson(String json) {
    return double.parse(json);
  }

  @override
  String toJson(double object) {
    return object.toString();
  }
}
