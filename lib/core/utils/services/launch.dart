import 'package:url_launcher/url_launcher.dart';

class Launcher {
  Launcher._();

  static void onTapLink(String? link) async {
    if (link == null) {
      return;
    }
    final uri = Uri.parse(link);

    if (await canLaunchUrl(uri)) {
      launchUrl(uri);
      return;
    }
  }
}
